/***************************************************************************
  written by    : Andrea Tangherloni
  last updated  : May 25, 2021
  e-mail:     : andrea.tangherloni@unibg.it

***************************************************************************/

#ifdef _WIN32
	#include "headers\\libraries.h"
    #include "headers\\dirent.h"
	#include "headers\\simulation.h"
	const string slash = "\\";
	const int os = 0;
#elif _WIN64
	#include "headers\\libraries.h"
	#include "headers\\dirent.h"
	#include "headers\\simulation.h"
	const string slash = "\\";
	const int os = 0;
#else
	#include "dirent.h"
	#include "headers/libraries.h"
	#include "headers/simulation.h"
	const string slash = "/";
	const int os = 1;
#endif

using namespace std;

vector<string> openDirectory(string path);

int main(int argc, char* argv[])
{

	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	// check mandatory parameters
	if(argc == 1)
	{	
		cout << "***************************************************************\n";
		cout << "ERROR: the following parameters are mandatory:\n";
		cout << " # path to BioSimWare project\n";
		cout << " # output folder\n\n";
		cout << "The following parameters are optional:\n";
		cout << " # verbose enable:\n";
		cout << "   * level 1 (-v1)\n";
		cout << "   * level 2 (-v2)\n";
		cout << "   * level 3 (-v3)\n";
		cout << " # fitness enabled (-f)\n";
		cout << "***************************************************************\n";
		exit(-1);
	}

	if(argc == 2)
	{
		cout << "***************************************************************\n";
		cout << "ERROR: the following parameters are mandatory:\n";
		cout << " # path to BioSimWare project\n";
		cout << " # output folder\n\n";
		cout << "The following parameters are optional:\n";
		cout << " # verbose enable:\n";
		cout << "   * level 1 (-v1)\n";
		cout << "   * level 2 (-v2)\n";
		cout << "   * level 3 (-v3)\n";
		cout << " # fitness enabled (-f)\n";
		cout << "***************************************************************\n";
		exit(-2);
	}

	//check output folder
	string pathOut = argv[2];
	string folder;

	if(pathOut[0] == '.' ||
		pathOut[0] == '-' ||
		pathOut[0] == '~' ||
		pathOut[0] == ';')
	{
		cout << "***************************************************************\n";
		cout << "The output folder cannot start with:\n";
		cout << " .\n";
		cout << " -\n";
		cout << " ~\n";
		cout << " ;\n";
		cout << "***************************************************************\n";
		exit(-5);
	}

	// check optional parameters
	int verbose = 0;
	int fitness = 0;

	for(int i = 3; i < argc; i++)
	{
		string temp = argv[i];
		if(strcmp(temp.c_str(), "-f") == 0)
		{
			fitness = 1;
			break;
		}
	}

	if(fitness)
		verbose=0;
	else
	{
		for(int i = 3; i < argc; i++)
		{
			string temp = argv[i];
			if(strcmp(temp.c_str(), "-v1") == 0)
			{
				verbose = 1;
				break;
			}
			if(strcmp(temp.c_str(), "-v2") == 0)
			{
				verbose = 2;
				break;
			}
			if(strcmp(temp.c_str(), "-v3") == 0)
			{
				verbose = 3;
				break;
			}
		}
	}

	if(fitness==0)
	{
		cout << "***************************************************************\n";
	}


	//read model files
	string pathIn = argv[1];

	//matrix reagents
	string left_side   = "NA";
	//matrix products
	string right_side  = "NA";
	//reaction rates
	string t_vector    = "NA";
	//initial quantities and reaction rates
	string M_0         = "NA";
	string c_vector    = "NA";

	//optinal files
	string atol_vector = "NA";
	string rtol        = "NA";
	string max_steps   = "NA";
	string cs_vector   = "NA";
	string M_feed      = "NA";
	string modelkind   = "NA";
	string ts_matrix   = "NA";

	vector<string> files = openDirectory(pathIn);

	if(fitness)
	{
		for(int ff = 0; ff < files.size(); ff++)
		{
			if(strcmp(files[ff].c_str(), "ts_matrix") == 0)
				ts_matrix = pathIn + slash + files[ff];
		}

		if(strcmp(ts_matrix.c_str(), "NA") == 0)
		{
			cout << "***************************************************************\n";
			cout << " * ERROR: fitness set to true, but there is not the ts_matrix file" << endl;
			cout << "***************************************************************\n";
			exit(-100);
		}
	}

	for(int ff = 0; ff < files.size(); ff++)
	{
		if(strcmp(files[ff].c_str(), "left_side") == 0)
		{
			left_side = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "right_side") == 0)
		{
			right_side = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "t_vector") == 0)
		{
			t_vector = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "M_0") == 0)
		{
			M_0 = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "c_vector") == 0)
		{
			c_vector = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "cs_vector") == 0)
		{
			cs_vector = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "M_feed") == 0)
		{
			M_feed = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "atol_vector") == 0)
		{
			atol_vector = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "rtol") == 0)
		{
			rtol = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "max_steps") == 0)
		{
			max_steps = pathIn + slash + files[ff];
		}
		else if(strcmp(files[ff].c_str(), "modelkind") == 0)
		{
			modelkind = pathIn + slash + files[ff];
		}
	}

	//check mandatory model files
	bool toExit = false;
	if(strcmp(left_side.c_str(), "NA") == 0)
	{
		if(!toExit)
			cout << "***************************************************************\n";
		cout << " * ERROR: there is not the left side file" << endl;
		cout << "***************************************************************\n";
		toExit = true;
	}
	if(strcmp(right_side.c_str(), "NA") == 0)
	{
		if(!toExit)
			cout << "***************************************************************\n";
		cout << " * ERROR: there is the not right side file" << endl;
		cout << "***************************************************************\n";
		toExit = true;
	}
	if(strcmp(c_vector.c_str(), "NA") == 0)
	{
		if(!toExit)
			cout << "***************************************************************\n";
		cout << " * ERROR: there is the not file of kinetic constants" << endl;
		cout << "***************************************************************\n";
		toExit = true;
	}
	if(strcmp(M_0.c_str(), "NA") == 0)
	{
		if(!toExit)
			cout << "***************************************************************\n";
		cout << " * ERROR: there is not the file of initial conditions" << endl;
		cout << "***************************************************************\n";
		toExit = true;
	}
	if(strcmp(t_vector.c_str(), "NA") == 0)
	{
		if(!toExit)
			cout << "***************************************************************\n";
		cout << " * ERROR: there is not the file of time instants" << endl;
		cout << "***************************************************************\n";
		toExit = true;
	}

	if(toExit)
	{
		exit(-4);
	}

	if(!fitness)
	{
		struct stat sb;
		if( !(stat(pathOut.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)))
		{
			if (os == 0)
			{
				folder = "MD " + pathOut;
				int sis = system(folder.c_str());
			}
			else
			{
				folder = "mkdir " + pathOut;
				int sis = system(folder.c_str());
			}
		}
	}

	runSimulation(pathIn, pathOut, left_side, right_side, t_vector, M_0,
				c_vector, M_feed, modelkind, cs_vector, atol_vector, rtol,
				max_steps, ts_matrix, verbose);


	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	double elapsed = duration<double>( t2 - t1 ).count();

	if(verbose)
	{
		cout << "\nSaving simulations in folder: " << pathOut.c_str() << endl;
	}

	if(fitness==0)
	{
		printf("Simulation time: %.8f\n", elapsed);
		cout << "***************************************************************\n";
		cout << "***************************************************************\n\n";
	}

	return 0;
}

//method to read all file in a folder
vector<string> openDirectory(string path)
{
	string str;
	vector<string> files;
	DIR*    dir;
	struct dirent *pdir;

	dir = opendir(path.c_str());
	if(dir == NULL)
	{
		cout << "\n\n***************************************************************\n\n";
		cout << "\nError: the input folder does not exist" << "\n";
		cout << "***************************************************************\n\n";
		exit(-1);
	}

	while ((pdir = readdir(dir)) != NULL )
	{
		str = pdir->d_name;
		if(strcmp(str.c_str(), ".") != 0 & strcmp(str.c_str(), "..") != 0 & strcmp(str.c_str(), ".DS_Store") != 0)
			files.push_back(str);
	}
	return files;
}