/***************************************************************************
  written by    : Andrea Tangherloni
  last updated  : May 25, 2021
  e-mail:     : andrea.tangherloni@unibg.it

***************************************************************************/

#ifdef _WIN32
    #include "headers\\dopri.h"
#elif _WIN64
    #include "headers\\dopri.h"
#else
    #include "headers/dopri.h"
#endif

using namespace std;

Dopri::Dopri(int N, int numSS, int numTimes, double yIn[], double t_vectorIn[], int cs_vectorIn[],
	double atol[], short2 HIn[], short2 ODEIn[], int indexHIn[], int indexODEIn[],
	double c_vectorIn[], double feedIn[], double** dynamicsIn, double rtol, int maxSteps, int verboseIn):
    t_vector(t_vectorIn), cs_vector(cs_vectorIn), atoler(atol), rtoler(rtol), 
    n(N), numSS(numSS), numTimes(numTimes), ind(0), h(1e-6), hOld(0), facl(0.2), facr(10.0),
    beta(0.04), nstiff(1000), nstep(0), nfcn(0), nrejct(0), naccpt(0)
{

	verbose = verboseIn;
	t      = 0.0;
	tD     = t_vector[0];
	tend   = t_vector[numTimes-1];
	nmax   = numTimes*maxSteps;
	
	// assegnazione delle strutture dati per ricostruire le ODEs
	y        = yIn;
	H        = HIn;
	ODE      = ODEIn;
	indexH   = indexHIn;
	indexODE = indexODEIn;
	c_vector = c_vectorIn;
	feed     = feedIn;
	dynamics = dynamicsIn;



	hmax   = tend - t;
	uround = 1.0e-16;
	// safe--safety factor in step size prediction
	safe = 0.9;

	// allocazione dei vettori necessari alla computazione
	rcont1 = new double[n];
	rcont2 = new double[n];
	rcont3 = new double[n];
	rcont4 = new double[n];
	rcont5 = new double[n];

	f = new double[n];
	yy1 = new double[n];
	k1 = new double[n];
	k2 = new double[n];
	k3 = new double[n];
	k4 = new double[n];
	k5 = new double[n];
	k6 = new double[n];
	ysti = new double[n];

}

Dopri::~Dopri()
{
	delete [] f;
	delete [] yy1;
	delete [] k1;
	delete [] k2;
	delete [] k3;
	delete [] k4;
	delete [] k5;
	delete [] k6;
	delete [] ysti;
	
	delete [] rcont1;
	delete [] rcont2;
	delete [] rcont3;
	delete [] rcont4;
	delete [] rcont5;
}

int Dopri::Integrate() 
{

	int ok = CoreIntegrator();

	if (ok < 0)
	{
		return ok;
	}

	if(t_vector[numTimes-1] == tend)
	{
		dynamics[numTimes-1][0] = tD;
		for(unsigned i = 0; i < numSS; i++)
			dynamics[numTimes-1][i+1] = ContinuousOutput(cs_vector[i]);
	}

	return 0;

} // Integrate

double Dopri::sign(double a, double b)
{
	return (b > 0.0) ? fabs(a) : -fabs(a);
} // sign

// calculates initial value for step length, h
double Dopri::hinit()
{
	int iord = 5;
	double posneg = sign(1.0, tend-t);
	double sk, sqr;
	double dnf = 0.0;
	double dny = 0.0;

	for (int i = 0; i < n; i++)
	{
		sk = atoler[i] + rtoler * fabs(y[i]);
		sqr = k1[i]/sk;
		dnf += sqr*sqr;
		sqr = y[i]/sk;
		dny += sqr*sqr;
	}

	if ((dnf <= 1.0e-10) || (dny <= 1.0e-10))
		h = 1.0e-6;
	else
		h = sqrt(dny/dnf)*0.01;

	h = min(h, hmax);
	h = sign(h, posneg);

	// perform an explicit Euler step
	for(int i = 0; i < n; i++)
		k3[i] = y[i] + h * k1[i];

	// Function(x+h, k3, k2);
	evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, k3, k2);

	// estimate the second derivative of the solution
	double der2 = 0.0;
	for (int i = 0; i < n; i++)
	{
		sk = atoler[i] + rtoler * fabs(y[i]);
		sqr = (k2[i] - k1[i])/sk;
		der2 += sqr*sqr;
	}

	der2 = sqrt(der2)/h;

	// step size is computed such that
	// h**iord * max(norm(k1), norm(der2)) = 0.01
	double der12 = max(fabs(der2), sqrt(dnf));

	double h1;
	if (der12 <= 1.0e-15)
		h1 = max(1.0e-6, fabs(h)*1.0e-3);
	else
		h1 = pow(0.01/der12, 1.0/(double)iord);

	h = min(100.0*h, min(h1, hmax));

	return sign(h, posneg);

} // hinit

// core integrator

// return value for dopcor:
//	 1 : computation successful,
//	 2 : computation successful interrupted by SolutionOutput,
//	-1 : input is not consistent,
//	-2 : larger nmax is needed,
//	-3 : step size becomes too small,
//	-4 : the problem is probably stff (interrupted).

int Dopri::CoreIntegrator()
{

	double c2, c3, c4, c5, e1, e3, e4, e5, e6, e7, d1, d3, d4, d5, d6, d7;
	double a21, a31, a32, a41, a42, a43, a51, a52, a53, a54;
	double a61, a62, a63, a64, a65, a71, a73, a74, a75, a76;

	c2=0.2, c3=0.3, c4=0.8, c5=8.0/9.0;
	a21=0.2, a31=3.0/40.0, a32=9.0/40.0;
	a41=44.0/45.0, a42=-56.0/15.0; a43=32.0/9.0;
	a51=19372.0/6561.0, a52=-25360.0/2187.0;
	a53=64448.0/6561.0, a54=-212.0/729.0;
	a61=9017.0/3168.0, a62=-355.0/33.0, a63=46732.0/5247.0;
	a64=49.0/176.0, a65=-5103.0/18656.0;
	a71=35.0/384.0, a73=500.0/1113.0, a74=125.0/192.0;
	a75=-2187.0/6784.0, a76=11.0/84.0;
	e1=71.0/57600.0, e3=-71.0/16695.0, e4=71.0/1920.0;
	e5=-17253.0/339200.0, e6=22.0/525.0, e7=-1.0/40.0;
	d1=-12715105075.0/11282082432.0, d3=87487479700.0/32700410799.0;
	d4=-10690763975.0/1880347072.0, d5=701980252875.0/199316789632.0;
	d6=-1453857185.0/822651844.0, d7=69997945.0/29380423.0;

	double posneg = sign(1.0, tend-t);
	double facold = 1.0e-4;
	double expo1 = 0.2 - beta*0.75;
	double facc1 = 1.0/facl;
	double facc2 = 1.0/facr;

	bool last = false;
	double hlamb = 0.0;
	int iasti = 0;

	// Function(x, y, k1);
	evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, y, k1);
	hmax = fabs(hmax);
	h = hinit();

	nfcn += 2;
	bool reject = false;
	int nonsti;

	SolutionOutput();

	while (true)
	{
		if(nstep > nmax)
		{

			if(verbose > 2)
				printf(" * Exit of DOPRI at t = %.4e, more than nmax = %d are needed\n", t, nmax);
			
			hOld = h;
			return -2;
		}
		if(0.1*fabs(h) <= fabs(t)*uround)
		{
			if(verbose > 2)
				printf(" * Exit of DOPRI at t = %.4e, step size too small h = %.4e\n", t, h);
			
			hOld = h;
			return -3;
		}
		if ((t + 1.01*h - tend)*posneg > 0.0)
		{
			h = tend - t;
			last = true;
		}
		nstep++;

		// the first 6 stages
		for(int i = 0; i < n; i++)
			yy1[i] = y[i] + h*a21*k1[i];
		// Function(x+c2*h, yy1, k2);
		evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, yy1, k2);
		for(int i = 0; i < n; i++)
			yy1[i] = y[i] + h*(a31*k1[i] + a32*k2[i]);
		// Function(x+c3*h, yy1, k3);
		evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, yy1, k3);

		for(int i = 0; i < n; i++)
			yy1[i] = y[i] + h*(a41*k1[i] + a42*k2[i] + a43*k3[i]);
		// Function(x+c4*h, yy1, k4);
		evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, yy1, k4);

		for(int i = 0; i <n; i++)
			yy1[i] = y[i] + h*(a51*k1[i] + a52*k2[i] + a53*k3[i] + a54*k4[i]);
		// Function(x+c5*h, yy1, k5);
		evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, yy1, k5);

		for(int i = 0; i < n; i++)
			ysti[i] = y[i] + h*(a61*k1[i] + a62*k2[i] + a63*k3[i] + a64*k4[i] + a65*k5[i]);
		// Function(x+h, ysti, k6);
		evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, ysti, k6);

		for(int i = 0; i < n; i++)
			yy1[i] = y[i] + h*(a71*k1[i] + a73*k3[i] + a74*k4[i] + a75*k5[i] + a76*k6[i]);
		// Function(x+h, yy1, k2);
		evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, yy1, k2);


		for(int i = 0; i < n; i++)
		{
			rcont5[i] = h*(d1*k1[i] + d3*k3[i] + d4*k4[i] + d5*k5[i] + d6*k6[i] + d7*k2[i]);
		}

		for (int i = 0; i < n; i++)
			k4[i] = h*(e1*k1[i] + e3*k3[i] + e4*k4[i] + e5*k5[i] + e6*k6[i] + e7*k2[i]);
		nfcn += 6;

		// for(int i=0; i < n; i++)
		// {
		// 	printf("%.4e -- %.4e\n", k4[i], rcont5[i]);
		// }
		// exit(-1000);

		// error estimation
		double err = 0.0, sk, sqr;
		for (int i = 0; i < n; i++)
		{
			sk = atoler[i] + rtoler*max(fabs(y[i]), fabs(yy1[i]));
			sqr = k4[i]/sk;
			err += sqr*sqr;
		}
		err = sqrt(err/(double)n);

		// computation of hnew
		double fac11 = pow(err, expo1);
		// Lund-stabilization
		double fac = fac11/pow(facold,beta);
		// we require facl <= hnew/h <= facr
		fac = max(facc2, min(facc1, fac/safe));
		double hnew = h/fac;

		if (err <= 1.0)
		{
			// step accepted
			facold = max(err, 1.0e-4);
			naccpt++;
			// stiffness detection
			if (!(naccpt % nstiff) || (iasti > 0))
			{
				double stnum = 0.0, stden = 0.0;
				for (int i = 0; i < n; i++)
				{
					sqr = k2[i] - k6[i];
					stnum += sqr*sqr;
					sqr = yy1[i] - ysti[i];
					stden += sqr*sqr;
				}
				if (stden > 0.0)
					hlamb = h*sqrt(stnum/stden);
				if (hlamb > 3.25)
				{
					nonsti = 0;
					iasti++;
					if (iasti == 15)
					{
						if(verbose > 2)
							printf(" * Exit of DOPRI, the problem seems to become stiff at t = %.4e\n", t);
						
						hOld = h;
						return -4;
					}
				}
				else
				{
					nonsti++;
					if (nonsti == 6)
						iasti = 0;
				}
			}

			double yd0, ydiff, bspl;
			for (int i = 0; i < n; i++)
			{
				yd0 = y[i];
				ydiff = yy1[i] - yd0;
				bspl = h*k1[i] - ydiff;
				rcont1[i] = y[i];
				rcont2[i] = ydiff;
				rcont3[i] = bspl;
				rcont4[i] = -h*k2[i] + ydiff - bspl;
			}

			memcpy(k1, k2, n*sizeof(double));
			memcpy(y, yy1, n*sizeof(double));
			tOld = t;
			t += h;

			hOld = h;

			SolutionOutput();

			if (last)
			{
				hOld = hnew;
				return 1;
			}

			if (fabs(hnew) > hmax)
				hnew = posneg*hmax;
			if (reject)
				hnew = posneg*min(fabs(hnew), fabs(h));
			reject = false;
		}
		else
		{
			// step rejected
			hnew = h/min(facc1, fac11/safe);
			reject = true;
			if (naccpt >= 1)
				nrejct++;
			last = false;
		}
		h = hnew;
	}
} // CoreIntegrator

double Dopri::ContinuousOutput(unsigned i)
{

	double theta = (tD - tOld)/hOld;
	double theta1 = 1.0 - theta;

	double value =  rcont1[i] + theta*(rcont2[i] +
		theta1*(rcont3[i] + theta*(rcont4[i] + theta1*rcont5[i])));

	if(value < DBL_MIN)
		return 0.0;
	else
		return value;
}

void Dopri::SolutionOutput()
{
	// if(naccpt == 0)
 //        tD = tOld;

	while(tD < t)
	{
		dynamics[ind][0] = tD;
		for(unsigned i = 0; i < numSS; i++)
		{
			dynamics[ind][i+1] = ContinuousOutput(cs_vector[i]);
		}

		if(ind < numTimes-1)
		{
			ind++;
			tD = t_vector[ind];
		}
		else
		{
			tD = tend;
		}
	}
}