/***************************************************************************
  written by    : Andrea Tangherloni
  last updated  : May 25, 2021
  e-mail:     : andrea.tangherloni@unibg.it

***************************************************************************/

#ifdef _WIN32
    #include "headers\\radau.h"
#elif _WIN64
    #include "headers\\radau.h"
#else
    #include "headers/radau.h"
#endif

using namespace std;


Radau::Radau(int N, int numSS, int numTimes, double yIn[], double t_vectorIn[], int cs_vectorIn[],
	double atol[], short2 HIn[], short2 ODEIn[], int indexHIn[], int indexODEIn[],
	double c_vectorIn[], double feedIn[], double** dynamicsIn,
	double rtol, int maxSteps, int verboseIn, int newtonIt):
    t_vector(t_vectorIn), cs_vector(cs_vectorIn), atoler(atol), rtoler(rtol),
    nit(newtonIt), n(N), numSS(numSS), numTimes(numTimes), ind(0), y(yIn), h(1e-6), hOld(0),
    facl(5.0), facr(1.0/8.0), nfcn(0), njac(0), ndec(0), nsol(0), nstep(0), naccpt(0), nrejct(0),
    quot1(1.0), quot2(1.2), thet(0.001), startn(false), pred(true), caljac(true), first(true)
{
	verbose = verboseIn;
	t      = 0.0;
	tOld   = 0.0;
	tD     = t_vector[0];
	tend   = t_vector[numTimes-1];
	nmax   = numTimes*maxSteps;
	
	// assegnazione delle strutture dati per ricostruire le ODEs
	y        = yIn;
	H        = HIn;
	ODE      = ODEIn;
	indexH   = indexHIn;
	indexODE = indexODEIn;
	c_vector = c_vectorIn;
	feed     = feedIn;
	dynamics = dynamicsIn;

    //max step size allowed
    hmax = tend - t;
	// -------- uround--smallest number satisfying 1.0 + uround > 1.0
	uround = 1.0e-16;
	// --------- safe--safety factor in step size prediction
	safe = 0.9;
	
	rtoler = 0.1*pow(rtoler, 2.0/3.0);
	for(int i=0; i < n;i++)
	{
		// cout << atoler[i] << endl;
		quot = atoler[i]/rtol;
    	atoler[i] = rtoler*quot;
	}

    fnewt = max(10.0*uround/rtoler, min(0.03, sqrt(rtoler)));

    // Allocate memory for 1-D arrays
    z = new double[2*n];
	z1 = new double[n];
	z2 = &z[0];
	z3 = &z[n];
	y0 = new double[n];
	scal = new double[n];
	f1 = new double[n];
	f2 = new double[n];
	f3 = new double[n];
	cont = new double[4*n];
	ip1 = new int[n];
	ip2 = new int[n];

	arrayJac = new double[n*n];

    // Allocate memory for 2-D arrays
	one = 1;
	trasp = 'T';
	e1 = new double[n*n];
	e2r = new complex<double>[n*n];

} // Constructor


Radau::~Radau()
{
	delete [] z1;
	delete [] y0;
	delete [] scal;
	delete [] f1;
	delete [] f2;
	delete [] f3;
	delete [] cont;
	delete [] ip1;
	delete [] ip2;

	delete [] e1;
	delete [] e2r;

} // Destructor

void Radau::Integrate() 
{

	int ok = CoreIntegrator();

	if (ok < 0)
	{

		for(int i=tD; i < numTimes; i++)
		{
			dynamics[i][0] = t_vector[i];
			for(unsigned j = 0; j < numSS; j++)
			{
				dynamics[i][j+1] = cont[cs_vector[j]];
			}
		}

		return;
	}

	if(t_vector[numTimes-1] == tend)
	{
		dynamics[numTimes-1][0] = tD;
		for(unsigned i = 0; i < numSS; i++)
		{
			dynamics[numTimes-1][i+1] = ContinuousOutput(cs_vector[i]);
		}
	}

	return;

} // Integrate

// return value of CoreIntegrator
//  1  computation successful,
//  2  comput. successful (interrupted by SolutionOutput)
// -1  error in linear algebra routines,
// -2  larger nmax is needed,
// -3  step size becomes too small,
// -4  matrix is repeatedly singular.
// -5 not enough memory

int Radau::CoreIntegrator()
{
    // constants
 	const double t11 = 9.1232394870892942792e-02;
	const double t12 = -0.14125529502095420843;
	const double t13 = -3.0029194105147424492e-02;
	const double t21 = 0.24171793270710701896;
	const double t22 = 0.20412935229379993199;
	const double t23 = 0.38294211275726193779;
	const double t31 = 0.96604818261509293619;
	const double ti11 = 4.3255798900631553510;
	const double ti12 = 0.33919925181580986954;
	const double ti13 = 0.54177053993587487119;
	const double ti21 = -4.1787185915519047273;
	const double ti22 = -0.32768282076106238708;
	const double ti23 = 0.47662355450055045196;
	const double ti31 = -0.50287263494578687595;
	const double ti32 = 2.5719269498556054292;
	const double ti33 = -0.59603920482822492497;

	const double sq6 = sqrt(6.0);
	const double c1 = (4.0 - sq6)/10.0;
	const double c2 = (4.0 + sq6)/10.0;
	const double c1m1 = c1 - 1.0;
	const double c2m1 = c2 - 1.0;
	const double c1mc2 = c1 - c2;
	const double u1 = 1.0/((6.0 + pow(81.0, 1.0/3.0) - pow(9.0, 1.0/3.0))/30.0);
	double alph = (12.0 - pow(81.0, 1.0/3.0) + pow(9.0, 1.0/3.0))/60.0;
	double beta = (pow(81.0, 1.0/3.0) + pow(9.0, 1.0/3.0))*sqrt(3.0)/60.0;
	const double cno = alph*alph + beta*beta;

	alph = alph/cno;
	beta = beta/cno;

	const double posneg = copysign(1.0, tend-t);
	const double hmaxn = min(fabs(hmax), fabs(tend - t));
	const double cfac = safe*(1 + 2*nit);

	h = min(fabs(h), hmaxn);
	h = copysign(h, posneg);
	hOld = h;

	bool last = false;

	if((t + h*1.0001 - tend)*posneg >= 0.0)
    {
		h = tend - t;
		last = true;
	}

	double hopt = h;
	double faccon = 1.0;

	for(int i = 0; i < n; i++)
	{
		cont[i] = y[i];
	}

	SolutionOutput();

	for(int i = 0; i < n; i++)
		scal[i] = atoler[i] + rtoler*fabs(y[i]);

	// Function(t, y, y0);
	evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, y, y0);
	nfcn++;

	double hhfac = h;
	double hacc = 0, erracc=0, thqold=0;
	int nsing = 0, ier = 0;

	// basic integration step
	ComputeJacobian();

	bool loop = true;
	while(loop)
	{
		// printf("%.8e\n", h);
		loop = false;
		// compute the matrices e1 and e2 and their decompositions
		fac1 = u1/h;
		alphn = alph/h;
		betan = beta/h;

		ier = DecompReal();

		// exit if the Jacobian is singular for 5 times
		if(ier != 0)
		{
			if(ier == -1)
				return -1;
			nsing++;
			if(nsing >= 5)
			{
				if(verbose > 2)
					printf(" * Exit of RADAU at t = %.4e\n, matrix is repeatedly singular", t);
				
				return -4;
			}

			h *= 0.5;
			hhfac = 0.5;
			reject = true;
			last = false;
			if(!caljac)
				ComputeJacobian();
			loop = true;
			continue;
		}

		ier = DecompComplex();

		if(ier != 0)
		{
			if(ier == -1)
				return(-1);
			nsing++;
			if(nsing >= 5)
			{
				if(verbose > 2)
					printf(" * Exit of RADAU at t = %.4e\n, matrix is repeatedly singular", t);
				
				return -4;
			}
			h *= 0.5;
			hhfac = 0.5;
			reject = true;
			last = false;
			if(!caljac)
				ComputeJacobian();
			loop = true;
			continue;
		}
		ndec++;

		while (true)
		{
			nstep++;
			if(nstep >= nmax)
			{
				if(verbose > 2)
					printf(" * Exit of RADAU at t = %.4e, more than nmax = %d steps are needed\n", t, nmax);
				
				return -2;
			}
			if (0.1*fabs(h) <= fabs(t)*uround)
			{
				if(verbose > 2)
					printf(" * Exit of RADAU at t = %.4e, step size too small, h = %.4e\n", t, h);
				return -3;
			}

			double tph = t + h;
			//  starting values for Newton iteration
			if (first || startn)
			{
				for(int i = 0; i < n; i++)
					z1[i] = z2[i] = z3[i] = f1[i] = f2[i] = f3[i] = 0.0;
			}
			else
			{
				double c3q = h/hOld;
				double c1q = c1*c3q;
				double c2q = c2*c3q;
				double ak1, ak2, ak3;
				for(int i = 0; i < n; i++)
				{
					ak1 = cont[i+n];
					ak2 = cont[i+2*n];
					ak3 = cont[i+3*n];
					z1[i] = c1q*(ak1 + (c1q - c2m1)*(ak2 + (c1q - c1m1)*ak3));
					z2[i] = c2q*(ak1 + (c2q - c2m1)*(ak2 + (c2q - c1m1)*ak3));
					z3[i] = c3q*(ak1 + (c3q - c2m1)*(ak2 + (c3q - c1m1)*ak3));
					f1[i] = ti11*z1[i] + ti12*z2[i] + ti13*z3[i];
					f2[i] = ti21*z1[i] + ti22*z2[i] + ti23*z3[i];
					f3[i] = ti31*z1[i] + ti32*z2[i] + ti33*z3[i];
				}
			}

			//  loop for the simplified Newton iteration
			int newt = 0;
			faccon = pow(max(faccon, uround), 0.8);
			double theta = fabs(thet);
			double dyno = 0.0, dynold = 0.0;

			while(true)
			{
				if(newt >= nit)
				{
					if(ier != 0)
					{
						nsing++;
						if(nsing >= 5)
						{
							if(verbose > 2)
								printf(" * Exit of RADAU at t = %.4e, matrix is repeatedly singular\n", t);
							return -4;
						}
					}
					h *= 0.5;
					hhfac = 0.5;
					reject = true;
					last = false;
					if(!caljac)
						ComputeJacobian();
					loop = true;
					break;
				}
				// compute the right-hand side
				for(int i = 0; i < n; i++)
					cont[i] = y[i] + z1[i];
				evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, cont, z1);

				for(int i = 0; i < n; i++)
					cont[i] = y[i] + z2[i];
				evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, cont, z2);

				for(int i = 0; i < n; i++)
					cont[i] = y[i] + z3[i];
				evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, cont, z3);

				nfcn += 3;

				// solve the linear systems
				for(int i = 0; i < n; i++)
				{
					double a1 = z1[i];
					double a2 = z2[i];
					double a3 = z3[i];
					z1[i] = ti11*a1 + ti12*a2 + ti13*a3;
					z2[i] = ti21*a1 + ti22*a2 + ti23*a3;
					z3[i] = ti31*a1 + ti32*a2 + ti33*a3;
				}

				ier = LinearSolve();

				if(ier == -1)
					return -1;
				nsol++;
				newt++;
				dyno = 0.0;
				double denom;
				for (int i = 0; i < n; i++)
				{
					denom = scal[i];
					dyno = dyno + pow(z1[i]/denom, 2) + pow(z2[i]/denom, 2) + pow(z3[i]/denom, 2);
				}
				dyno = sqrt(dyno/(3*n));

				// bad convergence or number of iterations to large
				if((newt > 1) && (newt < nit))
				{
					double thq = dyno/dynold;
					if(newt == 2)
						theta = thq;
					else
						theta = sqrt(thq*thqold);
					thqold = thq;

					if (theta < 0.99)
					{
						faccon = theta/(1.0 - theta);
						double dyth = faccon*dyno*pow(theta, nit-1-newt)/fnewt;
						if(dyth >= 1.0)
						{
							double qnewt = max(1.0e-4, min(20.0, dyth));
							hhfac = 0.8*pow(qnewt, -1.0/(4.0+nit-1-newt));
							h *= hhfac;
							reject = true;
							last = false;
							if(!caljac)
								ComputeJacobian();
							loop = true;
							break;
						}
					}
					else
					{
						if(ier != 0)
						{
							nsing++;
							if(nsing >= 5)
							{
								if(verbose > 2)
									printf(" * Exit of RADAU at t = %.4e, matrix is repeatedly singular\n", t);
								
								return -4;
							}
						}
						h *= 0.5;
						hhfac = 0.5;
						reject = true;
						last = false;
						if(!caljac)
							ComputeJacobian();
						loop = true;
						break;
					}
				}
				dynold = max(dyno, uround);

				for (int i = 0; i < n; i++)
				{
					f1[i] = f1[i] + z1[i];
					f2[i] = f2[i] + z2[i];
					f3[i] = f3[i] + z3[i];
					z1[i] = t11*f1[i] + t12*f2[i] + t13*f3[i];
					z2[i] = t21*f1[i] + t22*f2[i] + t23*f3[i];
					z3[i] = t31*f1[i] + f2[i];
				}

				if (faccon*dyno <= fnewt)
					break;
			}

			if(loop)
				break;

			// error estimation
			err = 0.0;
			ier = ErrorEstimate();
			if(ier == -1)
				return -1;

			// computation of hnew -- require 0.2 <= hnew/h <= 8.
			double fac = min(safe, cfac/(newt+2*nit));
			double quot = max(facr, min(facl, pow(err, 0.25)/fac));
			double hnew = h/quot;

			//  is the error small enough ?
			if(err < 1.0)
			{
				// step is accepted
				first = false;
				naccpt++;
				if(pred)
				{
					// predictive controller of Gustafsson
					if(naccpt > 1)
					{
						double facgus = (hacc/(h))*pow(err*err/erracc, 0.25)/safe;
						facgus = max(facr, min(facl, facgus));
						quot = max(quot,facgus);
						hnew = h/quot;
					}
					hacc = h;
					erracc = max(1.0e-2, err);
				}

				tOld = t;
				hOld = h;
				t = tph;

				double ak, acont3;
				for(int i = 0; i < n; i++)
				{
					y[i] = y[i] + z3[i];
					cont[i+n] = (z2[i] - z3[i])/c2m1;
					ak = (z1[i] - z2[i])/c1mc2;
					acont3 = z1[i]/c1;
					acont3 = (ak - acont3)/c2;
					cont[i+2*n] = (ak - cont[i+n])/c1m1;
					cont[i+3*n] = cont[i+2*n] - acont3;
				}

				for(int i = 0; i < n; i++)
				{
					scal[i] = atoler[i] + rtoler*fabs(y[i]);
					cont[i] = y[i];
				}

				SolutionOutput();

				caljac = false;
				if (last)
				{
					h = hopt;
					return 1;
				}

				evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, y, y0);

				nfcn++;
				hnew = posneg*min(fabs(hnew), hmaxn);
				hopt = min(h, hnew);

				if(reject)
					hnew = posneg*min(fabs(hnew), fabs(h));

				reject = false;

				if((t + hnew/quot1 - tend)*posneg >= 0.0)
				{
					h = tend - t;
					last = true;
				}
				else
				{
					double qt = hnew/(h);
					hhfac = h;
					if((theta <= thet) && (qt >= quot1) && (qt <= quot2))
						continue;
					h = hnew;
				}
				hhfac = h;
				if(theta > thet)
					ComputeJacobian();
				loop = true;
			}
			else // step is rejected
			{
				reject = true;
				last = false;
				if (first)
				{
					h *= 0.1;
					hhfac = 0.1;
				}
				else
				{
					hhfac = hnew/(h);
					h = hnew;
				}
				if(naccpt >= 1)
					nrejct++;
				if(!caljac)
					ComputeJacobian();
				loop = true;
			}
			break;
		}
	}

	return 1;
}  // CoreIntegrator

void Radau::ComputeJacobian()
{
	njac++;
	evaluateJAC(n, H, ODE, indexH, indexODE, feed, c_vector, y, arrayJac);
	caljac = true;
}

int Radau::DecompReal()
{
	int ier = 0;
	for(int j = 0; j < n; j++)
	{
		for(int i = 0; i < n; i++)
		{
			e1[i*n + j] = -arrayJac[i*n + j];
		}
		e1[j*n + j] += fac1;
	}


	// decomposizione mediante la fattorizzazione LU per numeri reali
	dgetrf_(&n, &n, e1, &n, ip1, &ier);

	return ier;
}

int Radau::DecompComplex()
{
	int ier = 0;
	for(int j = 0; j < n; j++)
	{
		for(int i = 0; i < n; i++)
		{
			e2r[i*n + j] = -arrayJac[i*n + j];
		}
		e2r[j*n + j] += complex<double>(alphn, betan);
	}

	// decomposizione mediante la fattorizzazione LU per numeri complessi
	zgetrf_(&n, &n, e2r, &n, ip2, &ier);

	return ier;
}

int Radau::LinearSolve()
{
	int ier = 0;
	double s2, s3 = 0;

	for(int i = 0; i < n; i++)
	{
		s2 = -f2[i];
		s3 = -f3[i];
		z1[i] -= f1[i]*fac1;
		z2[i] = z2[i] + s2*alphn - s3*betan;
		cont[i] = z3[i] + s3*alphn + s2*betan;
	}

	for(int i = n; i > 0; i--)
	{
		z[2*i-2] = z[i-1];
		z[2*i-1] = cont[i-1];
	}

	// risoluzione del sistema lineare a numeri reali
	dgetrs_(&trasp, &n, &one, e1, &n, ip1, z1, &n, &ier);

	// risoluzione del sistema lineare a numeri complessi
	zgetrs_(&trasp, &n, &one, e2r, &n, ip2, z2, &n, &ier);


	for(int i = 0; i < n; i++)
	{
		cont[i] = z[2*i+1];
		z[i] = z[2*i];
	}

	for(int i = 0; i < n; i++)
	{
		z3[i] = cont[i];
	}

	return ier;
}

int Radau::ErrorEstimate()
{

	int ier = 0;
	double hee1 = -(13.0 + 7.0*sqrt(6.0))/(3.0*h);
	double hee2 = (-13.0 + 7.0*sqrt(6.0))/(3.0*h);
	double hee3 = -1.0/(3.0*h);

	for (int i = 0; i < n; i++)
	{
		f2[i] = hee1*z1[i] + hee2*z2[i] + hee3*z3[i];
		cont[i] = f2[i] + y0[i];
	}
	dgetrs_(&trasp, &n, &one, e1, &n, ip1, cont, &n, &ier);

	err = 0.0;
	for (int i = 0; i < n; i++)
		err += pow(cont[i]/scal[i], 2);

	err = max(sqrt(err/n), 1.0e-10);

	if(err < 1.0)
		return ier;

	if(first || reject)
	{
		// printf("Ricalcolo errore\n");
		for(int i = 0; i < n; i++)
			cont[i] = y[i] + cont[i];
		evaluateODE(n, H, ODE, indexH, indexODE, feed, c_vector, cont, f1);

		nfcn++;
		
		for (int i = 0; i < n; i++)
			cont[i] = f1[i] + f2[i];
		dgetrs_(&trasp, &n, &one, e1, &n, ip1, cont, &n, &ier);

		err = 0.0;
		for (int i = 0; i < n; i++)
			err += pow(cont[i]/scal[i], 2);
		err = max(sqrt(err/n), 1.0e-10);
	}
	return ier;
}

double Radau::ContinuousOutput(unsigned i)
{
	double s, sq6, c1, c2, c2m1, c1m1;
	sq6 = sqrt(6.0);
	c1 = (4.0 - sq6)/10.0;
	c2 = (4.0 + sq6)/10.0;
	c1m1 = c1 - 1.0;
	c2m1 = c2 - 1.0;
	s = (tD - t)/hOld;
	double value = (cont[i] + s*(cont[i+n] + (s - c2m1)*(cont[i+2*n] + (s - c1m1)*cont[i+3*n])));

	return value;
}

void Radau::SolutionOutput()
{
	// if(naccpt == 0)
 //        tD = tOld;

	while(tD < t)
	{
		dynamics[ind][0] = tD;
		for(unsigned i = 0; i < numSS; i++)
		{
			dynamics[ind][i+1] = ContinuousOutput(cs_vector[i]);
		}
		if(ind < numTimes-1)
		{
			ind++;
			tD = t_vector[ind];
		}
		else
		{
			tD = tend;
		}
	}
}