/***************************************************************************
  written by    : Andrea Tangherloni
  last updated  : May 25, 2021
  e-mail:     : andrea.tangherloni@unibg.it

***************************************************************************/

#ifdef _WIN32
	#include "headers\\radau.h"
	#include "headers\\dopri.h"
	#include "headers\\reader.h"
	#include "headers\\simulation.h"
	const string slash = "\\";
#elif _WIN64
	#include "headers\\radau.h"
	#include "headers\\dopri.h"
	#include "headers\\reader.h"
	#include "headers\\rimulation.h"
	const string slash = "\\";
#else
	#include "headers/radau.h"
	#include "headers/dopri.h"
	#include "headers/reader.h"
	#include "headers/simulation.h"
	const string slash = "/";
#endif

void runSimulation(string pathIn, string pathOut, string left_sideIn,
				string right_sideIn, string t_vectorIn, string M_0In,
				string c_vectorIn, string M_feedIn, string modelkindIn,
				string cs_vectorIn, string atol_vectorIn, string rtolIn,
				string max_stepsIn, string ts_matrixIn, int verbose)
{
	Reader* reader = new Reader;

	int modelkind = 0;
	bool fitness  = false;
	double volume = 0.0;
	double conversionFactor = 0.0;

	if(strcmp(modelkindIn.c_str(), "NA") != 0)
	{
		modelkind = reader -> readModelkind(modelkindIn);
	}

	if(modelkind)
	{
		volume = reader -> readSingleValue(pathIn+slash+"volume");
		if(volume == 0)
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << " * ERROR: the modelkind is stochastic/number, but there is not the volume file\n";
			cout << "***************************************************************\n";
			exit(-7);
		}
		double avogadro = 6.022e23;
		conversionFactor =  volume*avogadro;
	}

	// read Stoichiometry Matrices
	vector<vector<short> > left_side  = reader -> readStoichiometryMatrix(left_sideIn);
	vector<vector<short> > right_side = reader -> readStoichiometryMatrix(right_sideIn);
	vector<double> M_0      = reader -> readByTab(M_0In);
	vector<double> c_vector = reader -> readByLine(c_vectorIn);
	vector<double> M_feed;
	vector<vector<double> > ts_matrix;

	vector<double> t_vector;
	vector<double> atol_vector;
	vector<int> cs_vector;
	
	double rtol = 1e-6;
	int max_steps = 10000;

	if(left_side.size() != right_side.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << left_side.size() << " rows in the left side matrix," << endl;
		cout << "          but there are " << right_side.size() << " rows in the right side matrix" << endl;
		cout << "***************************************************************\n";
		exit(-8);
	}

	if(left_side[0].size() != right_side[0].size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << left_side[0].size() << " columns in the left side matrix," << endl;
		cout << "          but there are " << right_side[0].size() << " columns in the right side matrix" << endl;
		cout << "***************************************************************\n";
		exit(-9);
	}

	if(M_0.size() != left_side[0].size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << M_0.size() << " initial conditions," << endl;
		cout << "          but there are " << left_side[0].size() << " species" << endl;
		cout << "***************************************************************\n";
		exit(-10);
	}

	if(c_vector.size() != left_side.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << c_vector.size() << " kinetic paramaters," << endl;
		cout << "          but there are " << left_side.size() << " reactions" << endl;
		cout << "***************************************************************\n";
		exit(-11);
	} 

	t_vector = reader -> readByLine(t_vectorIn);

	// Optional paramaters
	if(strcmp(M_feedIn.c_str(), "NA") != 0)
	{
		M_feed = reader -> readByTab(M_feedIn);

	}
	else
	{
		for(int i = 0; i < M_0.size(); i++)
			M_feed.push_back(0);
	}

	if(M_0.size() != M_feed.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << M_0.size() << " different initial conditions," << endl;
		cout << "          but there are " << M_feed.size() << " different feed parameterizations" << endl;
		cout << "***************************************************************\n";
		exit(-13);
	}

	if(strcmp(cs_vectorIn.c_str(), "NA") != 0)
	{
		cs_vector = reader -> readByLineInt(cs_vectorIn);
	}
	else
	{
		for(int i = 0; i < M_0.size(); i++)
			cs_vector.push_back(i);
	}

	if(strcmp(atol_vectorIn.c_str(), "NA") != 0)
	{
		atol_vector = reader -> readByLine(atol_vectorIn);
	}
	else
	{
		for(int i = 0; i < M_0.size(); i++)
			atol_vector.push_back(1e-12);
	}

	if(strcmp(rtolIn.c_str(), "NA") != 0)
	{
		rtol =  reader -> readSingleValue(rtolIn);
	}

	if(strcmp(max_stepsIn.c_str(), "NA") != 0)
	{
		max_steps =  (int)(reader -> readSingleValue(max_stepsIn));
	}

	if(strcmp(ts_matrixIn.c_str(), "NA") != 0)
	{
		ts_matrix =  reader -> readMatrix(ts_matrixIn);
	
		if(cs_vector.size() != ts_matrix[0].size()-1)
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << " * ERROR: there are " << cs_vector.size() << " species to be sampled," << endl;
			cout << "          but there are " << ts_matrix[0].size()-1 << " in the target matrix" << endl;
			cout << "***************************************************************\n";
			exit(-101);
		}

		if(ts_matrix.size() != t_vector.size())
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << " * ERROR: ts_matrix and t_vector must be have the same length" << endl;
			cout << "***************************************************************\n";
			exit(-102);
		}

		for(int i=0; i < ts_matrix.size(); i++)
		{
			if(fabs(ts_matrix[i][0] - t_vector[i]) > 1e-5)
			{
				if(!verbose)
					cout << "***************************************************************\n";
				cout << " * ERROR: ts_matrix and t_vector must be have the same values" << endl;
				cout << "***************************************************************\n";
				exit(-103);
			}
		}

		fitness = true;
	}

	if(M_0.size() != atol_vector.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << M_0.size() << " species," << endl;
		cout << "          but there are " << atol_vector.size() << " values for absolute tolerances" << endl;
		cout << "***************************************************************\n";
		exit(-15);
	}

	for(int i=0; i < cs_vector.size(); i++)
	{
		if(cs_vector[i] > M_0.size()-1)
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << " * ERROR: there are " << M_0.size() << " species," << endl;
			cout << "          so the last species that can be sampled is " << M_0.size()-1<< endl;
			cout << "***************************************************************\n";
			exit(-16);
		}
	}

	// Convertion stochastic into deterministic
	if(modelkind)
		conversionToDeterministic(conversionFactor, M_0, M_feed, c_vector, left_side, verbose);

	if(verbose)
	{
		cout << "***************************************************************";
		cout << "\nFiCoS: Fine- and Coarse-grained Simulator (CPU)\n";
		cout << "\nReading BioSimWare project from the folder: " << pathIn.c_str() << endl;
		
		if(modelkind)
		{
			cout << " * Stochastic model detected" << endl;
			printf(" * Read volume: %.8e\n", volume);
			printf(" * Conversion constant: %.8e\n", conversionFactor);
			cout << " * Stochastic model converted to deterministic" << endl;

		}
		else
			cout << " * Deterministic model detected" << endl;

		cout << " * The model has " << left_side[0].size() << " species" << endl;
		cout << " * The model has " << left_side.size() << " reactions" << endl;
		cout << " * Feed conditions loaded" << endl;
		cout << " * " << cs_vector.size() << " species to be sampled" << endl;
		cout << " * " << t_vector.size() << " points to be sampled" << endl;
		cout << " * Absolute error tolerances set to " << atol_vector[0] << endl;
		cout << " * Relative error tolerance set to " << rtol << endl;
		cout << " * Maximum number of steps set to " << max_steps << endl;
	}

	int N          = M_0.size();
	int numSS      = cs_vector.size();
	int numTimes   = t_vector.size();

	vector<short2> H;
	vector<short2> ODE;
	vector<int> indexH;
	vector<int> indexODE;
	createH(left_side, right_side, H, indexH);
	createODEstruct(left_side, ODE, indexODE);

	// creating pointer for simulation
	boolFeed(M_feed);
	double *c_vectorPointer    = &c_vector[0];
	double *M_feedPointer      = &M_feed[0];
	double *t_vectorPointer    = &t_vector[0];
	double *atol_vectorPointer = &atol_vector[0];
	int *cs_vectorPointer      = &cs_vector[0];

	short2 *HPointer        = &H[0];
	short2 *ODEPointer      = &ODE[0];
	int *indexHPointer      = &indexH[0];
	int *indexODEPointer    = &indexODE[0];

	double* M_0PointerRADAU = new double[N];
	double* M_0PointerDOPRI = new double[N];
	for(int i=0; i < N; i++)
	{
		M_0PointerDOPRI[i] = M_0[i];
		M_0PointerRADAU[i] = M_0[i];
	}

	if(verbose > 1)
	{
		cout << endl;
		cout << " * ODEs:" << endl;
		printODEs(N, HPointer, ODEPointer, indexHPointer, indexODEPointer, M_feedPointer);
	}
	if(verbose > 2)
	{
		cout << endl;
		cout << " * Jacobian matrix:" << endl;
		printJacobian(N, HPointer, ODEPointer, indexHPointer, indexODEPointer, M_feedPointer);
	}

	/* ******************** Estimation of a bound on the dominant eigenvalue ******************** */
	double *Jac   = new double[N*N];
	double *norms = new double[N];

	evaluateJAC(N, HPointer, ODEPointer, indexHPointer, indexODEPointer, 
		M_feedPointer, c_vectorPointer, M_0PointerRADAU, Jac);

	double eig = calculateBound(N, Jac, norms);

	if(verbose>2)
		printf(" * The model has the dominant eigenvalue equal to %5.2f\n", eig);



	// allocazione della matrice per salvare le soluzioni
	double** dynamics = new double*[numTimes];
	for (int i = 0; i < numTimes; i++)
		dynamics[i] = new double[numSS+1];

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	
	if(eig < 500)
	{
		if(verbose)
			cout << "\n * Launching DOPRI method\n";

		Dopri dopri = Dopri(N, numSS, numTimes, M_0PointerDOPRI, t_vectorPointer, cs_vectorPointer,
			atol_vectorPointer, HPointer, ODEPointer, indexHPointer, indexODEPointer,
			c_vectorPointer, M_feedPointer, dynamics, rtol, max_steps, verbose);

		int dopriRet = dopri.Integrate();

		if(dopriRet == 0)
		{
			if(verbose > 2)
			{
				printf(" * DOPRI number of function evaluations: %d\n"
					   "   number of attempted steps: %d\n"
					   "   number of accepted steps: %d\n"
					   "   number of rejected steps: %d\n",
					   dopri.NumFunction(), dopri.NumStep(), dopri.NumAccept(), dopri.NumReject());
			}
		}
		else
		{
			if(verbose)
				cout << "\n * Launching RADAU method\n";

			Radau radau(N, numSS, numTimes, M_0PointerRADAU, t_vectorPointer, cs_vectorPointer,
				atol_vectorPointer, HPointer, ODEPointer, indexHPointer, indexODEPointer,
				c_vectorPointer, M_feedPointer, dynamics, rtol, max_steps, verbose);
			radau.Integrate();
			
			if(verbose > 2)
			{
				printf(" * RADAU number of function evaluations: %d\n"
			       "   number of jacobian evaluations: %d\n"
			       "   number of attempted steps: %d\n"
			       "   number of accepted steps: %d\n"
			       "   number of rejected steps: %d\n"
			       "   number of lu-decompositions: %d\n"
			       "   number of linear systems resolutions: %d\n",
			       radau.NumFunction(), radau.NumJacobian(), radau.NumStep(), radau.NumAccept(),
			       radau.NumReject(), radau.NumDecomp(), radau.NumSol());
			}

		}
	}

	else
	{
		if(verbose)
			cout << "\n * Launching RADAU method\n";

		Radau radau(N, numSS, numTimes, M_0PointerRADAU, t_vectorPointer, cs_vectorPointer,
				atol_vectorPointer, HPointer, ODEPointer, indexHPointer, indexODEPointer,
				c_vectorPointer, M_feedPointer, dynamics, rtol, max_steps, verbose);
			radau.Integrate();


		if(verbose > 2)
		{
			printf(" * RADAU number of function evaluations: %d\n"
		       "   number of jacobian evaluations: %d\n"
		       "   number of attempted steps: %d\n"
		       "   number of accepted steps: %d\n"
		       "   number of rejected steps: %d\n"
		       "   number of lu-decompositions: %d\n"
		       "   number of linear systems resolutions: %d\n",
		       radau.NumFunction(), radau.NumJacobian(), radau.NumStep(), radau.NumAccept(),
		       radau.NumReject(), radau.NumDecomp(), radau.NumSol());
		}
	}

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	double elapsed = duration<double>( t2 - t1 ).count();

	if(!fitness)
	{
		cout << endl;
		printf("Integration time: %.8f\n", elapsed);

		FILE * fl;
		string path = pathOut + slash + "Solution";
		fl=fopen(path.c_str(), "w");

		if( fl==NULL )
		{
			printf("Error open output file: %s\n", path.c_str());
			exit(-9);
		}

		if(t_vector[0] == 0.0)
		{
			fprintf(fl, "%.8e\t", t_vector[0]);
			for(int j = 0; j < numSS; j++)
				fprintf(fl, "%.8e\t", M_0[cs_vector[j]]);
			fprintf(fl, "\n");

			for(int i = 1; i < numTimes; i++)
			{
				for(int j = 0; j < numSS+1; j++)
				{
					if(j<numSS)
						fprintf(fl, "%.8e\t", dynamics[i][j]);
					else
						fprintf(fl, "%.8e", dynamics[i][j]);
				}
				fprintf(fl, "\n");
			}
		}
		else
		{
			for(int i = 0; i < numTimes; i++)
			{
				for(int j = 0; j < numSS+1; j++)
				{
					if(j<numSS)
						fprintf(fl, "%.8e\t", dynamics[i][j]);
					else
						fprintf(fl, "%.8e", dynamics[i][j]);
				}
				fprintf(fl, "\n");
			}
		}
		fclose(fl);
	}
	else
	{
		double fitness  = 0.0;
		double valueTS  = 0.0;
		double valueSIM = 0.0;
		for(int i = 0; i < numTimes; i++)
		{
			for(int j = 1; j < numSS+1; j++)
			{
				valueTS = ts_matrix[i][j];
				valueSIM = dynamics[i][j];
				
				valueTS  = max(valueTS, 1e-16);
				valueSIM = max(valueSIM, 1e-16);
				fitness += abs(valueTS - valueSIM) / valueTS;
			}
		}

		printf("%.8e\n", fitness);

	}
}

/* ******************** Convertion stochastic into deterministic ******************** */
int count_nonzeroRow(vector<vector<short> > A, int i)
{
	int count = 0;
	for(int j = 0; j < A[0].size(); j++)
	{
		if(A[i][j] > 0)
			count++;
	}
	return count;
}

void conversionToDeterministic(double conversionFactor, vector<double> &M_0,
	vector<double> &M_feed, vector<double> &c_vector,
	vector<vector<short> > left_side, int verbose)
{

	for(int i=0; i < M_0.size(); i++)
	{
		M_0[i]    = M_0[i]/conversionFactor;
		M_feed[i] = M_feed[i]/conversionFactor;
	}

	for(int i=0; i < c_vector.size(); i++)
	{

		int value = count_nonzeroRow(left_side, i);
		if(value >= 3)
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << "There are one or more reactions with order higher wrt the second" << endl;
			cout << "***************************************************************\n";
			exit(-17);
		}
		else
		{
			int om = 0;
			for(int j = 0; j < left_side[0].size(); j++)
			{
				if(left_side[i][j] >= 2)
				{
					c_vector[i] =  (c_vector[i] * conversionFactor)/2.0;
					om = 1;
					break;
				}
			}
			if(om == 0)
			{
				if(count_nonzeroRow(left_side, i) > 1)
				{
					c_vector[i] =  (c_vector[i] * conversionFactor);
				}
			}
		}
	}
}

/* ******************** Methods to creaye ODEs structures ******************** */

void createH(vector<vector<short> > A, vector<vector<short> > B, vector<short2> &H1, vector<int> &index)
{
	vector<short> vect;
	vector<vector<short> > H;

	for(int j = 0; j < A[0].size(); j++)
	{
		vect.clear();
		for(int i = 0; i < A.size(); i++)
		{
			int temp = B[i][j] - A[i][j];
			vect.push_back(temp);
		}
		H.push_back(vect);
	}

	index.push_back(0);
	int count = 0;
	for(int i = 0; i < H.size(); i++)
	{
		for(int j = 0; j < H[0].size(); j++)
		{
			if(H[i][j] != 0)
			{
				short2 tmp;
				tmp.x = j;
				tmp.y = H[i][j];
				H1.push_back(tmp);
				count++;
			}
		}
		index.push_back(count);
	}
}

void createODEstruct(vector<vector<short> > A, vector<short2> &ODE, vector<int> &index)
{
	index.push_back(0);
	int idx = 0;
	for(int i = 0; i < A.size(); i++)
	{
		for(int j = 0; j < A[0].size(); j++)
		{
			if(A[i][j] != 0)
			{
				short2 tmp;
				tmp.x = j;
				tmp.y = A[i][j];
				ODE.push_back(tmp);
				idx ++;
			}
		}
		index.push_back(idx);
	}
}

void boolFeed(vector<double> &vect)
{
	for(int i=0; i < vect.size(); i++)
	{
		if(vect[i] > 0)
			vect[i] = 0.0;
		else
			vect[i] = 1.0;
	}
}

double calculateBound(int N, double* Jac, double* norms)
{	
	double value;
	for(int i=0; i < N; i++)
	{
		value = 0.0;
		for(int j=0; j < N; j++)
		{
			value += abs(Jac[i*N + j]);
		}
		norms[i] = value;
	}

	double eig = norms[0];
	for(int i=1; i < N; i++)
	{
		if(norms[i] > eig)
			eig = norms[i];
	}
	return eig;
}

void evaluateODE(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy)
{
	for (int idB=0; idB < N; idB++)
	{
		int startH   = indexH[idB];
		int endH     = indexH[idB+1];

		int start    = 0;
		int end      = 0;
		int idx      = 0;
		int exp      = 0;
		double coeff = 0.0;
		double tmp   = 0.0;

		dy[idB] = 0.0;

		for(int i=startH; i < endH; i++)
		{
			coeff = H[i].y;
			tmp   = coeff*c_vector[H[i].x];

			start = indexODE[H[i].x];
			end   = indexODE[H[i].x+1];
			for(int j=start; j < end; j++)
			{
				idx = ODE[j].x;
				exp = ODE[j].y;
				for(int k=0; k < exp; k++)
					tmp = tmp * y[idx];
			}
			dy[idB] = dy[idB] + tmp;
		}
		dy[idB] = dy[idB]*M_feed[idB];
	}
}

void evaluateJAC(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy)
{
	for (int idB=0; idB < N; idB++)
	{
		int startH   = indexH[idB];
		int endH     = indexH[idB+1];
		int start    = 0;
		int end      = 0;
		int idx      = 0;
		int exp      = 0;
		int idx1     = 0;
		int exp1     = 0;
		double tmp   = 0.0;
		double coeff = 0.0;

		for(int row=0; row < N; row++)
		{
			dy[idB*N + row] = 0.0;
			for(int i=startH; i < endH; i++)
			{
				tmp = 0.0;
				coeff = H[i].y;

				start = indexODE[H[i].x];
				end   = indexODE[H[i].x+1];
				for(int j=start; j < end; j++)
				{
					idx = ODE[j].x;
					exp = ODE[j].y;
					if(idx == row)
					{
						tmp = coeff*exp*c_vector[H[i].x];
						for(int k=0; k < exp-1; k++)
							tmp = tmp * y[idx];

						for(int k1 = start; k1 < end; k1++)
						{
							idx1 = ODE[k1].x;
							exp1 = ODE[k1].y;
							if(idx1 != row)	
								for(int k=0; k < exp1; k++)
									tmp = tmp * y[idx1];
						}
					}
				}
				dy[idB*N + row] = dy[idB*N + row] + tmp;
			}

			dy[idB*N + row] = dy[idB*N + row]*M_feed[idB];
		}
	}
}

void printODEs(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *feed)
{	
	string s     = "";
	double coeff = 0.0;
	
	for(int gid = 0; gid < N; gid++)
	{
		
		s = "";
		int startH = indexH[gid];
		int endH   = indexH[gid+1];

		for(int i=startH; i < endH; i++)
		{
			coeff = H[i].y;

			if(coeff > 0)
				s = s + "+" + to_string((int)coeff) + "*" + "k[" + to_string(H[i].x) + "]";
			else
				s = s + "" + to_string((int)coeff) + "*" + "k[" + to_string(H[i].x) + "]";

			int start = indexODE[H[i].x];
			int end   = indexODE[H[i].x+1];
			for(int j=start; j < end; j++)
			{
				int idx = ODE[j].x;
				int exp = ODE[j].y;
				for(int k=0; k < exp; k++)
					s = s + "*" + "y[" + to_string(idx) +"]";
			}
		}

		if(feed[gid] == 0)
			s = "";

		if(strcmp(s.c_str(), "") == 0)
			printf(" dy[%d]/dt = 0\n", gid);
		else
			printf(" dy[%d]/dt = %s\n", gid, s.c_str());

	}
}

void printJacobian(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *feed)
{
	string s     = "";
	double coeff = 0.0;
	
	for(int gid = 0; gid < N; gid++)
	{
		for(int row=0; row < N; row++)
		{
			s = "";
			int startH = indexH[gid];
			int endH   = indexH[gid+1];
			for(int i=startH; i < endH; i++)
			{
				coeff = H[i].y;
				int start = indexODE[H[i].x];
				int end   = indexODE[H[i].x+1];
				for(int j=start; j < end; j++)
				{
					int idx = ODE[j].x;
					int exp = ODE[j].y;
					if(idx == row)
					{
						if(coeff > 0)
							s = s + "+" + to_string((int)coeff*exp) + "*" + "k[" + to_string(H[i].x) + "]";
						else
							s = s + "" + to_string((int)coeff*exp) + "*" + "k[" + to_string(H[i].x) + "]";

						for(int k=0; k < exp-1; k++)
							s = s + "*" + "y[" + to_string(idx) +"]";

						for(int k1 = start; k1 < end; k1++)
						{
							int idx1 = ODE[k1].x;
							int exp1 = ODE[k1].y;
							if(idx1 != row)	
								for(int k=0; k < exp1; k++)
									s = s + "*" + "y[" + to_string(idx1) +"]";
						}
					}
				}
			}

			if(feed[gid] == 0)
				s = "";

			if(strcmp(s.c_str(), "") == 0)
				printf(" dy[%d][%d]/dt = 0\n", gid, row);
			else
				printf(" dy[%d][%d]/dt = %s\n", gid, row, s.c_str());
		}
	}
	printf("\n");
}