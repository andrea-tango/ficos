/***************************************************************************
  written by    : Andrea Tangherloni
  last updated  : May 25, 2021
  e-mail:     : andrea.tangherloni@unibg.it

***************************************************************************/

#ifndef STRUTCTURE_H_INCLUDED
#define STRUTCTURE_H_INCLUDED


struct short4
{
  short x;
  short y;
  short z;
  short w;
};
struct short2
{
  short x;
  short y;
};

#endif 