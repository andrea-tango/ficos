/***************************************************************************
	written by		: Andrea Tangherloni
	last updated	: Jan 15, 2021
	e-mail:			: andrea.tangherloni@unibg.it

This is a simplified, improved and corrected version of the C++ porting of RADAU5

	written by 		: Blake Ashby
	last updated	: Nov 15, 2002
	e-mail:			: bmashby@stanford.edu

tailored to mechanism based model of biological systems.

This code is written in C++ and is a modification of the code originally written 
in Fortran (version of July 9, 1996, latest small correction: January 18, 2002) by:

	E. Hairer and G. Wanner
	Universite de Geneve, Dept. de Mathematiques
	Ch-1211 Geneve 24, Switzerland
	E-mail:  ernst.hairer@math.unige.ch
	gerhard.wanner@math.unige.ch

RADAU5 is described in the book:

	E. Hairer and G. Wanner, Solving Ordinary Differential
	Equations II. Stiff and Differential-Algebraic Problems.
	Springer Series in Computational Mathematics 14,
	Springer-Verlag 1991, Second Edition 1996.

This code computes the numerical solution of a stiff system of first order
Ordinary Differential Equations:

					y'=f(x,y).

The method used is an implicit Runge-Kutta method (RADAU IIA) of order 5 with
step size control and continuous output. (See Section IV.8 of Hairer and Wanner).

***************************************************************************/

#ifndef RADAU_H_INCLUDED
#define RADAU_H_INCLUDED

#include "libraries.h"
#include "structure.h"
using namespace std;

extern "C"
{
	void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);
	void dgetrs_(char* C, int* N, int* NRHS, double* A, int* LDA, int* IPIV, double* B, int* LDB, int* INFO);
	void zgetrf_(int* M, int *N, complex<double>* A, int* lda, int* IPIV, int* INFO);
	void zgetrs_(char* C, int* N, int* NRHS, complex<double>* A, int* LDA, int* IPIV, double* B, int* LDB, int* INFO);
}

void evaluateODE(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *M_feed, double *c_vector, double *y, double *dy);
void evaluateJAC(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy);

class Radau
{
public:

	Radau(int N, int numSS, int numTimes, double yIn[], double t_vectorIn[], int cs_vectorIn[],
		double atol[], short2 HIn[], short2 ODEIn[], int indexHIn[], int indexODEIn[],
		double c_vectorIn[], double feedIn[], double** dynamicsIn,
		double rtol, int maxSteps, int verboseIn, int newtonIt=7);

	~Radau();

	void Integrate();
	// get number of Jacobian evaluations
	int NumJacobian() const { return njac; }
	// get number of lu-decompositions of both matrices
	int NumDecomp() const { return ndec; }
	// get number of forward-backward substitutions, of both systems;
	int NumSol() const { return nsol; }
	// get number of function evaluations
	int NumFunction() const { return nfcn; }
	// get number of attempted steps
	int NumStep() const { return nstep; }
	// get number of accepted steps
	int NumAccept() const { return naccpt; }
	// get number of rejected steps
	int NumReject() const { return nrejct; }

private:
	// member routines
	int CoreIntegrator();
	double ContinuousOutput(unsigned i);
	void ComputeJacobian();
	void SolutionOutput();

	// Linear Algebra routines:
	int DecompReal();
	int DecompComplex();
	int LinearSolve();
	int ErrorEstimate();

	short2* H;
	short2* ODE;
	int* indexH;
	int* indexODE;
	double* c_vector;
	double* feed;
	double* t_vector;
	int* cs_vector;
	double **dynamics;

	int verbose;
	// number of Jacobian evaluations
	int njac;
	// number of lu-decompositions of both matrices
	int ndec;
	int nsol;
	// number of function evaluations
	int nfcn;
	// number of attempted steps
	int nstep;
	// number of accepted steps
	int naccpt;
	// number of rejected steps
	int nrejct;

	int nit;

	// dimension of system
	int n;
	int numSS;
	int numTimes;
	int ind;
	// vector for y values
	double *y;
	// independent variable (usually time)
	double t;
	// final value for independent variable
	double tend;

	// relative error tolerance
	double rtoler;
	// absolute error tolerance
	double* atoler;
	// integration step length
	double h;

	// maximal step size
	double hmax;
	// maximal number of steps
	int nmax;
	// smallest number satisfying 1.0 + uround > 1.0
	double uround;
	// safety factor in step size prediction
	double safe;
	// facl, facr--parameters for step size selection
	double facl;
	double facr;

	// stores past value of t
	double tOld;
	// stores past value of h
	double hOld;
	// t at discrete points specified by dt interval
	double tD;

	// stopping criterion for Newton's method, usually chosen < 1
	double fnewt;
	// quot1 and quot2--if quot1 < hnew/hold < quot2, step size = const
	double quot;
	double quot1;
	double quot2;
	// decides whether the Jacobian should be recomputed
	double thet;

	// constants that are used in linear algebra routines
	double fac1;
	double alphn;
	double betan;

	// variables used in program
	double err;
	bool startn;
	bool caljac;
	bool first;
	bool reject;
	bool pred;

	// arrays used in program
	double *z;
	double *z1;
	double *z2;
	double *z3;
	double *y0;
	double *scal;
	double *f1;
	double *f2;
	double *f3;
	double *cont;
	int *ip1;
	int *ip2;
	double *e1;
	int info;
	int one;
	char trasp;
	complex<double> *e2r;
	double* arrayJac;

};

#endif // RADAU_H_INCLUDED
