/***************************************************************************
  written by    : Andrea Tangherloni
  last updated  : May 25, 2021
  e-mail:     : andrea.tangherloni@unibg.it

***************************************************************************/

#ifndef SIMULATION_H_INCLUDED
#define SIMULATION_H_INCLUDED

#include "dopri.h"
#include "radau.h"

using namespace std;

void runSimulation(string pathIn, string pathOut, string left_sideIn,
				string right_sideIn, string t_vectorIn, string M_0In,
				string c_vectorIn, string M_feedIn, string modelkindIn,
				string cs_vectorIn, string atol_vectorIn, string rtolIn,
				string max_stepsIn, string ts_matrixIn, int verbose);

int count_nonzeroRow(vector<vector<short> > A, int i);

void conversionToDeterministic(double conversionFactor, vector<double> &M_0,
	vector<double> &M_feed, vector<double> &c_vector,
	vector<vector<short> > left_side, int verbose);


void createH(vector<vector<short> > A, vector<vector<short> > B, vector<short2> &H1, vector<int> &index);
void createODEstruct(vector<vector<short> > A, vector<short2> &ODE, vector<int> &index);

void boolFeed(vector<double> &vect);

double calculateBound(int N, double* Jac, double* norms);

void evaluateODE(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy);
void evaluateJAC(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy);

void printODEs(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *feed);
void printJacobian(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *feed);

#endif // SIMULATION_H_INCLUDED