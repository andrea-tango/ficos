/***************************************************************************
	written by		: Andrea Tangherloni
	last updated	: May 25, 2021
	e-mail:			: andrea.tangherloni@unibg.it

The original code computes the numerical solution of a system of first order ordinary
differential equations y'=f(x,y). It uses an explicit Runge-Kutta method of
order (4)5 due to Dormand & Prince with step size control and dense output.

This is a simplified and improved version of the C++ porting of DOPRI

	written by 		: Blake Ashby
	last updated	: Nov 15, 2002
	e-mail:			: bmashby@stanford.edu

tailored to mechanism based model of biological systems.

This code is written in C++ and is a modification of the code originally written 
in Fortran (Version of April 28, 1994) by:

	E. Hairer & G. Wanner
	Universite de Geneve, dept. de Mathematiques
	CH-1211 GENEVE 4, SWITZERLAND
	E-mail : HAIRER@DIVSUN.UNIGE.CH, WANNER@DIVSUN.UNIGE.CH

and adapted for C by:
	J.Colinge (COLINGE@DIVSUN.UNIGE.CH).

The code is described in : E. Hairer, S.P. Norsett and G. Wanner, Solving
ordinary differential equations I, nonstiff problems, 2nd edition,
Springer Series in Computational Mathematics, Springer-Verlag (1993).

***************************************************************************/

#ifndef DOPRI_H_INCLUDED
#define DOPRI_H_INCLUDED

#include "libraries.h"
#include "structure.h"
using namespace std;

void evaluateODE(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *M_feed, double *c_vector, double *y, double *dy);

class Dopri
{
public:

	Dopri(int N, int numSS, int numTimes, double yIn[], double t_vectorIn[], int cs_vectorIn[],
	double atol[], short2 HIn[], short2 ODEIn[], int indexHIn[], int indexODEIn[],
	double c_vectorIn[], double feedIn[], double** dynamicsIn, double rtol, int maxSteps, int verboseIn);

	~Dopri();

	int Integrate();

	int NumFunction() const { return nfcn; }
	// get number of attempted steps
	int NumStep() const { return nstep; }
	// get number of accepted steps
	int NumAccept() const { return naccpt; }
	// get number of rejected steps
	int NumReject() const { return nrejct; }


private:

	double sign(double a, double b);
	int CoreIntegrator();
	double ContinuousOutput(unsigned i);
	void SolutionOutput();
	
	// determine initial step size, h
	double hinit();
	short2* H;
	short2* ODE;
	int* indexH;
	int* indexODE;
	double* c_vector;
	double* feed;
	double* t_vector;
	int* cs_vector;
	double **dynamics;

	// for stabilized step-size control
	double beta;
	// test for stiffness
	int nstiff;

	int verbose;
	// number of function evaluations
	int nfcn;
	int nstep;
	int nrejct;
	int naccpt;

	// dimension of system
	int n;
	int numSS;
	int numTimes;
	int ind;
	// vector for y values
	double *y;
	// independent variable (usually time)
	double t;
	// final value for independent variable
	double tend;

	// relative error tolerance
	double rtoler;
	// absolute error tolerance
	double* atoler;
	// integration step length
	double h;

	// maximal step size
	double hmax;
	// maximal number of steps
	int nmax;
	// smallest number satisfying 1.0 + uround > 1.0
	double uround;
	// safety factor in step size prediction
	double safe;
	// facl, facr--parameters for step size selection
	double facl;
	double facr;

	// stores past value of t
	double tOld;
	// stores past value of h
	double hOld;
	// t at discrete points specified by dx interval
	double tD;

	// working arrays used for dense output
	double *rcont1;
	double *rcont2;
	double *rcont3;
	double *rcont4;
	double *rcont5;
	
	// vectors used in integration steps
	double *f;
	double *yy1;
	double *k1;
	double *k2;
	double *k3;
	double *k4;
	double *k5;
	double *k6;
	double *ysti;
};
#endif // DOPRI_H_INCLUDED