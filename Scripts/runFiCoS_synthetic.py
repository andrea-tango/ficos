import os
import math
import numpy as np 
import random as rnd
from glob import glob
from subprocess import *

def generateParameters(c_vector):

	ret = []
	for i in range(len(c_vector)):

		minimo  = math.log(c_vector[i] - 0.5*c_vector[i])
		massimo = math.log(c_vector[i] + 0.5*c_vector[i])
		ret.append(math.exp(minimo+(massimo-minimo)*rnd.random()))

	return ret


def runFiCoS(pathModel, out_dir, n_sim):

	c_vector = np.loadtxt(pathModel+os.sep+"c_vectorBase")

	# perturbation of the initial kinetic parameters
	c_matrix = []
	for i in range(n_sim):
		X = generateParameters(c_vector)
		c_matrix.append(X)
	np.savetxt(pathModel+os.sep+"c_matrix", c_matrix, delimiter="\t")

	# repeat the initial conditions
	M_0  = np.loadtxt(model+os.sep+"M_0")
	MX_0 = np.tile(M_0, (n_sim,1))
	np.savetxt(pathModel+os.sep+"MX_0", MX_0, delimiter="\t")

	# run FiCoS and parse the console results
	fullcall  = ["./../FiCoS_gpu",
				 pathModel,
				 out_dir+os.sep+str(n_sim)]

	risultato = check_output(fullcall)
	risultato = risultato.split()

	return float(risultato[3]), float(risultato[6])

if __name__ == '__main__':

	folders = glob(".."+os.sep+"Models"+os.sep+"Synthetic"+os.sep+"*")

	simulations = [1, 32, 64, 128, 256, 512, 1024, 2048]

	for folder in folders:
		print("#"*150)
		print(" * Analysing folder %s"%folder)
		models = glob(folder+os.sep+"*")

		for model in models:
			print("*"*150)
			print(" * Simulating model %s"%model.split(os.sep)[-1])
			print()

			for n_sim in simulations:
				print(" * Number of simulations %d"%n_sim)

				out_dir = "out"+os.sep+model.split(os.sep)[-2]+os.sep+model.split(os.sep)[-1]

				if not os.path.exists(out_dir):
					os.makedirs(out_dir)

				integrationTime, simulationTime = runFiCoS(model, out_dir, n_sim)
				print(" * Integration time %.4e - simulation time %.4e"%(integrationTime, simulationTime))
				print()