set grid
# set autoscale fix
# set bmargin 5
# set lmargin 10
# set bmargin 3
set key off
# set xtics font ", 4"
# set ytics font ", 4"
# set ztics font ", 4"
set format x "%.1e"
set format y "%.1e"
set format z "%.1e"
set cbtic font "Times,10"
set ytics 1.0e-09,2.0e-07,1.0e-06
set xtics 0, 18000, 90000
set xyplane 0.0
set xtics offset 0,-1.25,0
set ytics offset 0,-0.75,0

set size 1,1

set xlabel "AMPK" font "Times,12" offset 1,-1.5,0
set ylabel "P_9" font "Times,12" offset 1,-1,0 
set zlabel "EIF4EBP oscillations amplitude" font "Times,12"  rotate by 90 offset -4,0,0
splot 'ampEIF_FiCoS.tsv'  using 1:2:3 with points palette pointsize 0.5 pointtype 7

# set xlabel "AMPK" font "Times,12" offset 1,-1.5,0
# set ylabel "P_9" font "Times,12" offset 1,-1,0 
# set zlabel "AMBRA oscillations amplitude" font "Times,12"  rotate by 90 offset -4,0,0
# splot 'ampAMP_FiCoS.tsv'  using 1:2:3 with points palette pointsize 0.5 pointtype 7

# set xlabel "AMPK" font "Times,12" offset 1,-1.5,0
# set ylabel "P_9" font "Times,12" offset 1,-1,0 
# set zlabel "EIF4EBP oscillations amplitude" font "Times,12"  rotate by 90 offset -4,0,0
# splot 'ampEIF_LSODA.tsv'  using 1:2:3 with points palette pointsize 0.5 pointtype 7

# set xlabel "AMPK" font "Times,12" offset 1,-1.5,0
# set ylabel "P_9" font "Times,12" offset 1,-1,0 
# set zlabel "AMBRA oscillations amplitude" font "Times,12"  rotate by 90 offset -4,0,0
# splot 'ampAMP_LSODA.tsv'  using 1:2:3 with points palette pointsize 0.5 pointtype 7

# set xlabel "AMPK" font "Times,12" offset 1,-1.5,0
# set ylabel "P_9" font "Times,12" offset 1,-1,0 
# set zlabel "EIF4EBP oscillations amplitude" font "Times,12"  rotate by 90 offset -4,0,0
# splot 'ampEIF_VODE.tsv'  using 1:2:3 with points palette pointsize 0.5 pointtype 7

# set xlabel "AMPK" font "Times,12" offset 1,-1.5,0
# set ylabel "P_9" font "Times,12" offset 1,-1,0 
# set zlabel "AMBRA oscillations amplitude" font "Times,12"  rotate by 90 offset -4,0,0
# splot 'ampAMP_VODE.tsv'  using 1:2:3 with points palette pointsize 0.5 pointtype 7