import os
import numpy as np
import random as rnd
from subprocess import *
from math import *
from random import randint
import shutil

def generateParamConc(pathModel, numPoints=36864, minCon=0, maxConc=90000, minParam=1e-9, maxParam=1e-6):

	c_vector = np.loadtxt(pathModel+os.sep+"c_vectorBase")
	M_0      = np.loadtxt(pathModel+os.sep+"M_0Base")

	c_matrix = np.zeros((numPoints, len(c_vector)))
	MX_0     = np.zeros((numPoints, len(M_0)))

	step = int(sqrt(numPoints))

	stepConc  = (maxConc - minCon)/float((step-1))
	stepParam = (maxParam - minParam)/float((step-1))


	for i in range(step):
		for j in range(step):
			# generate c_matrix
			for k in range(len(c_vector)):
				if c_vector[k] == 0:
					c_matrix[i*step+j][k] = minParam+stepParam*j
				else:
					c_matrix[i*step+j][k] = c_vector[k]

			# generate MX_0
			for k in range(len(M_0)):
				if k == 4:
					MX_0[i*step+j][k] = minCon+stepConc*i
				else:
					MX_0[i*step+j][k] = M_0[k]


	np.savetxt(pathModel+os.sep+"c_matrixTot", c_matrix, delimiter="\t")
	np.savetxt(pathModel+os.sep+"MX_0Tot", MX_0, delimiter="\t")

	return c_matrix, MX_0

def runFiCoS(model, out_dir, c_matrixTot, MX_0Tot, numPoints=64, numParalSim=32):

	steps = numPoints//numParalSim+1

	integrationTime = 0
	simulationTime  = 0
	print("\n * Running FiCoS")
	for i in range(1, steps+1):
		np.savetxt(model+os.sep+"c_matrix", c_matrixTot[(i-1)*numParalSim:i*numParalSim, :], delimiter="\t")
		np.savetxt(model+os.sep+"MX_0",     MX_0Tot[(i-1)*numParalSim:i*numParalSim, :], delimiter="\t")
		
		step = MX_0Tot[(i-1)*numParalSim:i*numParalSim, :].shape[0]

		if (i-1)*numParalSim == len(c_matrixTot):
			break

		print(" \t * running simulations from %5d to %5d"%((i-1)*numParalSim, (i-1)*numParalSim+step-1))

		# call FiCoS
		fullcall  = ["./../FiCoS_gpu",
					model,
					out_dir+os.sep+"sims_%d"%((i-1)*numParalSim)
					]
	
		risultato = check_output(fullcall)
		risultato = risultato.split()

		integrationTime += float(risultato[3])
		simulationTime  += float(risultato[6])

	print("*"*100)
	return integrationTime, simulationTime

def LSODA(model, out_dir, c_matrixTot, MX_0Tot, maxTime, numPoints):

	integrationTime = 0
	simulationTime  = 0

	print("\n * Running LSODA")
	listIndexes = []
	for i in range(len(c_matrixTot)):
		
		idx = randint(0, numPoints-1)
		while idx in listIndexes:
			idx = randint(0, numPoints-1)
		listIndexes.append(idx)
		
		np.savetxt(model+os.sep+"c_vector", c_matrixTot[idx], delimiter="\n")
		with open(model+os.sep+"M_0", "w") as fo:
			for j in range(len(MX_0Tot[idx])):
				if j == len(MX_0Tot[idx])-1:
					fo.write("%f"%MX_0Tot[idx][j])
				else:
					fo.write("%f\t"%MX_0Tot[idx][j])

		print(" \t * Running simulation number %5d out of %5d - index %5d"%(i, numPoints, idx))
		
		# call LSODA
		fullcall  = ["python",
					 ".."+os.sep+"Simulators"+os.sep+"simulator.py",
					 model,
					 out_dir+os.sep+"out_%d"%(idx),
					 "-l"]

		risultato = check_output(fullcall)
		risultato = risultato.split()

		integrationTime += float(risultato[8])
		simulationTime  += float(risultato[11])

		if simulationTime >= maxTime:
			return integrationTime, simulationTime, listIndexes

	return integrationTime, simulationTime, listIndexes

def VODE(model, out_dir, c_matrixTot, MX_0Tot, maxTime, numPoints, listIndexes):

	integrationTime = 0
	simulationTime  = 0

	print("\n * Running VODE")
	for i in range(len(listIndexes)):
		
		idx = listIndexes[i]
		np.savetxt(model+os.sep+"c_vector", c_matrixTot[idx], delimiter="\n")
		with open(model+os.sep+"M_0", "w") as fo:
			for j in range(len(MX_0Tot[idx])):
				if j == len(MX_0Tot[idx])-1:
					fo.write("%f"%MX_0Tot[idx][j])
				else:
					fo.write("%f\t"%MX_0Tot[idx][j])

		print(" \t * Running simulation number %5d out of %5d - index %5d"%(i, numPoints, idx))
		
		# call VODE
		fullcall  = ["python",
					 ".."+os.sep+"Simulators"+os.sep+"simulator.py",
					 model,
					 out_dir+os.sep+"out_%d"%(idx),
					 "-v"]

		risultato = check_output(fullcall)
		risultato = risultato.split()

		integrationTime += float(risultato[8])
		simulationTime  += float(risultato[11])

		if simulationTime >= maxTime:
			return integrationTime, simulationTime

		for i in range(len(c_matrixTot)):
			
			idx = randint(0, numPoints-1)
			while idx in listIndexes:
				idx = randint(0, numPoints-1)
			listIndexes.append(idx)
			
			np.savetxt(model+os.sep+"c_vector", c_matrixTot[idx], delimiter="\n")
			with open(model+os.sep+"M_0", "w") as fo:
				for j in range(len(MX_0Tot[idx])):
					if j == len(MX_0Tot[idx])-1:
						fo.write("%f"%MX_0Tot[idx][j])
					else:
						fo.write("%f\t"%MX_0Tot[idx][j])

			print(" \t * Running simulation number %5d out of %5d - index %5d"%(i, numPoints, idx))
			
			# call VODE
			fullcall  = ["python",
						 ".."+os.sep+"Simulators"+os.sep+"simulator.py",
						 model,
						 out_dir+os.sep+"out_%d"%(idx),
						 "-v"]

			risultato = check_output(fullcall)
			risultato = risultato.split()

			integrationTime += float(risultato[8])
			simulationTime  += float(risultato[11])

			if simulationTime >= maxTime:
				return integrationTime, simulationTime

	return integrationTime, simulationTime

if __name__ == '__main__':
	
	model         = ".."+os.sep+"Models"+os.sep+"Authophagy"
	numParalSim   = 512
	numPoints     = 36864
	out_dir       = "resultsPSA"

	print("*"*100)
	print(" * Generating PSA conditions")
	c_matrixTot, MX_0Tot = generateParamConc(model, numPoints=numPoints)
	print(" * PSA conditions genereted")
	print ("*"*100)

	if not os.path.exists(out_dir):
		os.makedirs(out_dir)

	shutil.copy(model+os.sep+"c_matrixTot", out_dir+os.sep+"c_matrixTot")
	shutil.copy(model+os.sep+"MX_0Tot",     out_dir+os.sep+"MX_0Tot")

	integrationTimeFiCoS, simulationTimeFiCoS = runFiCoS(model, out_dir, c_matrixTot, MX_0Tot, numPoints=numPoints, numParalSim=numParalSim)
	print("\n * FiCos -> Integration time %5.4fs - simulation time %5.4fs"%(integrationTimeFiCoS, simulationTimeFiCoS))
	print("*"*100)
	
	integrationTimeLSODA, simulationTimeLSODA, listIndexes = LSODA(model, out_dir, c_matrixTot, MX_0Tot, simulationTimeFiCoS, numPoints)
	print("\n * lsoda -> Integration time %5.4fs - simulation time %5.4fs"%(integrationTimeLSODA, simulationTimeLSODA))
	print("*"*100)

	integrationTimeVODE, simulationTimeVODE  = VODE(model, out_dir, c_matrixTot, MX_0Tot, simulationTimeFiCoS, numPoints, listIndexes)
	print("\n * vode  -> Integration time %5.4fs - simulation time %5.4fs"%(integrationTimeVODE, simulationTimeVODE))
	print("*"*100)
	
	with open(out_dir+os.sep+"RunningTime", "w") as fo:
		fo.write("VODE Int.Time\tVODE Sim.Time\tLSODA Int.Time\tLSODA Sim.Time\tFiCoS Int.Time\tFiCoS Sim.Time\n")
		fo.write("%5.4f\t%5.4f\t"%(integrationTimeVODE, simulationTimeVODE))
		fo.write("%5.4f\t%5.4f\t"%(integrationTimeLSODA, simulationTimeLSODA))
		fo.write("%5.4f\t%5.4f\n"%(integrationTimeFiCoS, simulationTimeFiCoS))