import os
import os
import numpy as np

def deleteTransient(series):

	points_max = []
	points_min = []
	for i in range(1, len(series)-1):
		# max
		if series[i-1]<series[i] and series[i]>series[i+1]:	points_max.append([i, series[i]])
		if series[i-1]>series[i] and series[i]<series[i+1]:	points_min.append([i, series[i]])

	if len(points_max) >= 2 and len(points_min) >= 1:
		idx = points_min[0][0]
		avg = np.mean(series[idx:])
		return avg, idx

	else:
		return 0, 0


def amplitudeAfterTransient(series, idx):
	points_max = []
	points_min = []
	for i in range(idx+1, len(series)-1):
		# max
		if series[i-1]<series[i] and series[i]>series[i+1]:	points_max.append([i, series[i]])
		if series[i-1]>series[i] and series[i]<series[i+1]:	points_min.append([i, series[i]])
	
	if len(points_min) < len(points_max):
		minLen = len(points_min)
	else:
		minLen = len(points_max) 

	newMax = []
	newMin = []
	for i in range(minLen):
		newMax.append(points_max[i])
		newMin.append(points_min[i])

	return np.array(newMax), np.array(newMin)


def calculateAmplitude(maxValues, minValues):

	value = 0
	conf  = 0
	for i in range(len(minValues)):

		value += maxValues[i][1] - minValues[i][1]
		conf +=1
		if i < len(minValues)-1: 
			value += maxValues[i][1] - minValues[i+1][1]
			conf +=1

	value = value/float(conf)

	return value


def collectDataGPU(pathIn, numParalSim, numPoints, MX_0, c_matrix):

	print("*"*100)
	print("Processing FiCoS")

	steps = numPoints//numParalSim

	ampEIF = []
	ampAMP = []
	xs = []
	ys = []

	for i in range(0, steps):

		pathFolder = pathIn+os.sep+"sims_%d"%(i*numParalSim)

		for sim in range(numParalSim):
			pathSim = pathFolder+os.sep+"Solution%d"%sim

			idx = i*numParalSim + sim
			xs.append(MX_0[idx][4])
			ys.append(c_matrix[idx][18])

			A = np.loadtxt(pathSim)
			series1 = A.T[2]
			series2 = A.T[3]
			avg, idx = deleteTransient(series1)
			if avg != 0 and idx!=0:
				pmax1, pmin1 = amplitudeAfterTransient(series1, idx-1)
				value = calculateAmplitude(pmax1, pmin1)
				ampEIF.append(value)
		
			else:
				ampEIF.append(0)

			avg, idx = deleteTransient(series2)
			if avg != 0 and idx!=0:
				pmax4, pmin2 = amplitudeAfterTransient(series2, idx-1)
				value = calculateAmplitude(pmax4, pmin2)
				ampAMP.append(value)
			else:
				ampAMP.append(0)

	return xs, ys, ampEIF, ampAMP


def collectDataCPU(pathIn, MX_0, c_matrix, method="lsoda"):

	print("*"*100)
	print("Processing %s"%method)

	pathFolder = pathIn

	listIndexes = []

	for D in os.listdir(pathIn):
		pathDir = pathIn+os.sep+D
		if os.path.isdir(pathDir):
			if D[0:3] == "out":
				idx = int(D[4:])
				listIndexes.append(idx)

	ampEIF = []
	ampAMP = []
	xs = []
	ys = []
	
	for i in range(len(listIndexes)):

			idx = listIndexes[i]
			
			pathSim = pathIn+os.sep+"out_%d"%idx+os.sep+"ODESol_%s"%method

			try:
				A = np.loadtxt(pathSim)
				xs.append(MX_0[idx][4])
				ys.append(c_matrix[idx][18])

				series1 = A.T[2]
				series2 = A.T[3]
				avg, idx     = deleteTransient(series1)
				if avg != 0 and idx!=0:
					pmax1, pmin1 = amplitudeAfterTransient(series1, idx-1)
					value = calculateAmplitude(pmax1, pmin1)
					ampEIF.append(value)
			
				else:
					ampEIF.append(0)

				avg, idx     = deleteTransient(series2)
				if avg != 0 and idx!=0:
					pmax4, pmin2 = amplitudeAfterTransient(series2, idx-1)
					value = calculateAmplitude(pmax4, pmin2)
					ampAMP.append(value)
				else:
					ampAMP.append(0)
			except:
				#print(" * %s does not perform the simulation number %d"%(method, idx))
				pass

	print("Number of simulations %d"%(len(xs)))
	
	return xs, ys, ampEIF, ampAMP


def processData(pathIn, numParalSim=512, numPoints=36864):

	MX_0     = np.loadtxt(pathIn+os.sep+"MX_0Tot")
	c_matrix = np.loadtxt(pathIn+os.sep+"c_matrixTot")

	xs, ys, ampEIF, ampAMP = collectDataGPU(pathIn, numParalSim, numPoints, MX_0, c_matrix)

	with open("ampEIF_FiCoS.tsv", "w") as fo:
		for i in range(len(xs)):
			fo.write("%.8e\t%.8e\t%.8e\n"%(xs[i],ys[i],ampEIF[i]))

	with open("ampAMP_FiCoS.tsv", "w") as fo:
		for i in range(len(xs)):
			fo.write("%.8e\t%.8e\t%.8e\n"%(xs[i],ys[i],ampAMP[i]))

	xs, ys, ampEIF, ampAMP = collectDataCPU(pathIn, MX_0, c_matrix, method="lsoda")

	with open("ampEIF_LSODA.tsv", "w") as fo:
		for i in range(len(xs)):
			fo.write("%.8e\t%.8e\t%.8e\n"%(xs[i],ys[i],ampEIF[i]))

	with open("ampAMP_LSODA.tsv", "w") as fo:
		for i in range(len(xs)):
			fo.write("%.8e\t%.8e\t%.8e\n"%(xs[i],ys[i],ampAMP[i]))

	xs, ys, ampEIF, ampAMP = collectDataCPU(pathIn, MX_0, c_matrix, method="vode")

	with open("ampEIF_VODE.tsv", "w") as fo:
		for i in range(len(xs)):
			fo.write("%.8e\t%.8e\t%.8e\n"%(xs[i],ys[i],ampEIF[i]))

	with open("ampAMP_VODE.tsv", "w") as fo:
		for i in range(len(xs)):
			fo.write("%.8e\t%.8e\t%.8e\n"%(xs[i],ys[i],ampAMP[i]))

if __name__ == '__main__':

	path = "resultsPSA"
	processData(path)