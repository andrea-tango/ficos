nvcc src/ficos.cu src/simulation.cu -O3 -Xptxas -O3 -std=c++11 -use_fast_math -rdc=true -o FiCoS_gpu -lcublas -lcublas_device -lcudadevrt \
-gencode arch=compute_35,code=compute_35 \
-gencode arch=compute_37,code=compute_37 \
-gencode arch=compute_50,code=compute_50 \
-gencode arch=compute_52,code=compute_52 \
-gencode arch=compute_60,code=compute_60 \
-gencode arch=compute_61,code=compute_61 