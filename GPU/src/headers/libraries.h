/***************************************************************************
  written by    : Andrea Tangherloni
  last updated  : May 25, 2021
  e-mail:     : andrea.tangherloni@unibg.it

***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <cmath>
#include <limits.h>
#include <complex> 
#include <cfloat>
#include <ctime>
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <chrono>

#include "cuda.h"
#include "cuda_runtime.h"
#include <cuComplex.h>

#include "cublas_v2.h"
#include <exception>

using namespace std::chrono;
using namespace std;