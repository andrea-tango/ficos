/***************************************************************************
	written by		: Andrea Tangherloni
	last updated	: May 25, 2021
	e-mail:			: andrea.tangherloni@disco.unimib.it

This is a GPU-powered version of DOPRI.
This version exploits the dynamic parallelism to accelerate the ODEs resolution of large scale system.

The original code computes the numerical solution of a system of first order ordinary
differential equations y'=f(x,y). It uses an explicit Runge-Kutta method of
order (4)5 due to Dormand & Prince with step size control and dense output.

This is a modified version of the C++ porting of DOPRI

	written by 		: Blake Ashby
	last updated	: Nov 15, 2002
	e-mail:			: bmashby@stanford.edu

tailored to mechanism based model of biological systems.

This code is written in C++/CUDA and is a modification of the code originally written 
in Fortran (Version of April 28, 1994) by:

	E. Hairer & G. Wanner
	Universite de Geneve, dept. de Mathematiques
	CH-1211 GENEVE 4, SWITZERLAND
	E-mail : HAIRER@DIVSUN.UNIGE.CH, WANNER@DIVSUN.UNIGE.CH

and adapted for C by:
	J.Colinge (COLINGE@DIVSUN.UNIGE.CH).

The code is described in : E. Hairer, S.P. Norsett and G. Wanner, Solving
ordinary differential equations I, nonstiff problems, 2nd edition,
Springer Series in Computational Mathematics, Springer-Verlag (1993).

***************************************************************************/

#ifndef __KERNELSDOPRI_H_INCLUDED__
#define __KERNELSDOPRI_H_INCLUDED__

__global__ void evaluateODE(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy);
__global__ void evaluateJAC(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy);


__constant__ double a21=0.2;
__constant__ double a31=3.0/40.0;
__constant__ double a32=9.0/40.0;
__constant__ double a41=44.0/45.0;
__constant__ double a42=-56.0/15.0;
__constant__ double a43=32.0/9.0;
__constant__ double a51=19372.0/6561.0;
__constant__ double a52=-25360.0/2187.0;
__constant__ double a53=64448.0/6561.0;
__constant__ double a54=-212.0/729.0;
__constant__ double a61=9017.0/3168.0;
__constant__ double a62=-355.0/33.0;
__constant__ double a63=46732.0/5247.0;
__constant__ double a64=49.0/176.0;
__constant__ double a65=-5103.0/18656.0;
__constant__ double a71=35.0/384.0;
__constant__ double a73=500.0/1113.0;
__constant__ double a74=125.0/192.0;
__constant__ double a75=-2187.0/6784.0;
__constant__ double a76=11.0/84.0;
__constant__ double e1=71.0/57600.0;
__constant__ double e3=-71.0/16695.0;
__constant__ double e4=71.0/1920.0;
__constant__ double e5=-17253.0/339200.0;
__constant__ double e6=22.0/525.0;
__constant__ double e7=-1.0/40.0;
__constant__ double d1=-12715105075.0/11282082432.0;
__constant__ double d3=87487479700.0/32700410799.0;
__constant__ double d4=-10690763975.0/1880347072.0;
__constant__ double d5=701980252875.0/199316789632.0;
__constant__ double d6=-1453857185.0/822651844.0;
__constant__ double d7=69997945.0/29380423.0;

__device__ void printStatus(int id, int nfcn, int nstep, int naccpt, int nrejct, int verbose)
{
	if(verbose > 2)
	{
		printf(" * Thread %d of DOPRI, number of function evaluations: %d\n"
			   "   number of attempted steps: %d\n"
			   "   number of accepted steps: %d\n"
			   "   number of rejected steps: %d\n",
			   id, nfcn, nstep, naccpt, nrejct);
	}
}


__device__ double sign(double a, double b)
{
	return (b > 0.0) ? fabs(a) : -fabs(a);
} // sign


__global__ void hinit_Kernel(int N, double h, double *y, double *k1, double *k3)
{
	int idB = threadIdx.x + blockDim.x * blockIdx.x;
	if(idB < N)
	{
		k3[idB] = y[idB] + h * k1[idB];
	}
}

__device__ double hinit(int N, int M, int idB, int numBlock, int numThreads,
	short2 *H, short2 *ODE, int *indexH, int *indexODE,
	double *M_feed, double *c_matrix,
	double hmax, double tend, double t, double rtoler,
	double *atoler, double *y, double *k1, double *k2, double *k3)
{
	
	int iord = 5;
	double posneg = sign(1.0, tend-t);
	double sk, sqr;
	double dnf = 0.0;
	double dny = 0.0;

	double h = 0.0;

	for (int i = 0; i < N; i++)
	{
		sk = atoler[i] + rtoler * fabs(y[N*idB +i]);
		sqr = k1[N*idB +i]/sk;
		dnf += sqr*sqr;
		sqr = y[N*idB +i]/sk;
		dny += sqr*sqr;
	}

	if ((dnf <= 1.0e-10) || (dny <= 1.0e-10))
		h = 1.0e-6;
	else
		h = sqrt(dny/dnf)*0.01;

	h = min(h, hmax);
	h = sign(h, posneg);

	hinit_Kernel<<<numBlock, numThreads>>>(N, h, &y[N*idB], &k1[N*idB], &k3[N*idB]);
	cudaDeviceSynchronize();

	evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &k3[N*idB], &k2[N*idB]);;
	cudaDeviceSynchronize();
	
	// estimate the second derivative of the solution
	double der2 = 0.0;
	for (int i = 0; i < N; i++)
	{
		sk = atoler[i] + rtoler * fabs(y[N*idB +i]);
		sqr = (k2[N*idB +i] - k1[N*idB +i])/sk;
		der2 += sqr*sqr;
	}

	der2 = sqrt(der2)/h;

	// step size is computed such that
	// h**iord * max(norm(k1), norm(der2)) = 0.01
	double der12 = max(fabs(der2), sqrt(dnf));

	double h1;
	if (der12 <= 1.0e-15)
		h1 = max(1.0e-6, fabs(h)*1.0e-3);
	else
		h1 = pow(0.01/der12, 1.0/(double)iord);

	h = min(100.0*h, min(h1, hmax));

	return sign(h, posneg);

} // hinit

// **************************** Kernels used for integration phase **************************** //

__global__ void updateForK2(int N, double h, double a21, double *y, double *k1, double *yy1)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		yy1[gid] = y[gid] + h*a21*k1[gid];
	}
}

__global__ void updateForK3(int N, double h, double a31, double a32, double *y, double *k1, double *k2, double *yy1)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		yy1[gid] = y[gid] + h*(a31*k1[gid] + a32*k2[gid]);
	}
}

__global__ void updateForK4(int N, double h, double a41, double a42, double a43,
	double *y, double *k1, double *k2, double *k3, double *yy1)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		yy1[gid] = y[gid] + h*(a41*k1[gid] + a42*k2[gid] + a43*k3[gid]);
	}
}

__global__ void updateForK5(int N, double h, double a51, double a52, double a53, double a54,
	double *y, double *k1, double *k2, double *k3, double *k4, double *yy1)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		yy1[gid] = y[gid] + h*(a51*k1[gid] + a52*k2[gid] + a53*k3[gid] + a54*k4[gid]);
	}
}

__global__ void updateForK6(int N, double h, double a61, double a62, double a63, double a64, double a65,
	double *y, double *k1, double *k2, double *k3, double *k4, double *k5, double *ysti)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		ysti[gid] = y[gid] + h*(a61*k1[gid] + a62*k2[gid] + a63*k3[gid] + a64*k4[gid] + a65*k5[gid]);
	}
}

__global__ void updateForK2_2(int N, double h, double a71, double a73, double a74, double a75, double a76,
	double *y, double *k1, double *k3, double *k4, double *k5, double *k6,double *yy1)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		yy1[gid] = y[gid] + h*(a71*k1[gid] + a73*k3[gid] + a74*k4[gid] + a75*k5[gid] + a76*k6[gid]);
	}
}

__global__ void computeRcont5(int N, double h, double d1, double d3, double d4, double d5, double d6, double d7,
	double *k1, double *k2, double *k3, double *k4, double *k5, double *k6,double *rcont5)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		rcont5[gid] = h*(d1*k1[gid] + d3*k3[gid] + d4*k4[gid] + d5*k5[gid] + d6*k6[gid] + d7*k2[gid]);
	}
}

__global__ void computeK4(int N, double h,  double e1, double e3, double e4, double e5, double e6, double e7,
	double *k1, double *k2, double *k3, double *k5, double *k6, double *k4)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		k4[gid] = h*(e1*k1[gid] + e3*k3[gid] + e4*k4[gid] + e5*k5[gid] + e6*k6[gid] + e7*k2[gid]);
	}
}

__global__ void computeRconts(int N, double h, double *y, double *yy1, double *k1, double *k2,
	double* rcont1, double* rcont2, double* rcont3, double* rcont4)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{ 
		double yd0 	 = y[gid];
		double ydiff = yy1[gid] - yd0;
		double bspl  = h*k1[gid] - ydiff;
		rcont1[gid]  = y[gid];
		rcont2[gid]  = ydiff;
		rcont3[gid]  = bspl;
		rcont4[gid]	 = -h*k2[gid] + ydiff - bspl;
	}
}

__device__ double function_continuousOutput(int i, double tD, double tOld, double hOld,
	double* rcont1, double* rcont2, double* rcont3, double* rcont4, double* rcont5)
{
	double theta = (tD - tOld)/hOld;
	double theta1 = 1.0 - theta;

	double value = rcont1[i] + theta*(rcont2[i] + theta1*(rcont3[i] + theta*(rcont4[i] + theta1*rcont5[i])));

	return value;	
}

__device__ void SolutionOutput(int naccpt, int &ind, int numSS, int numTimes,
	double &tD, double tOld, double t, double tend, double hOld,
	int *cs_vector, double *t_vector, double *rcont1, double *rcont2, 
	double *rcont3, double *rcont4, double *rcont5, double *dynamics)
{
	while(tD < t)
	{
		for(unsigned i = 0; i < numSS; i++)
		{
			int offset = ind * numSS;
			dynamics[i+offset] = function_continuousOutput(cs_vector[i], tD, tOld, hOld, rcont1, rcont2, rcont3, rcont4, rcont5);
		}

		if(ind < numTimes-1)
		{
			ind++;
			tD = t_vector[ind];
		}
		else
		{
			tD = tend;
		}
	}
}


__global__ void dopriMethod(int N, int M, int numDOPRI,
	int numThreads, int numBlock, short2 *H, short2 *ODE,
	int *indexH, int *indexODE,  double *M_feed, double *c_matrix, double *y,
	double *rcont1, double *rcont2, double *rcont3,
	double *rcont4, double *rcont5, double *f, double *yy1, double *k1,
	double *k2, double *k3,	double *k4, double *k5,	double *k6, double *ysti,
	int numSS, int numTimes, int nmax, double rtoler,
	double *atoler, double *t_vector, int *cs_vector, double *dynamics,
	int *dopriFails, int verbose)
{
	int idB = threadIdx.x + blockDim.x * blockIdx.x;

	if(idB < numDOPRI)
	{
		int ind     = 0;
		double t    = 0.0;
		double tOld = 0.0;
		double tD   = t_vector[0];
		double tend = t_vector[numTimes-1];
		double h    = 1e-6;
		double hOld = 0.0;
		double facl = 0.2; 
		double facr = 10.0;
		double beta = 0.04;
		int nstiff  = 1000;
		int nstep   = 0;
		int nfcn    = 0;
		int nrejct  = 0;
		int naccpt = 0;

		double hmax = tend - t;

		double uround = 1.0e-16;
		// safe--safety factor in step size prediction
		double safe = 0.9;

		double posneg = sign(1.0, tend-t);
		double facold = 1.0e-4;
		double expo1 = 0.2 - beta*0.75;
		double facc1 = 1.0/facl;
		double facc2 = 1.0/facr;

		bool last = false;
		double hlamb = 0.0;
		int iasti = 0;

		evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &k1[N*idB]);
		cudaDeviceSynchronize();

		hmax = fabs(hmax);

		h = hinit(N, M, idB, numBlock, numThreads, H, ODE, indexH, indexODE, M_feed, c_matrix,
			hmax, tend, t, rtoler, atoler, y, k1, k2, k3);

		nfcn += 2;
		bool reject = false;
		int nonsti;

		SolutionOutput(naccpt, ind, numSS, numTimes, tD, tOld, t, tend, hOld, cs_vector, t_vector,
			&rcont1[N*idB], &rcont2[N*idB], &rcont3[N*idB], &rcont4[N*idB], &rcont5[N*idB],
			&dynamics[numSS*numTimes*idB]);

		while (true)
		{
			if(nstep > nmax)
			{
				if(verbose > 2)
					printf(" * Thread %d exit of DOPRI at t = %.4e, more than nmax = %d are needed\n", idB, t, nmax);
				hOld = h;
				
				dopriFails[idB] = 1;
				printStatus(idB, nfcn, nstep, naccpt, nrejct, verbose);
				return;
			}
			if(0.1*fabs(h) <= fabs(t)*uround)
			{	
				if(verbose > 2)
					printf(" * Thread %d exit of DOPRI at t = %.4e, step size too small h = %.4e\n", idB, t, h);
				hOld = h;
				
				dopriFails[idB] = 1;
				printStatus(idB, nfcn, nstep, naccpt, nrejct, verbose);
				return;
			}
			if ((t + 1.01*h - tend)*posneg > 0.0)
			{
				h = tend - t;
				last = true;
			}
			nstep++;

			// the first 6 stages			
			updateForK2<<<numBlock, numThreads>>>(N, h, a21, &y[N*idB], &k1[N*idB], &yy1[N*idB]);
			cudaDeviceSynchronize();

			evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &yy1[N*idB], &k2[N*idB]);
			cudaDeviceSynchronize();
			

			//
			updateForK3<<<numBlock, numThreads>>>(N, h, a31, a32, &y[N*idB], &k1[N*idB], &k2[N*idB], &yy1[N*idB]);
			cudaDeviceSynchronize();

			evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &yy1[N*idB], &k3[N*idB]);
			cudaDeviceSynchronize();

			// 
			updateForK4<<<numBlock, numThreads>>>(N, h, a41, a42, a43, &y[N*idB], &k1[N*idB], &k2[N*idB], &k3[N*idB], &yy1[N*idB]);
			cudaDeviceSynchronize();

			evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &yy1[N*idB], &k4[N*idB]);
			cudaDeviceSynchronize();

			// 
			updateForK5<<<numBlock, numThreads>>>(N, h, a51, a52, a53, a54, &y[N*idB], &k1[N*idB], &k2[N*idB], &k3[N*idB], &k4[N*idB], &yy1[N*idB]);
			cudaDeviceSynchronize();

			evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &yy1[N*idB], &k5[N*idB]);
			cudaDeviceSynchronize();

			// 
			updateForK6<<<numBlock, numThreads>>>(N, h, a61, a62, a63, a64, a65, 
				&y[N*idB], &k1[N*idB], &k2[N*idB], &k3[N*idB], &k4[N*idB], &k5[N*idB], &ysti[N*idB]);
			cudaDeviceSynchronize();
			
			evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &ysti[N*idB], &k6[N*idB]);
			cudaDeviceSynchronize();

			// 
			updateForK2_2<<<numBlock, numThreads>>>(N, h, a71, a73, a74, a75, a76,
				&y[N*idB], &k1[N*idB], &k3[N*idB], &k4[N*idB], &k5[N*idB], &k6[N*idB], &yy1[N*idB]);
			cudaDeviceSynchronize();

			evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &yy1[N*idB], &k2[N*idB]);
			cudaDeviceSynchronize();

			// 
			computeRcont5<<<numBlock, numThreads>>>(N, h, d1, d3, d4, d5, d6, d7,
				&k1[N*idB], &k2[N*idB], &k3[N*idB], &k4[N*idB], &k5[N*idB], &k6[N*idB], &rcont5[N*idB]);
			cudaDeviceSynchronize();

			computeK4<<<numBlock, numThreads>>>(N, h, e1, e3, e4, e5, e6, e7,
				&k1[N*idB], &k2[N*idB], &k3[N*idB], &k5[N*idB], &k6[N*idB], &k4[N*idB]);
			cudaDeviceSynchronize();
			
			nfcn += 6;

			// error estimation
			double err = 0.0, sk, sqr;
			for (int i = 0; i < N; i++)
			{
				sk = atoler[i] + rtoler*max(fabs(y[N*idB+i]), fabs(yy1[N*idB+i]));
				sqr = k4[N*idB+i]/sk;
				err += sqr*sqr;
			}
			err = sqrt(err/(double)N);

			// computation of hnew
			double fac11 = pow(err, expo1);
			// Lund-stabilization
			double fac = fac11/pow(facold,beta);
			// we require facl <= hnew/h <= facr
			fac = max(facc2, min(facc1, fac/safe));
			double hnew = h/fac;

			if (err <= 1.0)
			{
				// step accepted
				facold = max(err, 1.0e-4);
				naccpt++;
				// stiffness detection
				if (!(naccpt % nstiff) || (iasti > 0))
				{
					double stnum = 0.0, stden = 0.0;
					for (int i = 0; i < N; i++)
					{
						sqr = k2[N*idB+i] - k6[N*idB+i];
						stnum += sqr*sqr;
						sqr = yy1[N*idB+i] - ysti[N*idB+i];
						stden += sqr*sqr;
					}
					if (stden > 0.0)
						hlamb = h*sqrt(stnum/stden);
					if (hlamb > 3.25)
					{
						nonsti = 0;
						iasti++;
						if (iasti == 15)
						{
							if(verbose > 2)
								printf(" * Thread %d exit of DOPRI, the problem seems to become stiff at t = %.4e\n", idB, t);
							hOld = h;
							
							dopriFails[idB] = 1;
							printStatus(idB, nfcn, nstep, naccpt, nrejct, verbose);
							return;
						}
					}
					else
					{
						nonsti++;
						if (nonsti == 6)
							iasti = 0;
					}
				}
				
				computeRconts<<<numBlock, numThreads>>>(N, h, &y[N*idB], &yy1[N*idB],
					&k1[N*idB], &k2[N*idB], &rcont1[N*idB], &rcont2[N*idB], &rcont3[N*idB],
					&rcont4[N*idB]);
				cudaDeviceSynchronize();

				memcpy(&k1[N*idB], &k2[N*idB], N*sizeof(double));
				memcpy(&y[N*idB], &yy1[N*idB], N*sizeof(double));

				tOld = t;
				t += h;

				hOld = h;

				SolutionOutput(naccpt, ind, numSS, numTimes, tD, tOld, t, tend, hOld, cs_vector, t_vector,
					&rcont1[N*idB], &rcont2[N*idB], &rcont3[N*idB], &rcont4[N*idB], &rcont5[N*idB],
					&dynamics[numSS*numTimes*idB]);

				if (last)
				{
					hOld = hnew;
					dopriFails[idB] = 0;

					if(t_vector[numTimes-1] == tend)
					{
						for(unsigned i = 0; i < numSS; i++)
						{
							int offset = ind * numSS + numSS*numTimes*idB;
							dynamics[i+offset] = function_continuousOutput(cs_vector[i], tD, tOld, hOld, &rcont1[N*idB], &rcont2[N*idB], &rcont3[N*idB], &rcont4[N*idB], &rcont5[N*idB]);

						}
					}

					printStatus(idB, nfcn, nstep, naccpt, nrejct, verbose);
					return;
				}

				if (fabs(hnew) > hmax)
					hnew = posneg*hmax;
				if (reject)
					hnew = posneg*min(fabs(hnew), fabs(h));
				reject = false;
			}
			else
			{
				// step rejected
				hnew = h/min(facc1, fac11/safe);
				reject = true;
				if (naccpt >= 1)
					nrejct++;
				last = false;
			}
			h = hnew;
		}
	}
}

#endif