/***************************************************************************
	written by		: Andrea Tangherloni
	last updated	: May 25, 2021
	e-mail:			: andrea.tangherloni@unibg.it

***************************************************************************/

#ifndef __KERNELSSIMULATIONS_H_INCLUDED__
#define __KERNELSSIMULATIONS_H_INCLUDED__

__global__ void evaluateODE(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy)
{
	int idB = threadIdx.x + blockDim.x * blockIdx.x;
	if(idB < N)
	{
		int startH   = indexH[idB];
		int endH     = indexH[idB+1];

		int start    = 0;
		int end      = 0;
		int idx      = 0;
		int exp      = 0;
		double coeff = 0.0;
		double tmp   = 0.0;

		dy[idB] = 0.0;

		for(int i=startH; i < endH; i++)
		{
			coeff = H[i].y;
			tmp   = coeff*c_vector[H[i].x];

			start = indexODE[H[i].x];
			end   = indexODE[H[i].x+1];
			for(int j=start; j < end; j++)
			{
				idx = ODE[j].x;
				exp = ODE[j].y;
				for(int k=0; k < exp; k++)
					tmp = tmp * y[idx];
			}
			dy[idB] = dy[idB] + tmp;
		}
		dy[idB] = dy[idB]*M_feed[idB];
	}
}

__global__ void evaluateJAC(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy)
{
	int idB = threadIdx.x + blockDim.x * blockIdx.x;
	if(idB < N)
	{
		int startH   = indexH[idB];
		int endH     = indexH[idB+1];
		int start    = 0;
		int end      = 0;
		int idx      = 0;
		int exp      = 0;
		int idx1     = 0;
		int exp1     = 0;
		double tmp   = 0.0;
		double coeff = 0.0;

		for(int row=0; row < N; row++)
		{
			dy[idB*N + row] = 0.0;
			for(int i=startH; i < endH; i++)
			{
				tmp = 0.0;
				coeff = H[i].y;

				start = indexODE[H[i].x];
				end   = indexODE[H[i].x+1];
				for(int j=start; j < end; j++)
				{
					idx = ODE[j].x;
					exp = ODE[j].y;
					if(idx == row)
					{
						tmp = coeff*exp*c_vector[H[i].x];
						for(int k=0; k < exp-1; k++)
							tmp = tmp * y[idx];

						// continue;

						for(int k1 = start; k1 < end; k1++)
						{
							idx1 = ODE[k1].x;
							exp1 = ODE[k1].y;
							if(idx1 != row)	
								for(int k=0; k < exp1; k++)
									tmp = tmp * y[idx1];
						}
					}
				}
				dy[idB*N + row] = dy[idB*N + row] + tmp;
			}

			dy[idB*N + row] = dy[idB*N + row]*M_feed[idB];
		}
	}
}

__device__ double calculateBound(int N, double *jac, double *norms)
{
	double value;
	for(int i = 0; i < N; i++)
	{
		value = 0.0;
		for(int j = 0; j < N; j++)
		{
			value += abs(jac[i*N + j]);
		}
		norms[i] = value;
	}

	double eig = norms[0];
	for(int i = 1; i < N; i++)
	{
		if(norms[i] > eig)
			eig = norms[i];
	}

	return eig;
}

__global__ void checkStartMethod(int N, int M, int numSim, int numThreads, int numBlock,
		short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_matrix,
		double *MX_0, double *jac, double *norms, double *eigen,
		int *selectionMethod, int verbose)
{
	int idB = threadIdx.x + blockDim.x * blockIdx.x;

	if(idB < numSim)
	{

		evaluateJAC<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &MX_0[N*idB], &jac[N*N*idB]);
		cudaDeviceSynchronize();

		eigen[idB] = calculateBound(N, &jac[N*N*idB], &norms[N*idB]);

		if(verbose > 2)
		{
			printf(" * Model %d has the dominant eigenvalue equal to %5.2f\n", idB, eigen[idB]);
		}

		if(eigen[idB] < 500)
			selectionMethod[idB] = 0;
		else
			selectionMethod[idB] = 1;
	}
}


__global__ void calculateFitness(int numSim, int numTimes, int numSS, 
		double *ts_matrix, double *dynamics, double *fitnessValues)
{
	int idB = threadIdx.x + blockDim.x * blockIdx.x;

	if(idB < numSim)
	{
		double valueSIM;
		double valueTS;
		double fitness = 0.0;
		int shift = idB*numSS*numTimes;
		
		for(int i=0; i < numTimes; i++)
		{
			for(int j = 0; j < numSS; j++)
			{
				valueTS = ts_matrix[i*numSS+j];
				valueSIM = dynamics[i*numSS+j + shift];
				valueTS  = max(valueTS, 1e-16);
				valueSIM = max(valueSIM, 1e-16);
				
				fitness += abs(valueTS - valueSIM) / valueTS;
			}
		}
		fitnessValues[idB] = fitness;
		// printf("Thread %d = %.8e\n", idB, fitness);
	}
}

#endif