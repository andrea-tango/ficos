/***************************************************************************
	written by		: Andrea Tangherloni
	last updated	: May 25, 2021
	e-mail:			: andrea.tangherloni@unibg.it

This is a GPU-powered version of RADAU5.
This version exploits the dynamic parallelism to accelerate the ODEs resolution of large scale system.

This is a modified, improved and corrected version of the C++ porting of RADAU5

	written by 		: Blake Ashby
	last updated	: Nov 15, 2002
	e-mail:			: bmashby@stanford.edu

tailored to mechanism based model of biological systems.

This code is written in C++/CUDA and is a modification of the code originally written 
in Fortran (version of July 9, 1996, latest small correction: January 18, 2002) by:

	E. Hairer and G. Wanner
	Universite de Geneve, Dept. de Mathematiques
	Ch-1211 Geneve 24, Switzerland
	E-mail:  ernst.hairer@math.unige.ch
	gerhard.wanner@math.unige.ch

RADAU5 is described in the book:

	E. Hairer and G. Wanner, Solving Ordinary Differential
	Equations II. Stiff and Differential-Algebraic Problems.
	Springer Series in Computational Mathematics 14,
	Springer-Verlag 1991, Second Edition 1996.

This code computes the numerical solution of a stiff system of first order
Ordinary Differential Equations:

					y'=f(x,y).

The method used is an implicit Runge-Kutta method (RADAU IIA) of order 5 with
step size control and continuous output. (See Section IV.8 of Hairer and Wanner).

***************************************************************************/

#ifndef __KERNELSRADAU_H_INCLUDED__
#define __KERNELSRADAU_H_INCLUDED__

#define CUDA_CHECK_RETURN(value) {											\
		cudaError_t _m_cudaStat = value;										\
		if (_m_cudaStat != cudaSuccess) {										\
			fprintf(stderr, "Error %s at line %d in file %s\n",					\
					cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);		\
					exit(-200);															\
		} }

__global__ void evaluateODE(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy);
__global__ void evaluateJAC(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_vector, double *y, double *dy);

__constant__ double t11 = 9.1232394870892942792e-02;
__constant__ double t12 = -0.14125529502095420843;
__constant__ double t13 = -3.0029194105147424492e-02;
__constant__ double t21 = 0.24171793270710701896;
__constant__ double t22 = 0.20412935229379993199;
__constant__ double t23 = 0.38294211275726193779;
__constant__ double t31 = 0.96604818261509293619;
__constant__ double ti11 = 4.3255798900631553510;
__constant__ double ti12 = 0.33919925181580986954;
__constant__ double ti13 = 0.54177053993587487119;
__constant__ double ti21 = -4.1787185915519047273;
__constant__ double ti22 = -0.32768282076106238708;
__constant__ double ti23 = 0.47662355450055045196;
__constant__ double ti31 = -0.50287263494578687595;
__constant__ double ti32 = 2.5719269498556054292;
__constant__ double ti33 = -0.59603920482822492497;

__device__ void printStatus(int id, int nfcn, int njac, int nstep,
	int naccpt, int nrejct, int ndec, int nsol, int verbose)
{
	if(verbose > 2)
	{
		printf(" * Thread %d of RADAU, number of function evaluations: %d\n"
		       "   number of jacobian evaluations: %d\n"
		       "   number of attempted steps: %d\n"
		       "   number of accepted steps: %d\n"
		       "   number of rejected steps: %d\n"
		       "   number of lu-decompositions: %d\n"
		       "   number of linear systems resolutions: %d\n",
		       id, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol);
	}
}

__device__ void saveFailedDynamics(int N, int idB, int numTimes, int numSS, int tD, 
	double *cont, int *cs_vector, double *dynamics)
{
	int pos;
	double value;
	
	for(int i=tD; i < numTimes; i++)
	{
		pos = i*numSS + idB*numSS*numTimes;
		for(int j = 0; j < numSS; j++)
		{
			value = cont[4*N*idB + cs_vector[j]];
			dynamics[pos] = value;
			pos++;
		}
	}
}


__global__ void cont_Kernel1(int N, double *y, double *cont)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{ 
		cont[gid] = y[gid];
	}
}

__global__ void scal_Kernel1(int N, double rtoler, double *atoler, double *y, double *scal)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{ 
		scal[gid] = atoler[gid] + rtoler*fabs(y[gid]);
	}
}

__global__ void updateForRealDec(int N, double fac1, double *arrayJac, double *e1)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		for(int i = 0; i < N; i++)
		{
			e1[i*N + gid] = -arrayJac[i*N + gid];
		}
		e1[gid*N + gid] += fac1;
	}
}

__global__ void updateForCompDec(int N, double alphn, double betan, double *arrayJac, cuDoubleComplex *e2r)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		for(int i = 0; i < N; i++)
        {
            e2r[i*N + gid] = make_cuDoubleComplex(-arrayJac[i*N + gid], 0.0);
        }
        e2r[gid*N + gid] = cuCadd(e2r[gid*N + gid], make_cuDoubleComplex(alphn, betan));
	}
}

__global__ void initialization(int N, double *z1, double *z2, double *z3, double *f1, double *f2, double *f3)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		
		z1[gid] = 0.0;
		z2[gid] = 0.0;
		z3[gid] = 0.0;
		f1[gid] = 0.0;
		f2[gid] = 0.0;
		f3[gid] = 0.0;
	}
}

__global__ void updateVectors(int N, double c1q, double c2q, double c3q, double c1m1, double c2m1,
	double *z1, double *z2, double *z3, double *f1, double *f2, double *f3, double *cont)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{	

		double ak1 = cont[gid+N];
		double ak2 = cont[gid+2*N];
		double ak3 = cont[gid+3*N];
		z1[gid] = c1q*(ak1 + (c1q - c2m1)*(ak2 + (c1q - c1m1)*ak3));
		z2[gid] = c2q*(ak1 + (c2q - c2m1)*(ak2 + (c2q - c1m1)*ak3));
		z3[gid] = c3q*(ak1 + (c3q - c2m1)*(ak2 + (c3q - c1m1)*ak3));
		f1[gid] = ti11*z1[gid] + ti12*z2[gid] + ti13*z3[gid];
		f2[gid] = ti21*z1[gid] + ti22*z2[gid] + ti23*z3[gid];
		f3[gid] = ti31*z1[gid] + ti32*z2[gid] + ti33*z3[gid];
	}
}

__global__ void updateForZ1(int N, double *y, double *z1, double *cont)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		cont[gid] = y[gid] + z1[gid];
	}						
}

__global__ void updateForZ2(int N, double *y, double *z2, double *cont)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		cont[gid] = y[gid] + z2[gid];
	}	
}

__global__ void updateForZ3(int N, double *y, double *z3, double *cont)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		cont[gid] = y[gid] + z3[gid];
	}	
}

__global__ void updateForLinearSystem(int N, double *z1, double *z2, double *z3)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{

		double a1 = z1[gid];
		double a2 = z2[gid];
		double a3 = z3[gid];

		z1[gid] = ti11*a1 + ti12*a2 + ti13*a3;
		z2[gid] = ti21*a1 + ti22*a2 + ti23*a3;
		z3[gid] = ti31*a1 + ti32*a2 + ti33*a3;
	}
}

__global__ void updateForLinearSystem2(int N, double fac1, double alphn, double betan, double *z1, double *z2, double *z3, double *f1, double *f2, double *f3)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		double s2 = -f2[gid];
		double s3 = -f3[gid];
		z1[gid] -= f1[gid]*fac1;
		z2[gid] = z2[gid] + s2*alphn - s3*betan;
		z3[gid] = z3[gid] + s3*alphn + s2*betan;
	}
}

__global__ void updateVectors2(int N, double *z1, double *z2, double *z3, double *f1, double *f2, double *f3)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{

		f1[gid] = f1[gid] + z1[gid];
		f2[gid] = f2[gid] + z2[gid];
		f3[gid] = f3[gid] + z3[gid];
		z1[gid] = t11*f1[gid] + t12*f2[gid] + t13*f3[gid];
		z2[gid] = t21*f1[gid] + t22*f2[gid] + t23*f3[gid];
		z3[gid] = t31*f1[gid] + f2[gid];
	}
}

__global__ void kernel_error1(int N, double h, double *z1, double *z2, double *z3, double *f2, double *y0, double *cont)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		
		double hee1 = -(13.0 + 7.0*sqrt(6.0))/(3.0*h);
		double hee2 = (-13.0 + 7.0*sqrt(6.0))/(3.0*h);
		double hee3 = -1.0/(3.0*h);

		f2[gid] = hee1*z1[gid] + hee2*z2[gid] + hee3*z3[gid];
		cont[gid] = f2[gid] + y0[gid];
	}
}

__global__ void kernel_error2(int N, double *cont, double *y)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		cont[gid] = y[gid] + cont[gid];
	}
}

__global__ void kernel_error3(int N, double *cont, double *f1, double *f2)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{
		cont[gid] = f1[gid] + f2[gid];
	}
}

__global__ void updateCont(int N, double c1, double c2, double c1m1, double c2m1, double c1mc2,
	double *y, double *z1, double *z2, double *z3, double *cont)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{

		y[gid] = y[gid] + z3[gid];
		cont[gid+N] = (z2[gid] - z3[gid])/c2m1;
		double ak = (z1[gid] - z2[gid])/c1mc2;
		double acont3 = z1[gid]/c1;
		acont3 = (ak - acont3)/c2;
		cont[gid+2*N] = (ak - cont[gid+N])/c1m1;
		cont[gid+3*N] = cont[gid+2*N] - acont3;
	}
}

__global__ void updateScalCont(int N, double rtoler, double *atoler, double *y, double *scal, double *cont)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < N)
	{	
		scal[gid] = atoler[gid] + rtoler*fabs(y[gid]);
		cont[gid] = y[gid];
	}
}

__device__ void ComputeJacobian(int N, int numBlock, int numThreads, int &njac, bool &caljac,
	short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_matrix, 
	double *y, double *dy)
{
	
	njac++;	
	evaluateJAC<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, M_feed, c_matrix, y, dy);
	cudaDeviceSynchronize();
	caljac = true;
}


__global__ void createZcomplex(int n, double *z2, double *z3, cuDoubleComplex *z)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < n)
	{ 
		z[gid] = make_cuDoubleComplex(z2[gid], z3[gid]);
	}
}

__global__ void updateZ2_Z3(int n, double *z2, double *z3, cuDoubleComplex *z)
{
	int gid = threadIdx.x + blockDim.x * blockIdx.x;
	if(gid < n)
	{ 
		z2[gid] = cuCreal(z[gid]);
		z3[gid] = cuCimag(z[gid]);
	}
}

__device__ void DecompReal(cublasHandle_t cublasHandle, int idB, int N, int *ip1, int *ier,
	double **pointRealMatrix, int threads)
{
	cublasDgetrfBatched(cublasHandle, N, pointRealMatrix, N, ip1, ier, 1);
	cudaDeviceSynchronize();
}

__device__ void DecompComplex(cublasHandle_t cublasHandle, int N, int *ip2, int *ier, 
	cuDoubleComplex **pointComplexMatrix, int threads)
{
	cublasZgetrfBatched(cublasHandle, N, pointComplexMatrix, N, ip2, ier, 1);
	cudaDeviceSynchronize();
}

__device__ void LinearSolve(cublasHandle_t cublasHandle, int N,  int *ip1, int *ip2, int *ier, 
			double **pointRealMatrix, double **pointRealVector,
			cuDoubleComplex **pointComplexMatrix, cuDoubleComplex **pointComplexVector, int threads)
{

	cublasDgetrsBatched(cublasHandle, CUBLAS_OP_T, N, 1, (const double**)pointRealMatrix, N, (const int *)ip1, pointRealVector, N, ier, 1);
	cudaDeviceSynchronize();

	cublasZgetrsBatched(cublasHandle, CUBLAS_OP_T, N, 1, (const cuDoubleComplex**)pointComplexMatrix, N, (const int *)ip2, pointComplexVector, N, ier, 1);
	cudaDeviceSynchronize();
}

__device__ void ErrorEstimate(cublasHandle_t cublasHandle, int numBlock, int numThreads, int N, double h,
	short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_matrix,
	double &err, int &nfcn, bool first, bool reject, double *z1, double *z2, double *z3, 
	double *f1, double *f2, double *y0, double *y, double *cont, double *scal,
	int *ip1, int *ier, double **pointRealMatrix, double **pointRealVectorE)
{

	kernel_error1<<<numBlock, numThreads>>>(N, h, z1, z2, z3, f2, y0, cont);
	cudaDeviceSynchronize();

	cublasDgetrsBatched(cublasHandle, CUBLAS_OP_T, N, 1, (const double**)pointRealMatrix, N, (const int *)ip1, pointRealVectorE, N, ier, 1);
	cudaDeviceSynchronize();

	err = 0.0;
	for (int i = 0; i < N; i++)
		err += pow(cont[i]/scal[i], 2);

	err = max(sqrt(err/N), 1.0e-10);

	if(err < 1.0)
		return;

	if(first || reject)
	{
		kernel_error2<<<numBlock, numThreads>>>(N, cont, y);
		cudaDeviceSynchronize();
		
		evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, M_feed, c_matrix, cont, f1);
		cudaDeviceSynchronize();

		nfcn++;
		
		kernel_error3<<<numBlock, numThreads>>>(N, cont, f1, f2);
		cudaDeviceSynchronize();


		cublasDgetrsBatched(cublasHandle, CUBLAS_OP_T, N, 1, (const double**)pointRealMatrix, N, (const int *)ip1, pointRealVectorE, N, ier, 1);
		cudaDeviceSynchronize();

		err = 0.0;
		for (int i = 0; i < N; i++)
			err += pow(cont[i]/scal[i], 2);
		err = max(sqrt(err/N), 1.0e-10);
	}
}

__device__ double function_continuousOutput(int N, int i, double tD, double t, double hOld, double* cont)
{
	double s, sq6, c1, c2, c2m1, c1m1;
	sq6 = sqrt(6.0);
	c1 = (4.0 - sq6)/10.0;
	c2 = (4.0 + sq6)/10.0;
	c1m1 = c1 - 1.0;
	c2m1 = c2 - 1.0;
	s = (tD - t)/hOld;
	double value = (cont[i] + s*(cont[i+N] + (s - c2m1)*(cont[i+2*N] + (s - c1m1)*cont[i+3*N])));

	return value;	
}

__device__ void SolutionOutput(int N, int naccpt, int &ind, int numSS, int numTimes,
	double &tD, double tOld, double t, double tend, double hOld,
	int *cs_vector, double *t_vector, double *cont, double *dynamics)
{
	while(tD < t)
	{
		for(unsigned i = 0; i < numSS; i++)
		{
			int offset = ind * numSS;
			dynamics[i+offset] = function_continuousOutput(N, cs_vector[i], tD, t, hOld, cont);
		}

		if(ind < numTimes-1)
		{
			ind++;
			tD = t_vector[ind];
		}
		else
		{
			tD = tend;
		}
	}
}

__global__ void radauMethod(int N, int M, int numRADAU, int numThreads, int numBlock,
	short2 *H, short2 *ODE, int *indexH, int *indexODE,  double *M_feed, double *c_matrix,
	double *y, double *y0,	double *cont, double *scal, double *z1, double *z2, double *z3,
	cuDoubleComplex *z,	double *f1, double *f2, double *f3,
	double *e1, cuDoubleComplex *e2r, double *arrayJac,
	int numSS, int numTimes, int nmax, int nit, double rtoler,
	double *atoler, double *t_vector, int *cs_vector,
	double *dynamics, int verbose,
	double **pointRealMatrix,
	double **pointRealVector,
	double **pointRealVectorE,
	cuDoubleComplex **pointComplexMatrix,
	cuDoubleComplex **pointComplexVector,
	int *ip1, int *ip2, int *ier)
{

	int idB = threadIdx.x + blockDim.x * blockIdx.x;

	if(idB < numRADAU)
	{
		cublasHandle_t cublasHandle = NULL; 
		cublasCreate_v2(&cublasHandle);

		cudaStream_t stream;
		cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);
		cublasSetStream(cublasHandle,stream);

		pointRealVector[idB]    = &z1[idB*N];
		pointComplexVector[idB] = &z[idB*N];
		pointRealVectorE[idB]   = &cont[4*idB*N];

		pointRealMatrix[idB]    = &e1[idB*N*N];
		pointComplexMatrix[idB] = &e2r[idB*N*N];

		pointRealVector[idB]    = &z1[idB*N];
		pointComplexVector[idB] = &z[idB*N];
		pointRealVectorE[idB]   = &cont[4*idB*N];

		pointRealMatrix[idB]    = &e1[idB*N*N];
		pointComplexMatrix[idB] = &e2r[idB*N*N];

		int ind      = 0;
		double t     = 0.0;
		double tOld  = 0.0;
		double tD    = t_vector[0];
		double tend  = t_vector[numTimes-1];
		double h     = 1e-6;
		double hOld  = 0.0;
		double facl  = 5.0; 
		double facr  = 1.0/8.0;
		double quot1 = 1.0;
		double quot2 = 1.2;
		double thet  = 0.001;

		double fac1;
		double err;
		double alphn;
		double betan;
		
		bool startn = false;
		bool pred   = true;
		bool caljac = true;
		bool first  = true;
		bool reject;

		int nfcn    = 0;
		int njac    = 0;
		int ndec    = 0;
		int nsol    = 0;
		int nstep   = 0;
		int naccpt  = 0;
		int nrejct  = 0;

		double hmax = tend - t;
		double uround = 1.0e-16;
		// safe--safety factor in step size prediction
		double safe = 0.9;
		double quot;

		double fnewt = max(10.0*uround/rtoler, min(0.03, sqrt(rtoler)));

		// constants
		double sq6 = sqrt(6.0);
		double c1 = (4.0 - sq6)/10.0;
		double c2 = (4.0 + sq6)/10.0;
		double c1m1 = c1 - 1.0;
		double c2m1 = c2 - 1.0;
		double c1mc2 = c1 - c2;
		double u1 = 1.0/((6.0 + pow(81.0, 1.0/3.0) - pow(9.0, 1.0/3.0))/30.0);
		double alph = (12.0 - pow(81.0, 1.0/3.0) + pow(9.0, 1.0/3.0))/60.0;
		double beta = (pow(81.0, 1.0/3.0) + pow(9.0, 1.0/3.0))*sqrt(3.0)/60.0;
		double cno = alph*alph + beta*beta;

		alph = alph/cno;
		beta = beta/cno;

		double posneg = copysign(1.0, tend-t);
		double hmaxn = min(fabs(hmax), fabs(tend - t));
		double cfac = safe*(1 + 2*nit);

		h = min(fabs(h), hmaxn);
		h = copysign(h, posneg);
		hOld = h;

		bool last = false;

		if((t + h*1.0001 - tend)*posneg >= 0.0)
		{
			h = tend - t;
			last = true;
		}

		double hopt = h;
		double faccon = 1.0;

		cont_Kernel1<<<numBlock, numThreads>>>(N,  &y[N*idB],  &cont[4*N*idB]);
		cudaDeviceSynchronize();

		SolutionOutput(N, naccpt, ind, numSS, numTimes, tD, tOld, t, tend, hOld, cs_vector, t_vector,
					&cont[4*N*idB], &dynamics[numSS*numTimes*idB]);

		scal_Kernel1<<<numBlock, numThreads>>>(N, rtoler, atoler, &y[N*idB], &scal[N*idB]);
		cudaDeviceSynchronize();
		
		evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &y0[N*idB]);
		cudaDeviceSynchronize();

		nfcn++;

		double hhfac = h;
		double hacc = 0, erracc=0, thqold=0;
		int nsing = 0;

		// basic integration step
		ComputeJacobian(N, numBlock, numThreads, njac, caljac, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &arrayJac[N*N*idB]);

		bool loop = true;
		
		while(loop)
		{	
			loop = false;
			// compute the matrices e1 and e2 and their decompositions
			fac1 = u1/h;
			alphn = alph/h;
			betan = beta/h;

			updateForRealDec<<<numBlock, numThreads>>>(N, fac1, &arrayJac[N*N*idB], &e1[N*N*idB]);
			cudaDeviceSynchronize();
						
			DecompReal(cublasHandle, idB, N, &ip1[idB*N], &ier[idB], &pointRealMatrix[idB], 1);

			// exit if the Jacobian is singular for 5 times
			if(ier[idB] != 0)
			{
				if(ier[idB] == -1)
				{
					saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
					printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
					return;
				}
				nsing++;
				if(nsing >= 5)
				{
					if(verbose > 2)
						printf(" * Thread %d exit of RADAU at t = %.4e\n, matrix is repeatedly singular", idB, t);
					
					saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
					printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
					return;
				}

				h *= 0.5;
				hhfac = 0.5;
				reject = true;
				last = false;
				if(!caljac)
					ComputeJacobian(N, numBlock, numThreads, njac, caljac, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &arrayJac[N*N*idB]);
				loop = true;
				continue;
			}

			updateForCompDec<<<numBlock, numThreads>>>(N, alphn, betan, &arrayJac[N*N*idB], &e2r[N*N*idB]);
			cudaDeviceSynchronize();

			DecompComplex(cublasHandle, N, &ip2[idB*N], &ier[idB], &pointComplexMatrix[idB], 1);	

			if(ier[idB] != 0)
			{
				if(ier[idB] == -1)
				{
					saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
					printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
					return;
				}
				nsing++;
				if(nsing >= 5)
				{
					if(verbose > 2)
						printf(" * Thread %d exit of RADAU at t = %.4e\n, matrix is repeatedly singular", idB, t);
					
					saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
					printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
					return;
				}
				h *= 0.5;
				hhfac = 0.5;
				reject = true;
				last = false;
				if(!caljac)
					ComputeJacobian(N, numBlock, numThreads, njac, caljac, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &arrayJac[N*N*idB]);
				loop = true;
				continue;
			}
			ndec++;

			while (true)
			{
				nstep++;
				if(nstep >= nmax)
				{
					if(verbose > 2)
						printf(" * Thread %d exit of RADAU at t = %.4e, more than nmax = %d steps are needed\n", idB, t, nmax);
					
					saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
					printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
					return;
				}
				if (0.1*fabs(h) <= fabs(t)*uround)
				{
					if(verbose > 2)
						printf(" * Thread %d exit of RADAU at t = %.4e, step size too small, h = %.4e\n", idB, t, h);
					
					saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
					printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
					return;
				}

				double tph = t + h;

				//  starting values for Newton iteration
				if (first || startn)
				{
					initialization<<<numBlock, numThreads>>>(N, &z1[N*idB], &z2[N*idB], &z3[N*idB], &f1[N*idB], &f2[N*idB], &f3[N*idB]);
					cudaDeviceSynchronize();
				}
				else
				{
					double c3q = h/hOld;
					double c1q = c1*c3q;
					double c2q = c2*c3q;
					updateVectors<<<numBlock, numThreads>>>(N, c1q, c2q, c3q, c1m1, c2m1, &z1[N*idB], &z2[N*idB], &z3[N*idB], &f1[N*idB], &f2[N*idB], &f3[N*idB], &cont[4*N*idB]);
					cudaDeviceSynchronize();
				}	

				//  loop for the simplified Newton iteration
				int newt = 0;
				faccon = pow(max(faccon, uround), 0.8);
				double theta = fabs(thet);
				double dyno = 0.0, dynold = 0.0;

				while(true)
				{
					if(newt >= nit)
					{
						if(ier[idB] != 0)
						{
							nsing++;
							if(nsing >= 5)
							{
								if(verbose > 2)
									printf(" * Thread %d exit of RADAU at t = %.4e, matrix is repeatedly singular\n", idB, t);
								
								saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
								printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
								return;
							}
						}
						h *= 0.5;
						hhfac = 0.5;
						reject = true;
						last = false;
						if(!caljac)
							ComputeJacobian(N, numBlock, numThreads, njac, caljac, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &arrayJac[N*N*idB]);
						loop = true;
						break;
					}

					updateForZ1<<<numBlock, numThreads>>>(N, &y[N*idB], &z1[N*idB], &cont[4*N*idB]);
					cudaDeviceSynchronize();

					evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &cont[4*N*idB], &z1[N*idB]);
					cudaDeviceSynchronize();

					updateForZ2<<<numBlock, numThreads>>>(N, &y[N*idB], &z2[N*idB], &cont[4*N*idB]);
					cudaDeviceSynchronize();

					evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &cont[4*N*idB], &z2[N*idB]);
					cudaDeviceSynchronize();

					updateForZ3<<<numBlock, numThreads>>>(N, &y[N*idB], &z3[N*idB], &cont[4*N*idB]);
					cudaDeviceSynchronize();

					evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &cont[4*N*idB], &z3[N*idB]);
					cudaDeviceSynchronize();

					nfcn += 3;

					// solve the linear systems
					updateForLinearSystem<<<numBlock, numThreads>>>(N, &z1[N*idB], &z2[N*idB], &z3[N*idB]);
					cudaDeviceSynchronize();

					updateForLinearSystem2<<<numBlock, numThreads>>>(N, fac1, alphn, betan, &z1[N*idB], &z2[N*idB], &z3[N*idB], &f1[N*idB], &f2[N*idB], &f3[N*idB]);
					cudaDeviceSynchronize();
					
					createZcomplex<<<numBlock, numThreads>>>(N, &z2[N*idB], &z3[N*idB], &z[N*idB]);
					cudaDeviceSynchronize();

					LinearSolve(cublasHandle, N, &ip1[idB*N], &ip2[idB*N], &ier[idB],
						&pointRealMatrix[idB], &pointRealVector[idB],
						&pointComplexMatrix[idB], &pointComplexVector[idB], 1);

					updateZ2_Z3<<<numBlock, numThreads>>>(N, &z2[N*idB], &z3[N*idB], &z[N*idB]);
					cudaDeviceSynchronize();

					if(ier[idB] == -1)
					{
						saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
						printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
						return;
					}
					nsol++;
					newt++;

					dyno = 0.0;
					double denom;
					for(int i = 0; i < N; i++)
					{
						denom = scal[N*idB+i];
						dyno  = dyno + pow(z1[N*idB+i]/denom, 2) + pow(z2[N*idB+i]/denom, 2) + pow(z3[N*idB+i]/denom, 2);
					}
					dyno = sqrt(dyno/(3*N));

					// bad convergence or number of iterations to large
					if((newt > 1) && (newt < nit))
					{
						double thq = dyno/dynold;
						if(newt == 2)
							theta = thq;
						else
							theta = sqrt(thq*thqold);
						thqold = thq;

						if (theta < 0.99)
						{
							faccon = theta/(1.0 - theta);
							double dyth = faccon*dyno*pow(theta, nit-1-newt)/fnewt;
							if(dyth >= 1.0)
							{
								double qnewt = max(1.0e-4, min(20.0, dyth));
								hhfac = 0.8*pow(qnewt, -1.0/(4.0+nit-1-newt));
								h *= hhfac;
								reject = true;
								last = false;
								if(!caljac)
									ComputeJacobian(N, numBlock, numThreads, njac, caljac, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &arrayJac[N*N*idB]);
								loop = true; 
								break;
							}
						}
						else
						{
							if(ier[idB] != 0)
							{
								nsing++;
								if(nsing >= 5)
								{
									if(verbose > 2)
										printf(" * Thread %d exit of RADAU at t = %.4e, matrix is repeatedly singular\n", idB, t);
									
									saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
									printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
									return;
								}
							}
							h *= 0.5;
							hhfac = 0.5;
							reject = true;
							last = false;
							if(!caljac)
								ComputeJacobian(N, numBlock, numThreads, njac, caljac, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &arrayJac[N*N*idB]);
							loop = true;
							break;
						}
					}
					dynold = max(dyno, uround);

					updateVectors2<<<numBlock, numThreads>>>(N, &z1[N*idB], &z2[N*idB], &z3[N*idB], &f1[N*idB], &f2[N*idB], &f3[N*idB]);
					cudaDeviceSynchronize();

					if(faccon*dyno <= fnewt)
						break;
				}

				if(loop)
					break;

				ErrorEstimate(cublasHandle, numBlock, numThreads, N, h, H, ODE, indexH, indexODE,
							&M_feed[idB*N], &c_matrix[idB*M], err, nfcn, first, reject, &z1[idB*N], 
							&z2[idB*N], &z3[idB*N],  &f1[idB*N], &f2[idB*N], &y0[idB*N], &y[idB*N], 
							&cont[4*idB*N], &scal[idB*N], &ip1[idB*N], &ier[idB],
							&pointRealMatrix[idB], &pointRealVectorE[idB]);

				if(ier[idB] == -1)
				{
					saveFailedDynamics(N, idB, numTimes, numSS, tD, cont, cs_vector, dynamics);
					printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
					return;
				}

				double fac = min(safe, cfac/(newt+2*nit));
				quot = max(facr, min(facl, pow(err, 0.25)/fac));
				double hnew = h/quot;

				//  is the error small enough ?
				if(err < 1.0)
				{
					// step is accepted
					first = false;
					naccpt++;
					if(pred)
					{
						// predictive controller of Gustafsson
						if(naccpt > 1)
						{
							double facgus = (hacc/(h))*pow(err*err/erracc, 0.25)/safe;
							facgus = max(facr, min(facl, facgus));
							quot = max(quot,facgus);
							hnew = h/quot;
						}
						hacc = h;
						erracc = max(1.0e-2, err);
					}
					tOld = t;
					hOld = h;
					t = tph;

					updateCont<<<numBlock, numThreads>>>(N, c1, c2, c1m1, c2m1, c1mc2, &y[N*idB], &z1[N*idB], &z2[N*idB], &z3[N*idB], &cont[4*N*idB]);
					cudaDeviceSynchronize();

					updateScalCont<<<numBlock, numThreads>>>(N, rtoler, atoler, &y[N*idB], &scal[N*idB], &cont[4*N*idB]);
					cudaDeviceSynchronize();

					SolutionOutput(N, naccpt, ind, numSS, numTimes, tD, tOld, t, tend, hOld, cs_vector, t_vector,
								&cont[4*N*idB], &dynamics[numSS*numTimes*idB]);
					
					caljac = false;
					
					if (last)
					{
						h = hopt;
						if(t_vector[numTimes-1] == tend)
						{
							for(unsigned i = 0; i < numSS; i++)
							{
								int offset = ind * numSS + numSS*numTimes*idB;
								dynamics[i+offset] = function_continuousOutput(N, cs_vector[i], tD, t, hOld, &cont[4*N*idB]);
							}
						}

						printStatus(idB, nfcn, njac, nstep, naccpt, nrejct, ndec, nsol, verbose);
						return;
					}

					evaluateODE<<<numBlock, numThreads>>>(N, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &y0[N*idB]);
					cudaDeviceSynchronize();

					nfcn++;
					hnew = posneg*min(fabs(hnew), hmaxn);
					hopt = min(h, hnew);

					if(reject)
						hnew = posneg*min(fabs(hnew), fabs(h));

					reject = false;

					if((t + hnew/quot1 - tend)*posneg >= 0.0)
					{
						h = tend - t;
						last = true;
					}
					else
					{
						double qt = hnew/(h);
						hhfac = h;
						if((theta <= thet) && (qt >= quot1) && (qt <= quot2))
							continue;
						h = hnew;
					}
					hhfac = h;
					if(theta > thet)
						ComputeJacobian(N, numBlock, numThreads, njac, caljac, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &arrayJac[N*N*idB]);
					loop = true;
				}
				else
				{
					reject = true;
					last = false;
					if (first)
					{
						h *= 0.1;
						hhfac = 0.1;
					}
					else
					{
						hhfac = hnew/(h);
						h = hnew;
					}
					if(naccpt >= 1)
						nrejct++;
					if(!caljac)
						ComputeJacobian(N, numBlock, numThreads, njac, caljac, H, ODE, indexH, indexODE, &M_feed[N*idB], &c_matrix[M*idB], &y[N*idB], &arrayJac[N*N*idB]);
					loop = true;
				}
				break;
			}
		}
	}
}

#endif