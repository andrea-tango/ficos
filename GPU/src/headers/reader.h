/***************************************************************************
  written by    : Andrea Tangherloni
  last updated  : May 25, 2021
  e-mail:     : andrea.tangherloni@unibg.it

***************************************************************************/

#ifndef READER_H_INCLUDED
#define READER_H_INCLUDED

#include "libraries.h"

class Reader
{
public:

  Reader()
  {

  }

  int readModelkind(string file)
  {
    f = file;
    string line;
    ifstream infile(f.c_str());
    getline(infile, line);
    istringstream buf(line);
    if(strcmp(line.c_str(), "deterministic") == 0 || strcmp(line.c_str(), "concentration") == 0)
      return 0;
    else if(strcmp(line.c_str(), "stochastic") == 0 || strcmp(line.c_str(), "number") == 0)
      return 1;
    else
      return 0;
  }

  vector<double> readByLine(string file)
  {
    vector<double> v;
    f = file;
    string line;
    ifstream infile(f.c_str());
    while (getline(infile, line))
    {
      istringstream buf(line);
      double x = strtod(line.c_str(), NULL);
      v.push_back(x);
    }
    return v;
  }

  vector<int> readByLineInt(string file)
  {
    vector<int> v;
    f = file;
    string line;
    ifstream infile(f.c_str());
    while (getline(infile, line))
    {
      istringstream buf(line);
      int x = (int) strtod(line.c_str(), NULL);
      v.push_back(x);
    }
    return v;
  }

  vector<double> readByTab(string file)
  {
    vector<double> v;
    f = file;
    string line;
    ifstream infile(f.c_str());
    while (getline(infile, line))
    {
      istringstream buf(line);
      for(string token; getline(buf, token, '\t'); )
      {
        double x = strtod(token.c_str(), NULL);
        v.push_back(x);
      }
    }
    return v;
  }

  vector<vector<short> > readStoichiometryMatrix(string file)
  {
    vector<vector<short> > matrix;
    f = file;
    string line;
    ifstream infile(f.c_str());
    vector<short> v1;
    while (getline(infile, line))
    {
      v1.clear();
      istringstream buf(line);
      for(string token; getline(buf, token, '\t'); )
      {
        int x = (int) strtod(token.c_str(), NULL);
        v1.push_back(x);
      }
      matrix.push_back(v1);
    }
    return matrix;

  }

  vector<vector<double> > readMatrix(string file)
  {
    vector<vector<double> > matrix;
    f = file;
    string line;
    ifstream infile(f.c_str());
    vector<double> v1;
    while (getline(infile, line))
    {
      v1.clear();
      istringstream buf(line);
      for(string token; getline(buf, token, '\t'); )
      {
        double x = strtod(token.c_str(), NULL);
        v1.push_back(x);
      }
      matrix.push_back(v1);
    }
    return matrix;

  }

  double readSingleValue(string file)
  {
    f = file;
    string line;
    ifstream infile(f.c_str());
    getline(infile, line);
    istringstream buf(line);
    return strtod(line.c_str(), NULL);
  }



private:
  string f;
};
#endif //READER_H_INCLUDED