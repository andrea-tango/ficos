/***************************************************************************
	written by		: Andrea Tangherloni
	last updated	: May 25, 2021
	e-mail:			: andrea.tangherloni@unibg.it

***************************************************************************/

#ifndef SIMULATION_H_INCLUDED
#define SIMULATION_H_INCLUDED

#include "libraries.h"
using namespace std;

void runSimulation(string pathIn, string pathOut, string left_sideIn,
				string right_sideIn, string t_vectorIn, string MX_0In,
				string M_0In, string c_matrixIn, string c_vectorIn,
				string M_feedIn, string modelkindIn, string cs_vectorIn,
				string atol_vectorIn, string rtolIn, string max_stepsIn,
				string ts_matrixIn, int GPU, int verbose, int heuristic);


int count_nonzeroRow(vector<vector<short> > A, int i);
void conversionToDeterministic(double conversionFactor, vector<vector<double> > &MX_0,
	vector<vector<double> > &M_feed, vector<vector<double> > &c_matrix,
	vector<vector<short> > left_side, int verbose);

vector<double> toVector(vector<vector<double> > A);
vector<double> toVectorTS_matrix(vector<vector<double> > A);
void boolFeed(vector<double> &vect);

void createH(vector<vector<short> > A, vector<vector<short> > B, vector<short2> &H1, vector<int> &index);
void createODEstruct(vector<vector<short> > A, vector<short2> &ODE, vector<int> &index);

void printODEs(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *feed);
void printJacobian(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *feed);


void returnGPU_properties(long *gpuProp);
int minThread(int n1, int n2);
void returnBlockThread(int sm, int thread, int* info);

int calculateNumBlocksShared(int N);

#endif // SIMULATION_H_INCLUDED