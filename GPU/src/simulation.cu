/***************************************************************************
	written by		: Andrea Tangherloni
	last updated	: May 25, 2021
	e-mail:			: andrea.tangherloni@unibg.it

***************************************************************************/

#ifdef _WIN32
	#include "headers\\reader.h"
	#include "headers\\simulation.cuh"
	#include "headers\\kernelsSimulation.cuh"
	#include "headers\\kernelsDOPRI.cuh"
	#include "headers\\kernelsRADAU.cuh"
	const string slash = "\\";
#elif _WIN64
	#include "headers\\reader.h"
	#include "headers\\simulation.cuh"
	#include "headers\\kernelsSimulation.cuh"
	#include "headers\\kernelsDOPRI.cuh"
	#include "headers\\kernelsRADAU.cuh"
	const string slash = "\\";
#else
	#include "headers/reader.h"
	#include "headers/simulation.cuh"
	#include "headers/kernelsSimulation.cuh"
	#include "headers/kernelsDOPRI.cuh"
	#include "headers/kernelsRADAU.cuh"
const string slash = "/";
#endif

#define DEEP_ERROR_CHECK
#define CUDA_ERROR_CHECK

inline void __cudaCheckError(const char *file, const int line)
{
#ifdef CUDA_ERROR_CHECK

#ifdef DEEP_ERROR_CHECK
	cudaThreadSynchronize();
#endif 

#ifdef _DEBUG

	cudaError err = cudaGetLastError();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "cudaCheckError() failed at %s:%i : %s\n", file, line, cudaGetErrorString(err));
		system("pause");
		exit(-1);
	}

#endif 
#endif
	return;
}
#define CudaCheckError()    __cudaCheckError( __FILE__, __LINE__ )

inline void __cudaCheckErrorRelease(const char *file, const int line)
{
#ifdef CUDA_ERROR_CHECK

	cudaThreadSynchronize();
	cudaError err = cudaGetLastError();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "cudaCheckError() failed at %s:%i : %s\n", file, line, cudaGetErrorString(err));
		// system("pause");
		exit(-200);
	}
	return;
}
#define CudaCheckErrorRelease()    __cudaCheckErrorRelease( __FILE__, __LINE__ )
#endif

void runSimulation(string pathIn, string pathOut, string left_sideIn,
				string right_sideIn, string t_vectorIn, string MX_0In,
				string M_0In, string c_matrixIn, string c_vectorIn,
				string M_feedIn, string modelkindIn, string cs_vectorIn,
				string atol_vectorIn, string rtolIn, string max_stepsIn,
				string ts_matrixIn, int GPU, int verbose, int heuristic)
{

	Reader* reader = new Reader;

	int modelkind = 0;
	bool fitness  = false;
	double volume = 0.0;
	double conversionFactor = 0.0;

	if(strcmp(modelkindIn.c_str(), "NA") != 0)
	{
		modelkind = reader -> readModelkind(modelkindIn);
	}

	if(modelkind)
	{
		volume = reader -> readSingleValue(pathIn+slash+"volume");
		if(volume == 0)
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << " * ERROR: the modelkind is stochastic/number, but there is not the volume file\n";
			cout << "***************************************************************\n";
			exit(-7);
		}
		double avogadro = 6.022e23;
		conversionFactor =  volume*avogadro;
	}

	// read Stoichiometry Matrices
	vector<vector<short> > left_side  = reader -> readStoichiometryMatrix(left_sideIn);
	vector<vector<short> > right_side = reader -> readStoichiometryMatrix(right_sideIn);
	vector<vector<double> > MX_0;
	vector<vector<double> > c_matrix;
	vector<vector<double> > M_feed;
	vector<vector<double> > ts_matrix;

	vector<double> t_vector;
	vector<double> atol_vector;
	vector<int> cs_vector;
	
	double rtol = 1e-6;
	int max_steps = 10000;

	if(left_side.size() != right_side.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << left_side.size() << " rows in the left side matrix," << endl;
		cout << "          but there are " << right_side.size() << " rows in the right side matrix" << endl;
		cout << "***************************************************************\n";
		exit(-8);
	}

	if(left_side[0].size() != right_side[0].size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << left_side[0].size() << " columns in the left side matrix," << endl;
		cout << "          but there are " << right_side[0].size() << " columns in the right side matrix" << endl;
		cout << "***************************************************************\n";
		exit(-9);
	}


	if(strcmp(MX_0In.c_str(), "NA") != 0)
	{
		MX_0     = reader -> readMatrix(MX_0In);
		c_matrix = reader -> readMatrix(c_matrixIn);
	}
	else
	{
		vector<double> M_0      = reader -> readByTab(M_0In);
		vector<double> c_vector = reader -> readByLine(c_vectorIn);
		MX_0.push_back(M_0);
		c_matrix.push_back(c_vector);
	}

	if(MX_0[0].size() != left_side[0].size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << MX_0[0].size() << " initial conditions," << endl;
		cout << "          but there are " << left_side[0].size() << " species" << endl;
		cout << "***************************************************************\n";
		exit(-10);
	}

	if(c_matrix[0].size() != left_side.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << c_matrix[0].size() << " kinetic paramaters," << endl;
		cout << "          but there are " << left_side.size() << " reactions" << endl;
		cout << "***************************************************************\n";
		exit(-11);
	} 

	if(MX_0.size() != c_matrix.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";		cout << " * ERROR: there are " << MX_0.size() << " different initial conditions," << endl;
		cout << "          but there are " << c_matrix.size() << " different parameterizations" << endl;
		cout << "***************************************************************\n";
		exit(-12);
	}

	t_vector = reader -> readByLine(t_vectorIn);

	// Optional paramaters
	if(strcmp(M_feedIn.c_str(), "NA") != 0)
	{
		M_feed = reader -> readMatrix(M_feedIn);

		if(M_feed.size() == 1)
		{
			for(int i = 0; i < MX_0.size()-1; i++)
				M_feed.push_back(M_feed[0]);

		}
	}
	else
	{	
		for(int i = 0; i < MX_0.size(); i++)
		{
			vector<double> v;
			for(int j = 0; j < MX_0[0].size(); j++)
				v.push_back(0.0);

			M_feed.push_back(v);
		}
	}

	if(MX_0.size() != M_feed.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << MX_0.size() << " different initial conditions," << endl;
		cout << "          but there are " << M_feed.size() << " different feed parameterizations" << endl;
		cout << "***************************************************************\n";
		exit(-13);
	}

	if(MX_0[0].size() != M_feed[0].size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << MX_0[0].size() << " initial conditions," << endl;
		cout << "          but there are " << M_feed[0].size() << " feed" << endl;
		cout << "***************************************************************\n";
		exit(-14);
	}

	if(strcmp(cs_vectorIn.c_str(), "NA") != 0)
	{
		cs_vector = reader -> readByLineInt(cs_vectorIn);
	}
	else
	{
		for(int i = 0; i < MX_0[0].size(); i++)
			cs_vector.push_back(i);
	}

	if(strcmp(atol_vectorIn.c_str(), "NA") != 0)
	{
		atol_vector = reader -> readByLine(atol_vectorIn);
	}
	else
	{
		for(int i = 0; i < MX_0[0].size(); i++)
			atol_vector.push_back(1e-12);
	}

	if(strcmp(rtolIn.c_str(), "NA") != 0)
	{
		rtol =  reader -> readSingleValue(rtolIn);
	}

	if(strcmp(max_stepsIn.c_str(), "NA") != 0)
	{
		max_steps =  (int)(reader -> readSingleValue(max_stepsIn));
	}

	if(strcmp(ts_matrixIn.c_str(), "NA") != 0)
	{
		ts_matrix =  reader -> readMatrix(ts_matrixIn);
	
		if(cs_vector.size() != ts_matrix[0].size()-1)
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << " * ERROR: there are " << cs_vector.size() << " species to be sampled," << endl;
			cout << "          but there are " << ts_matrix[0].size()-1 << " in the target matrix" << endl;
			cout << "***************************************************************\n";
			exit(-101);
		}

		if(ts_matrix.size() != t_vector.size())
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << " * ERROR: ts_matrix and t_vector must be have the same length" << endl;
			cout << "***************************************************************\n";
			exit(-102);
		}

		for(int i=0; i < ts_matrix.size(); i++)
		{
			if(fabs(ts_matrix[i][0] - t_vector[i]) > 1e-5)
			{
				if(!verbose)
					cout << "***************************************************************\n";
				cout << " * ERROR: ts_matrix and t_vector must be have the same values" << endl;
				cout << "***************************************************************\n";
				exit(-103);
			}
		}

		fitness = true;
	}

	if(MX_0[0].size() != atol_vector.size())
	{
		if(!verbose)
			cout << "***************************************************************\n";
		cout << " * ERROR: there are " << MX_0[0].size() << " species," << endl;
		cout << "          but there are " << atol_vector.size() << " values for absolute tolerances" << endl;
		cout << "***************************************************************\n";
		exit(-15);
	}

	for(int i=0; i < cs_vector.size(); i++)
	{
		if(cs_vector[i] > MX_0[0].size()-1)
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << " * ERROR: there are " << MX_0[0].size() << " species," << endl;
			cout << "          so the last species that can be sampled is " << MX_0[0].size()-1<< endl;
			cout << "***************************************************************\n";
			exit(-16);
		}
	}

	// Convertion stochastic into deterministic
	if(modelkind)
		conversionToDeterministic(conversionFactor, MX_0, M_feed, c_matrix, left_side, verbose);

	if(verbose)
	{
		cout << "***************************************************************";
		cout << "\nFiCoS: Fine- and Coarse-grained Simulator (GPU)\n";
		cout << "\nReading BioSimWare project from the folder: " << pathIn.c_str() << endl;
		
		if(modelkind)
		{
			cout << " * Stochastic model detected" << endl;
			printf(" * Read volume: %.8e\n", volume);
			printf(" * Conversion constant: %.8e\n", conversionFactor);
			cout << " * Stochastic model converted to deterministic" << endl;

		}
		else
			cout << " * Deterministic model detected" << endl;

		cout << " * The model has " << left_side[0].size() << " species" << endl;
		cout << " * The model has " << left_side.size() << " reactions" << endl;
		cout << " * Feed conditions loaded" << endl;
		cout << " * " << MX_0.size() << " different initial conditions loaded" << endl;
		cout << " * " << c_matrix.size() << " different parameterizations loaded" << endl;
		cout << " * " << cs_vector.size() << " species to be sampled" << endl;
		cout << " * " << t_vector.size() << " points to be sampled" << endl;
		cout << " * Absolute error tolerances set to " << atol_vector[0] << endl;
		cout << " * Relative error tolerance set to " << rtol << endl;
		cout << " * Maximum number of steps set to " << max_steps << endl;
	}

	/* ********************************************************************************************************************************************* */
	/* ******************** creating structure cudaFuncCachePreferShareddata to save differential equations ******************** */
	
	CUDA_CHECK_RETURN(cudaDeviceSetCacheConfig(cudaFuncCachePreferL1));

	int N          = MX_0[0].size();
	int M          = c_matrix[0].size();
	int numSS      = cs_vector.size();
	int numTimes   = t_vector.size();

	int numSim             = MX_0.size();
	int initialConditions  = MX_0.size()*MX_0[0].size();
	int reactionConditions = c_matrix.size()*c_matrix[0].size();

	vector<short2> H;
	vector<short2> ODE;
	vector<int> indexH;
	vector<int> indexODE;
	createH(left_side, right_side, H, indexH);
	createODEstruct(left_side, ODE, indexODE);

	vector<double> c_matrixLin = toVector(c_matrix);
	vector<double> MX_0Lin     = toVector(MX_0);
	vector<double> M_feedLin   = toVector(M_feed);
	boolFeed(M_feedLin);

	// creating pointer for CUDA
	double *c_matrixPointer    = &c_matrixLin[0];
	double *MX_0Pointer        = &MX_0Lin[0];
	double *M_feedPointer      = &M_feedLin[0];
	double *t_vectorPointer    = &t_vector[0];
	double *atol_vectorPointer = &atol_vector[0];
	int *cs_vectorPointer      = &cs_vector[0];

	short2 *HPointer        = &H[0];
	short2 *ODEPointer      = &ODE[0];
	int *indexHPointer      = &indexH[0];
	int *indexODEPointer    = &indexODE[0];


	long *gpuProp     = (long*)malloc(3 * sizeof (long));
	returnGPU_properties(gpuProp);
 
	short2 *H_GPU;
	short2 *ODE_GPU;
	int *indexH_GPU;
	int *indexODE_GPU;
	int *selectionMethod_GPU;
	int *cs_vector_GPU;

	double *MX_0_GPU;
	double *c_matrix_GPU;
	double *M_feed_GPU;
	double *atoler_GPU;
	double *t_vector_GPU;

	double *eigen_GPU;
	double *jac_GPU;
	double *norms_GPU;


	cudaEvent_t start, stop;
	cudaEventCreate(&start);  
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);

	CUDA_CHECK_RETURN(cudaSetDevice(GPU));

	//MALLOCC

	CUDA_CHECK_RETURN(cudaMalloc((void**) &H_GPU, H.size() * sizeof(short2)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &ODE_GPU, ODE.size() * sizeof(short2)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &indexH_GPU, indexH.size() * sizeof(int)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &indexODE_GPU, indexODE.size() * sizeof(int)));

	CUDA_CHECK_RETURN(cudaMalloc((void**) &MX_0_GPU, initialConditions * sizeof(double)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &M_feed_GPU, initialConditions * sizeof(double)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &c_matrix_GPU, reactionConditions * sizeof(double)));
	
	CUDA_CHECK_RETURN(cudaMalloc((void**) &atoler_GPU, N * sizeof(double)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &t_vector_GPU, numTimes * sizeof(double)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &cs_vector_GPU, numSS * sizeof(double)));

	CUDA_CHECK_RETURN(cudaMalloc((void**) &eigen_GPU, numSim * sizeof(double)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &selectionMethod_GPU, numSim * sizeof(int)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &jac_GPU, N*N*numSim * sizeof(double)));
	CUDA_CHECK_RETURN(cudaMalloc((void**) &norms_GPU, N*numSim * sizeof(double)));


	//MEMCPY
	CUDA_CHECK_RETURN(cudaMemcpy(H_GPU, HPointer, H.size() * sizeof(short2), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(ODE_GPU, ODEPointer, ODE.size() * sizeof(short2), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(indexH_GPU, indexHPointer, indexH.size() * sizeof(int), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(indexODE_GPU, indexODEPointer, indexODE.size() * sizeof(int), cudaMemcpyHostToDevice));

	CUDA_CHECK_RETURN(cudaMemcpy(MX_0_GPU, MX_0Pointer, initialConditions * sizeof(double), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(c_matrix_GPU, c_matrixPointer, reactionConditions * sizeof(double), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(M_feed_GPU, M_feedPointer, initialConditions * sizeof(double), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(atoler_GPU, atol_vectorPointer, N * sizeof(double), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(t_vector_GPU, t_vectorPointer, numTimes * sizeof(double), cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(cs_vector_GPU, cs_vectorPointer, numSS * sizeof(double), cudaMemcpyHostToDevice));

	int *blocksThreads = (int*)malloc(2 * sizeof (int));	
	int SMs = (int)gpuProp[0];

	int numThreads = 0;
	int numBlocks  = 0;

	int numThreadsSIM = 0;
	int numBlocksSIM  = 0;

	int numThreadsDOPRI = 0;
	int numBlocksDOPRI  = 0;

	int numThreadsRADAU = 0;
	int numBlocksRADAU  = 0;

	if(heuristic)
	{
		if(verbose)	
			cout << " * Using heuristic to select the number of threads and blocks" << endl;

		returnBlockThread(SMs, N, blocksThreads);
		numThreads = blocksThreads[0];
		numBlocks  = blocksThreads[1];

	}
	else
	{
		numThreads = 32;
		numBlocks   = max(1, (int) ceil(N/32.0));
	}


	if(heuristic)
	{
	
		returnBlockThread(SMs, numSim, blocksThreads);
		numThreadsSIM = blocksThreads[0];
		numBlocksSIM  = blocksThreads[1];

	}
	else
	{
		numThreadsSIM = 32;
		numBlocksSIM  = max(1, (int) ceil(numSim/32.0));
	}

	
	if(verbose > 1)
	{
		cout << endl;
		cout << " * ODEs:" << endl;
		printODEs(N, HPointer, ODEPointer, indexHPointer, indexODEPointer, M_feedPointer);
	}
	if(verbose > 2)
	{
		cout << endl;
		cout << " * Jacobian matrix:" << endl;
		printJacobian(N, HPointer, ODEPointer, indexHPointer, indexODEPointer, M_feedPointer);
	}
	
	checkStartMethod<<<numBlocksSIM, numThreadsSIM>>>(N, M, numSim,
		numThreads, numBlocks, H_GPU, ODE_GPU,
		indexH_GPU, indexODE_GPU, M_feed_GPU, c_matrix_GPU,
		MX_0_GPU, jac_GPU, norms_GPU, eigen_GPU, selectionMethod_GPU,
		verbose);
	CudaCheckError();
	CUDA_CHECK_RETURN(cudaDeviceSynchronize());


	int *selectionMethod = (int*)malloc(numSim * sizeof (int));
	CUDA_CHECK_RETURN(cudaMemcpy(selectionMethod, selectionMethod_GPU, numSim*sizeof(int), cudaMemcpyDeviceToHost));

	CUDA_CHECK_RETURN(cudaFree(MX_0_GPU));
	CUDA_CHECK_RETURN(cudaFree(M_feed_GPU));
	CUDA_CHECK_RETURN(cudaFree(c_matrix_GPU));
	CUDA_CHECK_RETURN(cudaFree(eigen_GPU));
	CUDA_CHECK_RETURN(cudaFree(selectionMethod_GPU));
	CUDA_CHECK_RETURN(cudaFree(jac_GPU));
	CUDA_CHECK_RETURN(cudaFree(norms_GPU));

	int numDOPRI = 0;
	int numRADAU = 0;
	double *dynamicsDOPRI_GPU;
	double *dynamicsRADAU_GPU;
	int *dopriFails_GPU;

	for(int i=0; i<numSim; i++)
	{
		if(selectionMethod[i] == 0)
			numDOPRI++;
	}

	if(verbose)
	{
		cout << endl;
		cout << " * " << gpuProp[0] << " multiprocessors detected" << endl;
		cout << " * " << gpuProp[1] << " bytes of global memory detected" << endl;
		cout << " * Using " << numThreads << " threads and " << numBlocks << " blocks for dynamic parallelism" << endl;
	}

	int *dopriFails  = (int*)malloc(numDOPRI * sizeof (int));
	double *dynamicsDOPRI = (double*)malloc(numSS*numTimes*numDOPRI * sizeof (double));
	int *indexThreadDOPRI = (int*)malloc(numDOPRI * sizeof (int));

	if (numDOPRI > 0)
	{

		if(heuristic)
		{
		
			returnBlockThread(SMs, numDOPRI, blocksThreads);
			numThreadsDOPRI = blocksThreads[0];
			numBlocksDOPRI  = blocksThreads[1];

		}
		else
		{
			numThreadsDOPRI = 32;
			numBlocksDOPRI  = max(1, (int) ceil(numDOPRI/32.0));
		}



		if(verbose)
		{
			cout << endl;
			cout << " * Using " << numThreadsDOPRI << " threads and " << numBlocksDOPRI << " blocks for DOPRI method" << endl;
			cout << " * Launching " << numDOPRI << " simulations on GPU " << GPU << endl;
		}

		int countD = 0;
		for(int i=0; i < numSim; i++)
		{
			if(selectionMethod[i] == 0)
			{
				indexThreadDOPRI[countD] = i;
				countD++;
			}
		}

		double *MX_0_DOPRI    = (double*)malloc(N*numDOPRI * sizeof (double));
		double *M_feedDOPRI   = (double*)malloc(N*numDOPRI * sizeof (double));
		double *c_matrixDOPRI = (double*)malloc(M*numDOPRI * sizeof (double));

		for(int i=0; i < numDOPRI; i++)
		{
			int idx = indexThreadDOPRI[i];
			for(int j=0; j < N; j++)
			{
				MX_0_DOPRI[i*N+j] = MX_0Pointer[idx*N+j];
			}
			for(int j=0; j < N; j++)
			{
				M_feedDOPRI[i*N+j] = M_feedPointer[idx*N+j];
			}
			for(int j=0; j < M; j++)
			{
				c_matrixDOPRI[i*M+j] = c_matrixPointer[idx*M+j];
			}
		}
		
		// DOPRI ARRAYS
		double *M_feedDOPRI_GPU;
		double *c_matrixDOPRI_GPU;
		double *yD;

		// working arrays used for dense output
		double *rcont1D;
		double *rcont2D;
		double *rcont3D;
		double *rcont4D;
		double *rcont5D;
		
		// vectors used in integration steps
		double *fD;
		double *yy1D;
		double *k1D;
		double *k2D;
		double *k3D;
		double *k4D;
		double *k5D;
		double *k6D;
		double *ystiD;

		CUDA_CHECK_RETURN(cudaMalloc((void**) &M_feedDOPRI_GPU,   N*numDOPRI * sizeof(double)));	
		CUDA_CHECK_RETURN(cudaMalloc((void**) &c_matrixDOPRI_GPU, M*numDOPRI * sizeof(double)));

		CUDA_CHECK_RETURN(cudaMalloc((void**) &yD,      N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &rcont1D, N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &rcont2D, N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &rcont3D, N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &rcont4D, N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &rcont5D, N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &fD,      N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &yy1D,    N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &k1D,     N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &k2D,     N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &k3D,     N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &k4D,     N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &k5D,     N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &k6D,     N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &ystiD,   N*numDOPRI * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &dopriFails_GPU, numDOPRI * sizeof(int)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &dynamicsDOPRI_GPU, numSS*numTimes*numDOPRI * sizeof(double)));

		CUDA_CHECK_RETURN(cudaMemcpy(yD, MX_0_DOPRI, numDOPRI*N * sizeof(double), cudaMemcpyHostToDevice));
		CUDA_CHECK_RETURN(cudaMemcpy(M_feedDOPRI_GPU, M_feedDOPRI, numDOPRI*N * sizeof(double), cudaMemcpyHostToDevice));	
		CUDA_CHECK_RETURN(cudaMemcpy(c_matrixDOPRI_GPU, c_matrixDOPRI, numDOPRI*M * sizeof(double), cudaMemcpyHostToDevice));

		dopriMethod<<<numBlocksDOPRI, numThreadsDOPRI>>>(N, M, numDOPRI, numThreads, numBlocks,
				H_GPU, ODE_GPU, indexH_GPU, indexODE_GPU, M_feedDOPRI_GPU, c_matrixDOPRI_GPU,
				yD, rcont1D, rcont2D, rcont3D, rcont4D, rcont5D, fD, yy1D, k1D,
				k2D, k3D, k4D, k5D, k6D, ystiD, numSS, numTimes, max_steps, rtol, atoler_GPU, 
				t_vector_GPU, cs_vector_GPU, dynamicsDOPRI_GPU, dopriFails_GPU, verbose);
		CudaCheckError();
		CUDA_CHECK_RETURN(cudaDeviceSynchronize());
		
		// int *dopriFails  = (int*)malloc(numDOPRI * sizeof (int));
		CUDA_CHECK_RETURN(cudaMemcpy(dopriFails, dopriFails_GPU, numDOPRI*sizeof(int), cudaMemcpyDeviceToHost));

		CUDA_CHECK_RETURN(cudaFree(M_feedDOPRI_GPU));	
		CUDA_CHECK_RETURN(cudaFree(c_matrixDOPRI_GPU));
		CUDA_CHECK_RETURN(cudaFree(yD));
		CUDA_CHECK_RETURN(cudaFree(rcont1D));
		CUDA_CHECK_RETURN(cudaFree(rcont2D));
		CUDA_CHECK_RETURN(cudaFree(rcont3D));
		CUDA_CHECK_RETURN(cudaFree(rcont4D));
		CUDA_CHECK_RETURN(cudaFree(rcont5D));
		CUDA_CHECK_RETURN(cudaFree(fD));
		CUDA_CHECK_RETURN(cudaFree(yy1D));
		CUDA_CHECK_RETURN(cudaFree(k1D));
		CUDA_CHECK_RETURN(cudaFree(k2D));
		CUDA_CHECK_RETURN(cudaFree(k3D));
		CUDA_CHECK_RETURN(cudaFree(k4D));
		CUDA_CHECK_RETURN(cudaFree(k5D));
		CUDA_CHECK_RETURN(cudaFree(k6D));
		CUDA_CHECK_RETURN(cudaFree(ystiD));
	}

	// check if dopri fails
	for(int id=0; id < numDOPRI; id++)
	{
		int fail = dopriFails[id];
		if(fail==1)
		{
			numRADAU++;
		}
	}
	
	for(int i=0; i<numSim; i++)
	{
		if(selectionMethod[i] == 1)
			numRADAU++;
	}

	double *dynamicsRADAU = (double*)malloc(numSS*numTimes*numRADAU * sizeof (double));
	int *indexThreadRADAU = (int*)malloc(numRADAU * sizeof (int));

	if(numRADAU > 0)
	{
		int countR = 0;
		for(int id=0; id < numDOPRI; id++)
		{
			int fail = dopriFails[id];
			if(fail==1)
			{
				int idx = indexThreadDOPRI[id];
				indexThreadRADAU[countR] = idx;
				countR++;
			}
		}

		
		for(int i=0; i < numSim; i++)
		{
			if(selectionMethod[i] == 1)
			{
				indexThreadRADAU[countR] = i;
				countR++;
			}
		}

		if(heuristic)
		{
		
			returnBlockThread(SMs, numRADAU, blocksThreads);
			numThreadsRADAU = blocksThreads[0];
			numBlocksRADAU  = blocksThreads[1];

		}
		else
		{
			numThreadsRADAU = 32;
			numBlocksRADAU  = max(1, (int) ceil(numRADAU/32.0));
		}

		if(verbose)
		{
			cout << endl;
			cout << " * Using " << numThreadsRADAU << " threads and " << numBlocksRADAU << " blocks for RADAU method" << endl;
			cout << " * Launching " << numRADAU << " simulations on GPU " << GPU << endl;
		}


		double *MX_0_RADAU    = (double*)malloc(N*numRADAU * sizeof (double));
		double *M_feedRADAU   = (double*)malloc(N*numRADAU * sizeof (double));
		double *c_matrixRADAU = (double*)malloc(M*numRADAU * sizeof (double));

		for(int i=0; i < numRADAU; i++)
		{
			int idx = indexThreadRADAU[i];
			for(int j=0; j < N; j++)
			{
				MX_0_RADAU[i*N+j] = MX_0Pointer[idx*N+j];
			}
			for(int j=0; j < N; j++)
			{
				M_feedRADAU[i*N+j] = M_feedPointer[idx*N+j];
			}
			for(int j=0; j < M; j++)
			{
				c_matrixRADAU[i*M+j] = c_matrixPointer[idx*M+j];
			}
		}

		double rtoler = 0.1*pow(rtol, 2.0/3.0);
		for(int i=0; i < N;i++)
		{
			double quot           = atol_vectorPointer[i]/rtol;
			atol_vectorPointer[i] = rtoler*quot;
		}
		
		double *M_feedRADAU_GPU;
		double *c_matrixRADAU_GPU;

		double *yR;
		double *y0;
		double *cont;
		double *scal;
		
		double *z1;
		double *z2;
		double *z3;

		double *f1;
		double *f2;
		double *f3;

		double *arrayJac;
		double *e1;
		cuDoubleComplex *e2r;
		cuDoubleComplex *z;

		CUDA_CHECK_RETURN(cudaMalloc((void**) &M_feedRADAU_GPU, N*numRADAU * sizeof(double)));	
		CUDA_CHECK_RETURN(cudaMalloc((void**) &c_matrixRADAU_GPU, M*numRADAU * sizeof(double)));

		CUDA_CHECK_RETURN(cudaMalloc((void**) &yR,   N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &y0,   N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &z1,   N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &z2,   N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &z3,   N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &z,    N*numRADAU * sizeof(cuDoubleComplex)));

		CUDA_CHECK_RETURN(cudaMalloc((void**) &scal, N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &f1,   N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &f2,   N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &f3,   N*numRADAU * sizeof(double)));

		CUDA_CHECK_RETURN(cudaMalloc((void**) &cont, 4*N*numRADAU * sizeof(double)));

		CUDA_CHECK_RETURN(cudaMalloc((void**) &e1,   N*N*numRADAU * sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &e2r,  N*N*numRADAU * sizeof(cuDoubleComplex)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &arrayJac, N*N*numRADAU * sizeof(double)));		

		CUDA_CHECK_RETURN(cudaMalloc((void**) &dynamicsRADAU_GPU, numSS*numTimes*numRADAU * sizeof(double)));

		// FOR USING CUBLAS ROUTINES
		double **pointRealMatrix, **pointRealVector, **pointRealVectorE;
		cuDoubleComplex **pointComplexMatrix, **pointComplexVector;
		int *ip1, *ip2, *ier;


		CUDA_CHECK_RETURN(cudaMalloc((void**)&pointRealMatrix, numRADAU*sizeof(double*)));
		CUDA_CHECK_RETURN(cudaMalloc((void**)&pointRealVector, numRADAU*sizeof(double*)));
		CUDA_CHECK_RETURN(cudaMalloc((void**)&pointRealVectorE, numRADAU*sizeof(double*)));
		CUDA_CHECK_RETURN(cudaMalloc((void**)&pointComplexMatrix, numRADAU*sizeof(cuDoubleComplex*)));
		CUDA_CHECK_RETURN(cudaMalloc((void**)&pointComplexVector, numRADAU*sizeof(cuDoubleComplex*)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &ip1,  N*numRADAU * sizeof(int)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &ip2,  N*numRADAU * sizeof(int)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &ier,  numRADAU * sizeof(int)));

		CUDA_CHECK_RETURN(cudaMemcpy(yR, MX_0_RADAU, numRADAU*N * sizeof(double), cudaMemcpyHostToDevice));
		CUDA_CHECK_RETURN(cudaMemcpy(M_feedRADAU_GPU, M_feedRADAU, numRADAU*N * sizeof(double), cudaMemcpyHostToDevice));	
		CUDA_CHECK_RETURN(cudaMemcpy(c_matrixRADAU_GPU, c_matrixRADAU, numRADAU*M * sizeof(double), cudaMemcpyHostToDevice));

		CUDA_CHECK_RETURN(cudaMemcpy(atoler_GPU, atol_vectorPointer, N * sizeof(double), cudaMemcpyHostToDevice));

		radauMethod<<<numBlocksRADAU, numThreadsRADAU>>>(N, M, numRADAU,
		numThreads, numBlocks, H_GPU, ODE_GPU,
		indexH_GPU, indexODE_GPU, M_feedRADAU_GPU, c_matrixRADAU_GPU, yR,
		y0,	cont, scal, z1, z2, z3, z, f1, f2, f3, e1, e2r, arrayJac,
		numSS, numTimes, max_steps, 7, rtoler, atoler_GPU, t_vector_GPU,
		cs_vector_GPU, dynamicsRADAU_GPU, verbose,
		pointRealMatrix,
		pointRealVector,
		pointRealVectorE,
		pointComplexMatrix,
		pointComplexVector,
		ip1, ip2, ier);
		CudaCheckError();
		CUDA_CHECK_RETURN(cudaDeviceSynchronize());

		CUDA_CHECK_RETURN(cudaFree(pointRealMatrix));
		CUDA_CHECK_RETURN(cudaFree(pointRealVector));
		CUDA_CHECK_RETURN(cudaFree(pointRealVectorE));
		CUDA_CHECK_RETURN(cudaFree(pointComplexMatrix));
		CUDA_CHECK_RETURN(cudaFree(pointComplexVector));
		CUDA_CHECK_RETURN(cudaFree(ip1));
		CUDA_CHECK_RETURN(cudaFree(ip2));
		CUDA_CHECK_RETURN(cudaFree(ier));
		CUDA_CHECK_RETURN(cudaFree(M_feedRADAU_GPU));	
		CUDA_CHECK_RETURN(cudaFree(c_matrixRADAU_GPU));
		CUDA_CHECK_RETURN(cudaFree(yR));
		CUDA_CHECK_RETURN(cudaFree(y0));
		CUDA_CHECK_RETURN(cudaFree(z1));
		CUDA_CHECK_RETURN(cudaFree(z2));
		CUDA_CHECK_RETURN(cudaFree(z3));
		CUDA_CHECK_RETURN(cudaFree(z));
		CUDA_CHECK_RETURN(cudaFree(scal));
		CUDA_CHECK_RETURN(cudaFree(f1));
		CUDA_CHECK_RETURN(cudaFree(f2));
		CUDA_CHECK_RETURN(cudaFree(f3));
		CUDA_CHECK_RETURN(cudaFree(cont));
		CUDA_CHECK_RETURN(cudaFree(e1));
		CUDA_CHECK_RETURN(cudaFree(e2r));
		CUDA_CHECK_RETURN(cudaFree(arrayJac));	

	}

	CUDA_CHECK_RETURN(cudaEventRecord( stop, 0 ));
	cudaEventSynchronize( stop );
	float tempo;
	cudaEventElapsedTime( &tempo, start, stop );
	tempo /= 1000;

	cudaEventDestroy(start); 
	cudaEventDestroy(stop);
	
	if (numDOPRI>0)
		CUDA_CHECK_RETURN(cudaMemcpy(dynamicsDOPRI, dynamicsDOPRI_GPU, numSS*numTimes*numDOPRI*sizeof(double), cudaMemcpyDeviceToHost));
	
	if (numRADAU>0)
		CUDA_CHECK_RETURN(cudaMemcpy(dynamicsRADAU, dynamicsRADAU_GPU, numSS*numTimes*numRADAU*sizeof(double), cudaMemcpyDeviceToHost));

	if(!fitness)
	{
		cout << endl;
		printf("Integration time: %.8f\n", tempo);
		
		// saving dynamics DOPRI and RADAU
		for(int id=0; id < numDOPRI; id++)
		{
			int fail = dopriFails[id];
			if(fail==0)
			{
				int idx = indexThreadDOPRI[id];

				FILE * fl;
				string path = pathOut + slash + "Solution" + to_string(idx);
				fl=fopen(path.c_str(), "w");

				if(t_vectorPointer[0] == 0.0)
				{

					fprintf(fl, "%.8e\t", t_vectorPointer[0]);
					for(int j = 0; j < numSS; j++)
					{
						int pos = N*idx + cs_vector[j];
						fprintf(fl, "%.8e\t", MX_0Pointer[pos]);;
					}
					fprintf(fl, "\n");

					for (int i = 1; i < numTimes; i++)
					{
						fprintf (fl, "%.8e\t", t_vector[i]);
						int pos = i*numSS + id*numSS*numTimes;
						for(int j = 0; j < numSS; j++)
						{
							if(j<numSS-1)
								fprintf(fl, "%.8e\t", dynamicsDOPRI[pos]);
							else
								fprintf(fl, "%.8e", dynamicsDOPRI[pos]);
							pos++;
						}
						fprintf(fl, "\n");
					}
				}
				else
				{
					for (int i = 0; i < numTimes; i++)
					{
						fprintf (fl, "%.8e\t", t_vector[i]);
						int pos = i*numSS + id*numSS*numTimes;
						for(int j = 0; j < numSS; j++)
						{
							if(j<numSS-1)
								fprintf(fl, "%.8e\t", dynamicsDOPRI[pos]);
							else
								fprintf(fl, "%.8e", dynamicsDOPRI[pos]);
							pos++;
						}
						fprintf(fl, "\n");
					}
				}
				fclose(fl);	
			}
		}

		for(int id=0; id < numRADAU; id++)
		{
			int idx = indexThreadRADAU[id];

			FILE * fl;
			string path = pathOut + slash + "Solution" + to_string(idx);
			fl=fopen(path.c_str(), "w");

			if(t_vectorPointer[0] == 0.0)
			{
				fprintf(fl, "%.8e\t", t_vectorPointer[0]);
				for(int j = 0; j < numSS; j++)
				{
					int pos = N*idx + cs_vector[j];
					fprintf(fl, "%.8e\t", MX_0Pointer[pos]);
				}
				fprintf(fl, "\n");

				for (int i = 1; i < numTimes; i++)
				{
					fprintf (fl, "%.8e\t", t_vector[i]);
					int pos = i*numSS + id*numSS*numTimes;
					for(int j = 0; j < numSS; j++)
					{
						if(j<numSS-1)
							fprintf(fl, "%.8e\t", dynamicsRADAU[pos]);
						else
							fprintf(fl, "%.8e", dynamicsRADAU[pos]);
						pos++;
					}
					fprintf(fl, "\n");
				}
			}
			else
			{
				for (int i = 0; i < numTimes; i++)
				{
					fprintf (fl, "%.8e\t", t_vector[i]);
					int pos = i*numSS + id*numSS*numTimes;
					for(int j = 0; j < numSS; j++)
					{
						if(j<numSS-1)
							fprintf(fl, "%.8e\t", dynamicsRADAU[pos]);
						else
							fprintf(fl, "%.8e", dynamicsRADAU[pos]);
						pos++;
					}
					fprintf(fl, "\n");
				}
			}
			fclose(fl);
		}
	}

	else
	{
		vector<double> ts_matrixLin = toVectorTS_matrix(ts_matrix);
		double *ts_matrixPointer    = &ts_matrixLin[0];  
		double *fitnessValues       = new double[numSim];
		
		double *dynamicsGPU;
		double *fitnessValuesGPU;
		double *ts_matrixGPU;

		CUDA_CHECK_RETURN(cudaMalloc((void**) &fitnessValuesGPU, numSim*sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &ts_matrixGPU,     numSS*numTimes*sizeof(double)));
		CUDA_CHECK_RETURN(cudaMalloc((void**) &dynamicsGPU,      numSim*numSS*numTimes*sizeof(double)));

		CUDA_CHECK_RETURN(cudaMemcpy(ts_matrixGPU, ts_matrixPointer, numSS*numTimes*sizeof(double), cudaMemcpyHostToDevice));

		for(int id=0; id < numDOPRI; id++)
		{
			int fail = dopriFails[id];
			if(fail==0)
			{
				int idx = indexThreadDOPRI[id];
				CUDA_CHECK_RETURN(cudaMemcpy(&dynamicsGPU[idx*numSS*numTimes], &dynamicsDOPRI_GPU[id*numSS*numTimes], numSS*numTimes*sizeof(double), cudaMemcpyDeviceToDevice));
			}
		}
		for(int id=0; id < numRADAU; id++)
		{
			int idx = indexThreadRADAU[id];
			CUDA_CHECK_RETURN(cudaMemcpy(&dynamicsGPU[idx*numSS*numTimes], &dynamicsRADAU_GPU[id*numSS*numTimes], numSS*numTimes*sizeof(double), cudaMemcpyDeviceToDevice));
		}

		numThreads = 32;
		numBlocks   = max(1, (int) ceil(numSim/32.0));

		calculateFitness<<<numBlocks, numThreads>>>(numSim, numTimes, numSS,	ts_matrixGPU, dynamicsGPU, fitnessValuesGPU);
		CudaCheckError();
		CUDA_CHECK_RETURN(cudaDeviceSynchronize());
		
		CUDA_CHECK_RETURN(cudaMemcpy(fitnessValues, fitnessValuesGPU, numSim*sizeof(double), cudaMemcpyDeviceToHost));

		for(int i=0; i < numSim; i++)
		{
			if(i<numSim-1)
				printf("%.8e\t", fitnessValues[i]);
			else
				printf("%.8e\n", fitnessValues[i]);
		}
	}
}

/* ******************** Convertion stochastic into deterministic ******************** */
int count_nonzeroRow(vector<vector<short> > A, int i)
{
	int count = 0;
	for(int j = 0; j < A[0].size(); j++)
	{
		if(A[i][j] > 0)
			count++;
	}
	return count;
}

void conversionToDeterministic(double conversionFactor, vector<vector<double> > &MX_0,
	vector<vector<double> > &M_feed, vector<vector<double> > &c_matrix,
	vector<vector<short> > left_side, int verbose)
{

	for(int i=0; i < MX_0.size(); i++)
	{
		for(int j=0; j < MX_0[0].size(); j++)
		{
			MX_0[i][j]   = MX_0[i][j]/conversionFactor;
			M_feed[i][j] = M_feed[i][j]/conversionFactor;
		}
	}

	for(int i=0; i < c_matrix[0].size(); i++)
	{

		int value = count_nonzeroRow(left_side, i);
		if(value >= 3)
		{
			if(!verbose)
				cout << "***************************************************************\n";
			cout << "There are one or more reactions with order higher wrt the second" << endl;
			cout << "***************************************************************\n";
			exit(-17);
		}
		else
		{
			int om = 0;
			for(int j = 0; j < left_side[0].size(); j++)
			{
				if(left_side[i][j] >= 2)
				{
					for(int k=0; k < c_matrix.size(); k++)
					{
						c_matrix[k][i] =  (c_matrix[k][i] * conversionFactor)/2.0;
					}
					om = 1;
					break;
				}
			}
			if(om == 0)
			{
				if(count_nonzeroRow(left_side, i) > 1)
				{
					for(int k=0; k < c_matrix.size(); k++)
					{
						c_matrix[k][i] =  (c_matrix[k][i] * conversionFactor);
					}
				}
			}
		}
	}	
}

/* ******************** Methods to creaye ODEs structures ******************** */

void createH(vector<vector<short> > A, vector<vector<short> > B, vector<short2> &H1, vector<int> &index)
{
	vector<short> vect;
	vector<vector<short> > H;

	for(int j = 0; j < A[0].size(); j++)
	{
		vect.clear();
		for(int i = 0; i < A.size(); i++)
		{
			int temp = B[i][j] - A[i][j];
			vect.push_back(temp);
		}
		H.push_back(vect);
	}

	index.push_back(0);
	int count = 0;
	for(int i = 0; i < H.size(); i++)
	{
		for(int j = 0; j < H[0].size(); j++)
		{
			if(H[i][j] != 0)
			{
				short2 tmp;
				tmp.x = j;
				tmp.y = H[i][j];
				H1.push_back(tmp);
				count++;
			}
		}
		index.push_back(count);
	}
}

void createODEstruct(vector<vector<short> > A, vector<short2> &ODE, vector<int> &index)
{
	index.push_back(0);
	int idx = 0;
	for(int i = 0; i < A.size(); i++)
	{
		for(int j = 0; j < A[0].size(); j++)
		{
			if(A[i][j] != 0)
			{
				short2 tmp;
				tmp.x = j;
				tmp.y = A[i][j];
				ODE.push_back(tmp);
				idx ++;
			}
		}
		index.push_back(idx);
	}
}

vector<double> toVector(vector<vector<double> > A)
{
	vector<double> vect;

	for(int i=0; i < A.size(); i++)
	{
		for(int j=0; j < A[i].size(); j++)
		{
			vect.push_back(A[i][j]);
		}
	}

	return vect;
}

vector<double> toVectorTS_matrix(vector<vector<double> > A)
{
	vector<double> vect;

	for(int i=0; i < A.size(); i++)
	{
		for(int j=1; j < A[i].size(); j++)
		{
			vect.push_back(A[i][j]);
		}
	}

	return vect;
}

void boolFeed(vector<double> &vect)
{
	for(int i=0; i < vect.size(); i++)
	{
		if(vect[i] > 0)
			vect[i] = 0.0;
		else
			vect[i] = 1.0;
	}
}

void returnGPU_properties(long *gpuProp)
{
	cudaDeviceProp  prop;
	CUDA_CHECK_RETURN(cudaGetDeviceProperties(&prop, 0));
	gpuProp[0] = prop.multiProcessorCount;
	gpuProp[1] = prop.totalGlobalMem;
	gpuProp[2] = prop.sharedMemPerBlock;
}

void returnBlockThread(int sm, int thread, int* info)
{
	info[0] = 0;
	info[1] = 0;


	float threadF = thread;
	int putative_threads_per_block =  minThread(256.0, ceil(threadF/sm));

	float used_blocks;
	float final_tpb;
	if(putative_threads_per_block % 32 == 0)
	{
		final_tpb = putative_threads_per_block * 1.;
		used_blocks = ceil(thread/final_tpb);
	}
	else
	{
		final_tpb =  (putative_threads_per_block/32+1)*32.;
		used_blocks = ceil(thread/final_tpb);
	}

	if(std::isnan(used_blocks) || used_blocks < 0)
		used_blocks = 0;
	if(std::isnan(final_tpb) || final_tpb < 0)
		final_tpb = 0;

	info[1] = (int) used_blocks;
	info[0] = (int) final_tpb;

}

int minThread(int n1, int n2)
{
	if (n1 < n2)
		return n1;
	else
		return n2;
}

int calculatenumBlockssShared(int N)
{
	int numThreads = 32;
	int count = 1;
	while(1)
	{
		if(numThreads >= N)
			break;
		numThreads = 32*count;
		count++;
	}

	return numThreads/32;
}

void printODEs(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *feed)
{	
	string s     = "";
	double coeff = 0.0;
	
	for(int gid = 0; gid < N; gid++)
	{
		
		s = "";
		int startH = indexH[gid];
		int endH   = indexH[gid+1];

		for(int i=startH; i < endH; i++)
		{
			coeff = H[i].y;

			if(coeff > 0)
				s = s + "+" + to_string((int)coeff) + "*" + "k[" + to_string(H[i].x) + "]";
			else
				s = s + "" + to_string((int)coeff) + "*" + "k[" + to_string(H[i].x) + "]";

			int start = indexODE[H[i].x];
			int end   = indexODE[H[i].x+1];
			for(int j=start; j < end; j++)
			{
				int idx = ODE[j].x;
				int exp = ODE[j].y;
				for(int k=0; k < exp; k++)
					s = s + "*" + "y[" + to_string(idx) +"]";
			}
		}

		if(feed[gid] == 0)
			s = "";

		if(strcmp(s.c_str(), "") == 0)
			printf(" dy[%d]/dt = 0\n", gid);
		else
			printf(" dy[%d]/dt = %s\n", gid, s.c_str());

	}
}

void printJacobian(int N, short2 *H, short2 *ODE, int *indexH, int *indexODE, double *feed)
{
	string s     = "";
	double coeff = 0.0;
	
	for(int gid = 0; gid < N; gid++)
	{
		for(int row=0; row < N; row++)
		{
			s = "";
			int startH = indexH[gid];
			int endH   = indexH[gid+1];
			for(int i=startH; i < endH; i++)
			{
				coeff = H[i].y;
				int start = indexODE[H[i].x];
				int end   = indexODE[H[i].x+1];
				for(int j=start; j < end; j++)
				{
					int idx = ODE[j].x;
					int exp = ODE[j].y;
					if(idx == row)
					{
						if(coeff > 0)
							s = s + "+" + to_string((int)coeff*exp) + "*" + "k[" + to_string(H[i].x) + "]";
						else
							s = s + "" + to_string((int)coeff*exp) + "*" + "k[" + to_string(H[i].x) + "]";

						for(int k=0; k < exp-1; k++)
							s = s + "*" + "y[" + to_string(idx) +"]";

						for(int k1 = start; k1 < end; k1++)
						{
							int idx1 = ODE[k1].x;
							int exp1 = ODE[k1].y;
							if(idx1 != row)	
								for(int k=0; k < exp1; k++)
									s = s + "*" + "y[" + to_string(idx1) +"]";
						}
					}
				}
			}

			if(feed[gid] == 0)
				s = "";

			if(strcmp(s.c_str(), "") == 0)
				printf(" dy[%d][%d]/dt = 0\n", gid, row);
			else
				printf(" dy[%d][%d]/dt = %s\n", gid, row, s.c_str());
		}
	}
	printf("\n");
}