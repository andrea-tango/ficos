#!/usr/bin/python
import os
import sys
import numpy as np
import time

sys.path.append(os.path.join(os.path.dirname(__file__), "src"))

from GenerateODEs import *
from Integrator import *

path = ""
path_output =""
dump = False

if __name__ == '__main__':

	t0     = time.time()
	dump   = False
	fit    = False
	method = "lsoda"

	if len(sys.argv)>1:
		path = sys.argv[1]
	if len(sys.argv)>2:
		path_output = sys.argv[2]
	
	if len(sys.argv)>3:
		for i in range(3, len(sys.argv)):
			if sys.argv[i]=="-d":
				dump = True
			elif sys.argv[i]=="-f":
				fit = True
			elif sys.argv[i]=="-l":
				method = "lsoda"
			elif sys.argv[i]=="-v":
				method = "vode"

	if fit:
		dump = False
		try:
			ts_matrix = np.loadtxt(path+os.sep+"ts_matrix")
		except:
			print(" * There is not ts_matrix file containing DTTS in %s"%path)


	if dump:
		print(" * Opening project in folder %s"%path)
		print(" * Saving output file in folder %s"%path_output)

	try:
		os.makedirs(path_output)
	except:
		if dump: print("WARNING: %s already exists"%path_output)

	# generation of ODEs and Jacobian
	gen = GenerateODEs()
	subspecies = gen.open_files(folder=path, output=path+"ODE.py", dump=dump, use_cmatrix=False)
		
	# Solver 
	integrator = Integrator()
	integrator.load_data_nofile(gen.functions_strings_for_lsoda)

	if gen.time_instants[0] != 0:
		tmp = np.zeros(len(gen.time_instants)+1)
		tmp[0] = 0
		tmp[1:] = gen.time_instants
		integrator.time_instants = tmp
	else:
		integrator.time_instants = gen.time_instants

	integrator.atolvector = gen.atolvector
	integrator.rtol       = gen.rtol
	integrator.max_steps  = gen.max_integration_steps

	if not fit:
		integrator.verbose    = True

	integrator.method     = method

	ret = integrator.run()

	new_matrix = [[]]*len(integrator.time_instants)

	for n, a in enumerate(new_matrix):
		new_matrix[n] = [ integrator.time_instants[n] ]
		new_matrix[n].extend(ret[n])

	finalMatrix = np.zeros((len(gen.time_instants), len(subspecies) + 1))

	if gen.time_instants[0] != 0:
		for i in range(len(gen.time_instants)):
			finalMatrix[i][0] = new_matrix[i+1][0]

		for i in range(len(gen.time_instants)):
			for j in range(0, len(subspecies)):
				idx = subspecies[j]
				finalMatrix[i][j+1] = new_matrix[i+1][idx+1]
	else:
		for i in range(len(gen.time_instants)):
			finalMatrix[i][0] = new_matrix[i][0]
		for i in range(len(gen.time_instants)):
			for j in range(0, len(subspecies)):
				idx = subspecies[j]
				finalMatrix[i][j+1] = new_matrix[i][idx+1]

	if fit:
		acc = sys.float_info.max
		try:
			acc = 0
			for i in range(0, finalMatrix.shape[0]):
				for j in range(1, finalMatrix.shape[1]):
					valueTS  = ts_matrix[i,j]
					valueSIM = finalMatrix[i,j]
					if valueTS != 0:
						if ts_matrix[i,j] < 1e-16:
							valueTS = 1e-16
						
						if finalMatrix[i,j] < 1e-16:
							valueSIM = 1e-16
						acc += abs(valueTS - valueSIM) / valueTS
		except:
			acc = sys.float_info.max

		print(acc)
	
	else:
		np.savetxt(path_output + os.sep + "ODESol_"+method, finalMatrix, fmt="%.8e", delimiter = "\t")
	
	t1 = time.time()
	total = t1-t0

	if dump:
		print("* Dynamics saved in folder", path_output+os.sep+"ODESol")
	
	if not fit:
		print("Simulation time:", total)
