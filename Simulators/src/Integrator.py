import time
import scipy
from scipy import *
from scipy import linspace
from scipy.integrate import ode
import numpy as np
import time



class Integrator():

	def __init__(self):
		self.PATH = ""
		self.t0 = 0
		self.points = 0
		self.parametri = 0
		self.time_max = 0
		self.time_instants = []
		self.max_steps = 0
		self.atolvector = None
		self.rtol = None
		self.verbose = False
		self.method  = "lsoda"


	def load_data(self, pa):
		self.PATH = pa
		exec(open(self.PATH).read(), globals())
		self.parametri = parametri
		self.time_max = tEnd

	def load_data_nofile(self, stringa):
		exec(stringa, globals())
		self.parametri = parametri
		self.time_max = tEnd

	def run(self):

		t0 = time.time()
		ret1 = []
		ret1.append(y0)

		if self.method == "lsoda":
			if self.verbose:
				print(" * Using lsoda as integration method")

			r = ode(ODE, JAC).set_integrator('lsoda', nsteps=self.max_steps, atol=self.atolvector, rtol=self.rtol)
			r.set_initial_value(y0).set_f_params(self.parametri).set_jac_params(self.parametri)
			
			count = 1
			while r.t < self.time_instants[-1]:
				dt = self.time_instants[count]
				ret1.append(r.integrate(dt))
				count+=1

		elif self.method == "vode":
			if self.verbose:
				print(" * Using vode as integration method")

			r = ode(ODE, JAC).set_integrator('vode', nsteps=self.max_steps, atol=self.atolvector, rtol=self.rtol)
			r.set_initial_value(y0).set_f_params(self.parametri).set_jac_params(self.parametri)
			
			count = 1
			while r.t < self.time_instants[-1]:
				dt = self.time_instants[count]
				ret1.append(r.integrate(dt))
				count+=1

		else:
			print(" * Error: unknown integration method")
		
		t1 = time.time()
		total = t1-t0
		if self.verbose:
			print("Integration time:", total)
		return ret1